/*
 * Genparse file for...genparse!!!
 */
#mandatory file

c / cppext	string	{"cc"}			"C++ file extension."
d		flag				"Turns on logging."
D / directory	string	{""}			"Directory for storing results."
f / logfile	string	{"genparse.log"}	"Log file name."
g / gnulib	flag				"Use GNU Portability Library (Gnulib)."
i / internationalize	flag			"Put internationalization macro _() around text output."
l / language	string	{"c"}			"Output language."
m / longmembers	flag				"Use long names for class members."
o / outfile	string	{"parse_cl"}		"Output file name."
p / parsefunc	string	{"Cmdline"}		"Name of parsing function / class."
P / manyprints	flag				"Output help text for every command line parameter in a"
						"separate print command."
q / quiet	flag				"Turns on quiet mode."
s / static-headers	flag			"Keep headers of generated files static. Don't add"
						"creation date, username, kernel version etc."
