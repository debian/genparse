.\"                                      -*- nroff -*-
.TH GENPARSE 1 2008-01-19 "" "Programmer's Manual"
.SH NAME
genparse \- command line parser generator
.SH SYNOPSIS
.B genparse
.RI [ options ] " files" ...
.SH "DESCRIPTION"
\fBgenparse\fP is a generic command line parser generator.  From simple and
concise specification file, you can define the command line parameters
and switches that you would like to be able to pass to your program.
Genparse creates the C, C++ or Java code of the parser for you.
.PP
Genparse assumes that the GNU getopt_long(3) function is built into your C 
library.  For Java you may have to specify an appropriate jar file to
your Java compiler.
.SH OPTIONS
\fBgenparse\fP accepts these options:
.TP
.BR -c , " --cppext " \fIextension\fP
C++ file extension. (default = cc)
.TP
.BR -d
Turn on logging.
.TP
.BR -f , " --logfile " \fIname\fP
Log file name. (default = genparse.log)
.TP
.BR -h , " --help"
Display help information.
.TP
.BR -g , " --gnulib"
Use GNU Compatibility Library (Gnulib, see \fIhttp://www.gnu.org/software/gnulib/\fP).  
Only available for C output.  Allows some more types (unsigned long, 
intmax_t etc.) for which Gnulib provides conversion functions.
.TP
.BR -i , " --internationalize"
Put internationalization macro _() around text output so that the generated
program can be internationalized using the GNU gettext command.  Presently 
only implemented for C output.
.TP
.BR -l , " --language " \fIlang\fP
Output language.  Only C, C++ and Java are supported.  Any of the following
indicate C++: "c++", "cpp", "cc", and "cxx".  For Java use: "java" or "Java".
(default = c)
.TP
.BR -o , " --outfile " \fIname\fP
Output file name. (default = parse_cl)
.TP
.BR -m , " --longmembers"
Use long options for the members of the parser class (struct).  The 
default is to use the short representation except if there is only a 
long representation defined in the genparse file.  If this option is 
set then the behavior is reverted.  The long representation is used 
then except if there is only a short representation defined.
.TP
.BR -o , " --outfile " \fIfilename\fP
Root name of output file.  The extension will be determined by the
output language and possibly by other options.  For example, when the
output language is C, giving this option an argument of "file" will
result in output file names of "file.h", "file.c" and "file_cb.c" for
the header, parser, and callback files, respectively.  Default value
is "parse_cl".
.TP
.BR -p , " --parsefunc " \fIfunc\fP
Name of parsing function / class.  This option allows the user to
specify the name of the function (for C) or class (for C++ and Java) 
that does the actual command line parsing (default = "Cmdline").
.TP
.BR -P , " --manyprints"
Output help text for every command line parameter in a separate print 
command.
.TP
.BR -q , " --quiet"
Quiet mode - no on screen output.
.TP
.BR -s , " --static-headers"
Keep the descriptive header on top of the generated files static. 
Without this option genparse prints creation date and time, Linux 
kernel version, kernel build time, computer architecture name, host 
name and user name.
.TP
.BR -v , " --version"
Output version.
.TP
.BR -D , " --directory "
Directory for storing results.
.SH "INPUT FILE"
A genparse specification file (usually just called a 'genparse file')
consists of a number of entries, one per command line parameter, of
the form:
.sp
.IR  short_names "[*|!] [/ " "long_name" [*|!][= "opt_name" ]] " type " [ " options " ]
.sp
A \fIshort_name\fP is a single character (small or capital) or a single 
digit.  \fIlong_name\fP is a longer (more descriptive) option name.  
On the command line a short name will be preceded by a single dash 
(e.g. '\-a') and a long version will be preceded by two dashes 
(e.g. '\-\-all').  If a long parameter name is not necessary, you may 
specify only the short one (and the slash need not appear).  In order 
to specify a parameter that only has a long name set \fIshort_names\fP 
to \fBNONE\fP.  It is possible to have multiple short options, so 
for example setting \fIshort_name\fP to 'aA' and \fIlong_name\fP 
to 'all' would allow specifying the command line switch as '\-a' or '\-A'
or '\-\-all', all of them doing the same thing.
.sp
A \fB*\fP after \fIshort_name\fP or \fIlong_name\fP makes the argument optional.
This can be specified for short and long options separately.
.sp
A \fB!\fP after \fIshort_name\fP or \fIlong_name\fP makes the option boolean.
This allows one to combine a boolean short option with a long option with an optional
or mandatory argument or to combine a boolean long option with a short option with 
an optional or mandatory argument. A \fB!\fP doesn't make sense if the option's
type is \fBflag\fP.
.PP
\fItype\fP must be one of \fBint float char string\fP or
\fBflag\fP.  The first four should be self-explanatory.  The last is a
"switch" option that takes no arguments.  For C output and if \-\-gnulib is set on 
the command line additionally the following types are allowed: \fBlong\fP 
(for long int), \fBulong\fP (for unsigned long int), \fBintmax\fP (for intmax_t, 
defined in Gnulib), \fBuintmax\fP (for uintmax_t), \fBdouble\fP.
.PP
The following \fIoptions\fP are supported. They may appear in any 
order and except for \fBdescriptions\fP only one of each field may be 
defined per option.
.sp
.RS
A \fBdefault value\fP for the parameter.  For a string this
is just the plain default value, whatever it is.  For strings, a
default must be specified within braces and quotes, and may include
whitespace, e.g. {"my default value"}.  For a char parameter it must
be enclosed in single quotes, e.g. 'a' or '\\n'.
.PP
A \fBrange\fP of values within brackets.  The low and high values are
specified between a range specifier (either '...' or '..').
Either the high or the low value may be omitted for a range bounded on
one side only.  The parameter will be checked to make sure that it
lies within this range.
.PP
A \fBcallback function\fP.  This function is called after any range
checking is performed.  The purpose of the callback to do validity
checking that is more complicated than can be specified in the
genparse file.  For example, you might write a program that requires
input to be prime numbers, strings of a certain length, etc.
.PP
A \fBdescription\fP in double quotes.  It is printed by the \fBusage()\fP
function.  If one line is not enough then specify multiple descriptions, 
one per line and each of them in double quotes.  If the description starts 
in the 1st column in the Genparse file then it will also be printed in the 1st 
column in the \fBusage()\fP function.
.PP
A \fB#gp_include\fP directive will instruct genparse to include another 
genparse file, e.g \fB#gp_include another.gp\fP.  Only parameter definitions 
are allowed in the included file, no global directives.
.PP
An \fB__ERR_MSG__(err_txt)\fP directive.  Specifies the error message which 
is printed when the argument could not be converted.  Example: 
\fB__ERR_MSG__("%s: invalid argument")\fP.  This message will be printed when 
either the conversion function failed or when the argument was out of range.  
Assumes to contain one \fB%s\fP which will be replaced with
the argument which could not be converted.  Only available when Genparse is 
invoked with \fB--gnulib\fP, ignored otherwise.
.PP
Optionally a conversion function can be added as a second argument, e. g. 
\fB__ERR_MSG__("%s: invalid argument", quotearg)\fP.  This would lead to an
error message like 
\fBerror (EXIT_FAILURE, 0, "%s: invalid argument", quotearg (optind))\fP.
.PP
An \fB__ADD_FLAG__\fP directive.  Makes sense only if the command line 
parameter is not already a flag, in this case an additional flag parameter 
is added which will be set if the command line parameter was specified on 
the command line.  This option is automatically set if a parameter has
an optional argument.
.PP
A \fB__CODE__(statements)\fP directive.  The specified code statements are 
copied literally.  Example: \fB__CODE__(printf ("Parameter x was set");)\fP.
The specified code can extend  over more than one line.  In order to give 
Genparse the chance to indent the code properly, do not mix space and tab 
indentations in one \fB__CODE__\fP statement.
.PP
A \fB__STORE_LONGINDEX__\fP directive.  Instructs Genparse to add an interer
type field to the result class which will be set to the longindex variable
(last argument in the call to @code{getopt_long()}).  This new field will 
get the same name as the result field it is related to but with an 
\fB_li\fP postfix.
.RE
.sp
The following \fIglobal directives\fP are supported. They may appear in any 
order.
.sp
.RS
An \fB#include\fP directive will instruct genparse to copy the said include
statement into the C or C++ code generated by genparse, but not any 
header files or callback files.
.PP
A \fB#mandatory\fP directive can be used it make usage() function calls
nicer.  It allows you to specify mandatory command line parameters
that might follow switches.  Note that Genparse does not check for 
mandatory parameters, they are only printed in the \fBusage ()\fP
function with the \fB__MANDATORIES__\fP directive. \fBDeprecated: add 
mandatory parameters in the #usage section instead.\fP
.PP
An \fB#exit_value\fP directive which specifies the exit value in case
of an error.  Default is EXIT_FAILURE.
.PP
A \fB#break_lines\fP directive which specifies the width to which lines
shall be broken on the help screen.  If no \fB#break_lines\fP directive
is specified then lines will be printed exactly as given in the genparse file.
.PP
If \fB#no_struct\fP is specified then no struct will be defined which will be 
filled with the command line parameters in the generated parser.  This may be 
useful if you want to add your own code with \fB__CODE__\fP statements instead.
Only supported for C output.
.PP
A \fB#export_long_options\fP directive. If \fB#export_long_options\fP is 
defined then a function \fB#get_long_options()\fP is added which exports the 
longoptions array used by \fB#getopt_long()\fP.  This directive is only available
for C output, for other languages it is ignored.
.PP
A \fBglobal callback function\fP.  This function is useful for checking
interdependencies between parameters.  Interdependencies cannot be
checked within each individual callback function because the order in
which these functions will be called varies, depending on the order of
the parameters on the command line.
.RE
.sp
Genparse also generates a \fBusage()\fP function which prints a help text 
to stdout about the usage of the program for which Genparse is 
generating the parser.  It can be customized by specifying a usage 
section at the bottom of the Genparse file.  If no such section is 
specified it defaults to
.sp
.nf
#usage_begin
usage: __PROGRAM_NAME__ __OPTIONS_SHORT__ __MANDATORIES__
__GLOSSARY__
#usage_end
.fi
.sp
The usage section starts with \fB#usage_begin\fP and ends with
\fB#usage_end\fP.  Any text between is printed verbatim except 
for the following keywords, which will be replaced as listed below:
.sp
.RS
\fB__PROGRAM_NAME__\fP: The program name.  In C and C++ the program
name is given in argv[0].
.PP
\fB__OPTIONS_SHORT__\fP: A list of available short form options, e.g.
[ \-abc ].
.PP
\fB__MANDATORIES__\fP: A list of all mandatory parameters as defined
with #mandatory commands.  \fBDeprecated: List mandatory 
parameters here directly.\fP
.PP
\fB__GLOSSARY__\fP: A description of all command line options.  This is 
the information given for the parameter definitons in human readable 
form.  It includes the parameter type, default, range and any comments.  
A line which contains \fB__GLOSSARY__\fP is replaced by the glossary 
of the parameters, any other text in the same line is ignored.
.PP
\fB__GLOSSARY_GNU__\fP: Same as \fB__GLOSSARY__\fP but in GNU style.
Optionally followed by an integer in brackets which specifies the
indentation of the descriptive text (e.g. \fB__GLOSSARY__\fP(30)). 
Default indentation is 24.
.PP
\fB__STRING__(s)\fP: A string constant, in C probably a string macro
defined with the #define preprocessor command.  This macro can be
imported from another file using the include directive in the genparse 
file.  Ignored when generating Java output.
.PP
\fB__INT__(x)\fP: An integer constant, in C probably an integer macro
defined with the #define preprocessor command.  This macro can be
imported from another file using the include directive in the genparse 
file.  Ignored when generating Java output.
.PP
\fB__CODE__(statements)\fP: Same as for the parameter options, see above.
.PP
\fB__DO_NOT_DOCUMENT__\fP: Any line which contains this macro will not be
printed in the \fIusage()\fP function.  Can be used for implementing 
command line parameters without listing them on the help screen.
.PP
\fB__NL__\fP: New line.  Useful for breaking lines manually while automatic
line breaking is on (see \fB#break_lines\fP).  Ignored when generating 
Java output.
.PP
\fB__NEW_PRINT__\fP: Close the active print command and start a new one.
.PP
\fB__COMMENT__(text)\fP: Comment in the code for printing the usage text.
.RE
.PP
long options can be followed by an = sign and an optional designation 
\fIopt_name\fP which can be referred to in the following description.  
It will be used in the \fIusage()\fP function only.  For example the 
following genparse line
.sp
.nf
s / block-size=SIZE		int	"use SIZE-byte blocks"
.fi
.sp
.PP
will lead to the following line in the help screen
.sp
.nf
   [ \-s ] [ \-\-block-size=SIZE ] (type=INTEGER)
          use SIZE-byte blocks
.fi
.sp
in genparse style (\fB__GLOSSARY__\fP) or 
.sp
.nf
   \-s, \-\-block\-size=SIZE   use SIZE\-byte blocks
.fi
.sp
in GNU style (\fB__GLOSSARY_GNU__\fP).

It is also possible to put square braces around the optional name in order
to indicate that the argument is optional.  This has no meaning for the 
generated parser however.  Use * postfixes in order to make an 
argument optional.

.nf
s* / block*[=SIZE]   int   "use blocks."
                           "If SIZE is not given then they will get a size of 1kB."
.fi

will lead to the following line in the help screen

.nf
\-s, \-\-block[=SIZE]   use blocks.
                     If SIZE is not given then they will get a size of 1kB.
.fi
.RE
.sp
.SH EXAMPLE
Here is a sample genparse file:
.sp
.nf
#include<sys/types.h>

/* comment */
my_callback()

i / iterations int 100 [10...1000] iter_callback()
        "Number of iterations to run for."

/*
 * Comment
 */

n / name string {"mike"} name_cb() "User's name"
s / str  string                    "test string"
f        flag                      "a stupid flag!"

#usage_begin
usage: __PROGRAM_NAME__ __OPTIONS_SHORT__ filename
This is only a stupid test program.
__GLOSSARY__
#usage_end
.fi
.sp
.SH "SEE ALSO"
For an example input file, see
\fI/usr/share/doc/genparse/examples/test.gp\fP.
.P
The full documentation for \fBgenparse\fP is maintained as a Texinfo
manual.  If the \fBinfo\fP and \fBgenparse\fP programs are properly
installed at your site, the command
.sp
        info genparse
.sp
should give you access to the complete manual.
.sp
The latest version of \fBgenparse\fP can always be found at 
\fIhttp://genparse.sourceforge.net\fP.
.SH AUTHOR
This manual page was written by James R. Van Zandt and Michael Geng.
