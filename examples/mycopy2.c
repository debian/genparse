/* mycopy2.c */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

int main (int argc, char *argv[])
{
  int c, i;
  FILE *fp;
  extern char *optarg;
  extern int optind;
  int option_index = 0;
  char ch;
  int help, iterations;
  int errflg = 0;

  static struct option long_options[] =
  {
    {"help", no_argument, NULL, 'h'},
    {"iterations", required_argument, NULL, 'i'},
    {NULL, 0, NULL, 0}
  };

  help = 0;
  iterations = 1;

  while ((ch = getopt_long (argc, argv, "hi:", long_options, 
			    &option_index)) != EOF)
    {
      switch (ch)
        {
	case 'h':
          help = 1;
          break;

        case 'i':
          iterations = atoi (optarg);
          if (iterations < 0)
            {
              fprintf (stderr, "error: iterations must be >= 0\n");
              errflg ++;
            }
          break;

        default:
          errflg++;

	}
    } /* while */

  if (errflg || help)
    {
      printf ("usage: %s [ -i ] <file>\n", argv[0]);
      printf ("  [ -h ] [ --help ] Print help screen \n");
      printf ("  [ -i ] [ --iterations ] Number of times to \
output <file>\n");
      exit (1);
    }

  if (optind >= argc)
    fp = stdin;
  else
    fp = fopen (argv[optind],"r");

  for (i = 0; i < iterations; i++)
    {
      while ((c = fgetc (fp)) != EOF)
	fputc (c, stdout);
      rewind (fp);
    }
  
  fclose (fp);
  return 0;
}
