/* mycopy1.c */

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
  int c, i, n;
  FILE *fp;

  n = atoi (argv[1]);
  fp = fopen (argv[2],"r");
  
  for (i = 0; i < n; i++)
    {
      while ((c = fgetc (fp)) != EOF)
	fputc (c, stdout);
      rewind (fp);
    }
  
  fclose (fp);
  return 0;
}
