#include "coreutils.h"
#include "tail.h"

NONE / retry			flag	"keep trying to open a file even if it is"
					"inaccessible when tail starts or if it becomes"
					"inaccessible later; useful when following by name,"
					"i.e., with --follow=name"
c / bytes=N			string	"output the last N bytes; alternatively, use +N to"
					"output bytes starting with the Nth of each file__NEW_PRINT__"
f! / follow*[={name|descriptor}] string	""
					"output appended data as the file grows;"
					"-f, --follow, and --follow=descriptor are"
					"equivalent"
F				flag	"same as --follow=name --retry__NEW_PRINT__"
n / lines=N			string	"output the last N lines, instead of the last __INT__(DEFAULT_N_LINES);"
					"or use +N to output lines starting with the Nth"
NONE / max-unchanged-stats=N	uintmax	0
					""
					"with --follow=name, reopen a FILE which has not"
					"changed size after N (default __INT__(DEFAULT_MAX_N_UNCHANGED_STATS_BETWEEN_OPENS)) iterations"
					"to see if it has been unlinked or renamed"
					"(this is the usual case of rotated log files)__NEW_PRINT__"
					__ERR_MSG__("%s: invalid maximum number of unchanged stats between opens")
NONE / pid=PID			ulong	[..PID_T_MAX]	0
					"with -f, terminate after process ID, PID dies"
					__ERR_MSG__("%s: invalid PID")
q / quiet			flag	"never output headers giving file names"
NONE / silent			flag	"same as --quiet"
s / sleep-interval=S		double	[0..]	0
					"with -f, sleep for approximately S seconds"
					"(default 1.0) between iterations."
					__ERR_MSG__("%s: invalid number of seconds")
v / verbose			flag	"always output headers giving file names__NEW_PRINT__"
#gp_include help_version.gp
NONE / -presume-input-pipe	flag	"__DO_NOT_DOCUMENT__"
0				flag	"__DO_NOT_DOCUMENT__"
1				flag	"__DO_NOT_DOCUMENT__"
2				flag	"__DO_NOT_DOCUMENT__"
3				flag	"__DO_NOT_DOCUMENT__"
4				flag	"__DO_NOT_DOCUMENT__"
5				flag	"__DO_NOT_DOCUMENT__"
6				flag	"__DO_NOT_DOCUMENT__"
7				flag	"__DO_NOT_DOCUMENT__"
8				flag	"__DO_NOT_DOCUMENT__"
9				flag	"__DO_NOT_DOCUMENT__"

#usage_begin
Usage: __PROGRAM_NAME__ [OPTION]... [FILE]...
Print the last __INT__(DEFAULT_N_LINES) lines of each FILE to standard output.
With more than one FILE, precede each with a header giving the file name.
With no FILE, or when FILE is -, read standard input.__NEW_PRINT__
Mandatory arguments to long options are mandatory for short options too.__NEW_PRINT__
__GLOSSARY_GNU__(27)

If the first character of N (the number of bytes or lines) is a `+',
print beginning with the Nth item from the start of each file, otherwise,
print the last N items in the file.  N may have a multiplier suffix:
b 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,
GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y.__NEW_PRINT__

With --follow (-f), tail defaults to following the file descriptor, which
means that even if a tail'ed file is renamed, tail will continue to track
its end.  This default behavior is not desirable when you really want to
track the actual name of the file, not the file descriptor (e.g., log
rotation).  Use --follow=name in that case.  That causes tail to track the
named file by reopening it periodically to see if it has been removed and
recreated by some other program.
__CODE__(emit_bug_reporting_address ();)
#usage_end
