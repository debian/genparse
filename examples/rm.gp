#include "coreutils.h"

d / directory	flag	"unlink directory, even if non-empty (super-user only)"
f / force	flag	"ignore nonexistant files, never prompt"
i / interactive	flag	"prompt before any removal"
rR / recursive	flag	"remove the contents of directories recursively"
v / verbose	flag	"explain what is being done"
h / help	flag	"print this help and exit"
NONE / version	flag	"print version information and exit"

#usage_begin
Remove (unlink) the FILE(s).

Usage: __PROGRAM_NAME__ [OPTION]... FILE...
__GLOSSARY_GNU__

To remove a file whose name starts with a `-', for example `-foo',
use one of these commands:
  __PROGRAM_NAME__ -- -foo

  __PROGRAM_NAME__ ./-foo

Note that if you use rm to remove a file, it is usually possible to recover
the contents of that file.  If you want more assurance that the contents are
truly unrecoverable, consider using shred.
__CODE__(emit_bug_reporting_address ();)
#usage_end
