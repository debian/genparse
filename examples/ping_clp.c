/******************************************************************************
**
** ping_clp.c
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "error.h"
#include "xstrtol.h"
#include "xstrtod.h"
#include "xstrtod.h"
#include "ping_clp.h"

static struct option const long_options[] =
{
  {"help", no_argument, NULL, 'h'},
  {"license", no_argument, NULL, 'L'},
  {"version", no_argument, NULL, 'V'},
  {"echo", no_argument, NULL, 256},
  {"address", no_argument, NULL, 257},
  {"timestamp", no_argument, NULL, 258},
  {"type", required_argument, NULL, 't'},
  {"router", no_argument, NULL, 259},
  {"count", required_argument, NULL, 'c'},
  {"debug", no_argument, NULL, 'd'},
  {"interval", required_argument, NULL, 'i'},
  {"numeric", no_argument, NULL, 'n'},
  {"ignore-routing", no_argument, NULL, 'r'},
  {"verbose", no_argument, NULL, 'v'},
  {"flood", no_argument, NULL, 'f'},
  {"preload", required_argument, NULL, 'l'},
  {"pattern", required_argument, NULL, 'p'},
  {"quiet", no_argument, NULL, 'q'},
  {"route", no_argument, NULL, 'R'},
  {"size", required_argument, NULL, 's'},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->help = false;
  my_args->license = false;
  my_args->version = false;
  my_args->echo = false;
  my_args->address = false;
  my_args->timestamp = false;
  my_args->type = NULL;
  my_args->router = false;
  my_args->count = NULL;
  my_args->debug = false;
  my_args->interval_flag = false;
  my_args->numeric = false;
  my_args->ignore_routing = false;
  my_args->verbose = false;
  my_args->flood = false;
  my_args->preload = 0;
  my_args->pattern = NULL;
  my_args->quiet = false;
  my_args->route = false;
  my_args->size = NULL;

  optind = 0;
  while ((c = getopt_long (argc, argv, "hLVt:c:di:nrvfl:p:qRs:", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'h':
          my_args->help = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 'L':
          my_args->license = true;
          break;

        case 'V':
          my_args->version = true;
          break;

        case 256:
          my_args->echo = true;
          break;

        case 257:
          my_args->address = true;
          break;

        case 258:
          my_args->timestamp = true;
          break;

        case 't':
          my_args->type = optarg;
          break;

        case 259:
          my_args->router = true;
          break;

        case 'c':
          my_args->count = optarg;
          break;

        case 'd':
          my_args->debug = true;
          break;

        case 'i':
          my_args->interval_flag = true;
          if (optarg != NULL)
            if (! xstrtod (optarg, NULL, &my_args->interval, strtod))
              error (EXIT_FAILURE, 0, "Invalid value (`%s')", optarg);
          break;

        case 'n':
          my_args->numeric = true;
          break;

        case 'r':
          my_args->ignore_routing = true;
          break;

        case 'v':
          my_args->verbose = true;
          break;

        case 'f':
          my_args->flood = true;
          break;

        case 'l':
          if (xstrtoul (optarg, NULL, 10, &my_args->preload, NULL) != LONGINT_OK
              || my_args->preload < 1
              || my_args->preload > INT_MAX)
            error (EXIT_FAILURE, 0, "ping: invalid preload value (%s)", optarg);
          break;

        case 'p':
          my_args->pattern = optarg;
          break;

        case 'q':
          my_args->quiet = true;
          break;

        case 'R':
          my_args->route = true;
          break;

        case 's':
          my_args->size = optarg;
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, "Try `%s --help' for more information.\n",
            program_name);
  else
    {
      printf ("\
Usage: ping [OPTION]... [ADDRESS]...\n\
\n\
Informational options:\n\
  -h, --help         display this help and exit\n\
  -L, --license      display license and exit\n\
  -V, --version      output version information and exit\n\
Options controlling ICMP request types:\n\
      --echo         Send ICMP_ECHO requests (default)\n\
      --address    * Send ICMP_ADDRESS packets\n\
      --timestamp    Send ICMP_TIMESTAMP packets\n\
  -t, --type         Send TYPE packets\n\
      --router     * Send ICMP_ROUTERDISCOVERY packets\n\
Options valid for all request types:\n\
  -c, --count=N      stop after sending N packets\n\
  -d, --debug        set the SO_DEBUG option\n\
  -i, --interval=N   wait N seconds between sending each packet\n\
  -n, --numeric      do not resolve host addresses\n\
  -r, --ignore-routing  send directly to a host on an attached network\n\
  -v, --verbose      verbose output\n\
Options valid for --echo requests:\n\
  -f, --flood      * flood ping\n\
  -l, --preload=N  * send N packets as fast as possible before falling into\n\
                     normal mode of behavior\n\
  -p, --pattern=PAT  fill ICMP packet with given pattern (hex)\n\
  -q, --quiet        quiet output\n\
  -R, --route        record route\n\
  -s, --size=N       set number of data octets to send\n\
\n\
Options marked with an * are available only to super-user\n\
\n\
Report bugs to <%s>.\n", PACKAGE_BUGREPORT);
    }
  exit (status);
}
