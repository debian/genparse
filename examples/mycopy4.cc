/* mycopy4.cc */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include "mycopy4_clp.h"

using namespace std;

#define VERSION "3.0"

int main (int argc, char *argv[])
{
  int i;
  char c;
  ifstream input_file;
  ofstream output_file;
  bool ofile = false, ifile = false;

  Cmdline cl (argc, argv);

  if (cl.v ())
    {
      cout << argv[0] << " version " << VERSION << endl;
      exit (0);
    }

  if (!cl.o ().empty ())
    {
      output_file.open (cl.o ().c_str ());
      ofile = true;
    }
  
  if (cl.next_param ())
    {
      input_file.open (argv[cl.next_param ()]);
      ifile = true;
    }

  for (i = 0; i < cl.i (); i++)
    {
      if (ifile) c = input_file.get ();
      else cin >> c;

      while (c != EOF)
	{
	  if (ofile) output_file.put (c);
	  else cout << c;

	  if (ifile) c = input_file.get ();
	  else cin >> c;
	}
      if (ifile) 
	{
	  input_file.clear ();
	  input_file.seekg (0);
	}
    }
  
  input_file.close ();
  output_file.close ();

  return 0;
}
