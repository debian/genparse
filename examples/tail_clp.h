/******************************************************************************
**
** tail_clp.h
**
** Header file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <stdio.h>
#include "xstrtol.h"
#include "coreutils.h"
#include "tail.h"

#ifndef bool
typedef enum bool_t
{
  false = 0, true
} bool;
#endif

/* customized structure for command line parameters */
struct arg_t
{
  bool retry;
  char * bytes;
  char * follow;
  bool follow_flag;
  bool F;
  char * lines;
  uintmax_t max_unchanged_stats;
  unsigned long int pid;
  bool quiet;
  bool silent;
  double sleep_interval;
  bool verbose;
  bool help;
  bool version;
  bool _presume_input_pipe;
  bool _0;
  bool _1;
  bool _2;
  bool _3;
  bool _4;
  bool _5;
  bool _6;
  bool _7;
  bool _8;
  bool _9;
  int optind;
};

/* function prototypes */
void Cmdline (struct arg_t *my_args, int argc, char *argv[]);
void usage (int status, char *program_name);
