/******************************************************************************
**
** mycopy4_clp.h
**
** Sun Nov 13 11:28:46 2016
** Linux 4.6.0 (#7 Fri Jun 17 22:37:23 CEST 2016) i686
** linux@mgpc (Michael Geng)
**
** Header file for command line parser class
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#ifndef CMDLINE_H
#define CMDLINE_H

#include <iostream>
#include <string>
#include "mycopy4.h"

/*----------------------------------------------------------------------------
**
** class Cmdline
**
** command line parser class
**
**--------------------------------------------------------------------------*/

class Cmdline
{
private:
  /* parameters */
  int _i;
  std::string _o;
  bool _h;
  bool _v;

  /* other stuff to keep track of */
  std::string _program_name;
  int _optind;

public:
  /* constructor and destructor */
  Cmdline (int, char **) throw (std::string);
  ~Cmdline (){}

  /* usage function */
  void usage (int status);

  /* return next (non-option) parameter */
  int next_param () { return _optind; }

  /* callback functions */
  bool my_callback ();
  bool outfile_cb ();

  int i () const { return _i; }
  std::string o () const { return _o; }
  bool h () const { return _h; }
  bool v () const { return _v; }
};

#endif
