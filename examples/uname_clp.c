/******************************************************************************
**
** uname_clp.c
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "uname_clp.h"

static struct option const long_options[] =
{
  {"all", no_argument, NULL, 'a'},
  {"kernel-name", no_argument, NULL, 's'},
  {"nodename", no_argument, NULL, 'n'},
  {"kernel-version", no_argument, NULL, 'v'},
  {"kernel-release", no_argument, NULL, 'r'},
  {"machine", no_argument, NULL, 'm'},
  {"processor", no_argument, NULL, 'p'},
  {"version", no_argument, NULL, 256},
  {"help", no_argument, NULL, 'h'},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->all = false;
  my_args->kernel_name = false;
  my_args->nodename = false;
  my_args->kernel_version = false;
  my_args->kernel_release = false;
  my_args->machine = false;
  my_args->processor = false;
  my_args->version = false;
  my_args->help = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "asnvrmph", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'a':
          my_args->all = true;
          break;

        case 's':
          my_args->kernel_name = true;
          break;

        case 'n':
          my_args->nodename = true;
          break;

        case 'v':
          my_args->kernel_version = true;
          break;

        case 'r':
          my_args->kernel_release = true;
          break;

        case 'm':
          my_args->machine = true;
          break;

        case 'p':
          my_args->processor = true;
          break;

        case 256:
          my_args->version = true;
          break;

        case 'h':
          my_args->help = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, _("Try `%s --help' for more information.\n"),
            program_name);
  else
    {
      printf (_("\
Usage: %s [OPTION]...\n\
Print certain system information.  With no OPTION, same as -s.\n\
\n\
  -a, --all             print all information, in the following order,\n\
                          except omit -p and -i if unknown:\n\
  -s, --kernel-name     print the kernel name\n\
  -n, --nodename        print the network node hostname\n\
  -v, --kernel-version  print the kernel name\n\
  -r, --kernel-release  print the kernel version\n\
  -m, --machine         print the kernel machine hardware name\n\
  -p, --processor       print the processor type or \n\
      --version         print version information and exit\n\
  -h, --help            Display this help and exit.\n"), program_name);
      emit_bug_reporting_address ();
    }
  exit (status);
}
