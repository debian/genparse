/******************************************************************************
**
** mycopy4_clp.cc
**
** Sun Nov 13 11:28:46 2016
** Linux 4.6.0 (#7 Fri Jun 17 22:37:23 CEST 2016) i686
** linux@mgpc (Michael Geng)
**
** Definition of command line parser class
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <getopt.h>
#include <stdlib.h>
#include "mycopy4_clp.h"

/*----------------------------------------------------------------------------
**
** Cmdline::Cmdline ()
**
** Constructor method.
**
**--------------------------------------------------------------------------*/

Cmdline::Cmdline (int argc, char *argv[]) throw (std::string )
{
  extern char *optarg;
  extern int optind;
  int c;

  static struct option long_options[] =
  {
    {"iterations", required_argument, NULL, 'i'},
    {"outfile", required_argument, NULL, 'o'},
    {"help", no_argument, NULL, 'h'},
    {"version", no_argument, NULL, 'v'},
    {NULL, 0, NULL, 0}
  };

  _program_name += argv[0];

  /* default values */
  _i = 1;
  _h = false;
  _v = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "i:o:hv", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'i': 
          _i = atoi (optarg);
          if (_i < 1)
            {
              std::string s;
              s += "parameter range error: i must be >= 1";
              throw (s);
            }
          if (_i > MAX)
            {
              std::string s;
              s += "parameter range error: i must be <= MAX";
              throw (s);
            }
          break;

        case 'o': 
          _o = optarg;
          if (!outfile_cb ())
            this->usage (EXIT_FAILURE);
          break;

        case 'h': 
          _h = true;
          this->usage (EXIT_SUCCESS);
          break;

        case 'v': 
          _v = true;
          break;

        default:
          this->usage (EXIT_FAILURE);

        }
    } /* while */

  _optind = optind;
  if (!my_callback ())
    usage (EXIT_FAILURE);

}

/*----------------------------------------------------------------------------
**
** Cmdline::usage ()
**
** Print out usage information, then exit.
**
**--------------------------------------------------------------------------*/

void Cmdline::usage (int status)
{
  if (status != EXIT_SUCCESS)
    std::cerr << "Try `" << _program_name << " --help' for more information.\n";
  else
    {
      std::cout << "\
usage: " << _program_name << " [ -iohv ] file\n\
Print a file for a number of times to stdout.\n\
   [ -i ] [ --iterations ] (type=INTEGER, range=1...MAX, default=1)\n\
          Number of times to output <file>.\n\
          do it like this\n\
   [ -o ] [ --outfile ] (type=STRING)\n\
          Output file.\n\
   [ -h ] [ --help ] (type=FLAG)\n\
          Display this help and exit.\n\
   [ -v ] [ --version ] (type=FLAG)\n\
          Output version information and exit.\n";
    }
  exit (status);
}
