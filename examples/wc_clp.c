/******************************************************************************
**
** wc_clp.c
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "wc_clp.h"

static struct option const long_options[] =
{
  {"bytes", no_argument, NULL, 'c'},
  {"chars", no_argument, NULL, 'm'},
  {"lines", no_argument, NULL, 'l'},
  {"files0-from", required_argument, NULL, 256},
  {"max-line-length", no_argument, NULL, 'L'},
  {"words", no_argument, NULL, 'w'},
  {"help", no_argument, NULL, 257},
  {"version", no_argument, NULL, 258},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->bytes = false;
  my_args->chars = false;
  my_args->lines = false;
  my_args->files0_from = NULL;
  my_args->max_line_length = false;
  my_args->words = false;
  my_args->help = false;
  my_args->version = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "cmlLw", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'c':
          my_args->bytes = true;
          break;

        case 'm':
          my_args->chars = true;
          break;

        case 'l':
          my_args->lines = true;
          break;

        case 256:
          my_args->files0_from = optarg;
          break;

        case 'L':
          my_args->max_line_length = true;
          break;

        case 'w':
          my_args->words = true;
          break;

        case 257:
          my_args->help = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 258:
          my_args->version = true;
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, _("Try `%s --help' for more information.\n"),
            program_name);
  else
    {
      printf (_("\
Usage: %s [OPTION]... [FILE]...\n\
  or:  %s [OPTION]... --files0-from=F\n"), program_name, program_name);
      puts (_("\
Print newline, word, and byte counts for each FILE, and a total line if\n\
more than one FILE is specified.  With no FILE, or when FILE is -,\n\
read standard input.\n\
  -c, --bytes            print the byte counts\n\
  -m, --chars            print the character counts\n\
  -l, --lines            print the newline counts"));
      puts (_("\
      --files0-from=F    read input from the files specified by\n\
                           NUL-terminated names in file F\n\
  -L, --max-line-length  print the length of the longest line\n\
  -w, --words            print the word counts"));
      puts (_("\
      --help             display this help and exit"));
      puts (_("\
      --version          output version information and exit"));
      emit_bug_reporting_address ();
    }
  exit (status);
}
