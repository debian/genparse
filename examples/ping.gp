#include <limits.h>
#include "ping.h"

h / help			flag	"  display this help and exit"
L / license			flag	"  display license and exit"
V / version			flag	"  output version information and exit"
"Options controlling ICMP request types:"
NONE / echo			flag	"  Send ICMP_ECHO requests (default)"
NONE / address			flag	"* Send ICMP_ADDRESS packets"
NONE / timestamp		flag	"  Send ICMP_TIMESTAMP packets"
t / type			string	"  Send TYPE packets"
NONE / router			flag	"* Send ICMP_ROUTERDISCOVERY packets"
"Options valid for all request types:"
c / count=N			string	"  stop after sending N packets"
d / debug			flag	"  set the SO_DEBUG option"
i / interval=N			double	"  wait N seconds between sending each packet"
					__ERR_MSG__("Invalid value (`%s')")
					__ADD_FLAG__
n / numeric			flag	"  do not resolve host addresses"
r / ignore-routing		flag	"  send directly to a host on an attached network"
v / verbose			flag	"  verbose output"
"Options valid for --echo requests:"
f / flood			flag	"* flood ping"
l / preload=N			ulong	[1..INT_MAX]	0
					"* send N packets as fast as possible before falling into"
					"  normal mode of behavior"
					__ERR_MSG__("ping: invalid preload value (%s)")
p / pattern=PAT			string	"  fill ICMP packet with given pattern (hex)"
q / quiet			flag	"  quiet output"
R / route			flag	"  record route"
s / size=N			string	"  set number of data octets to send"

#usage_begin
Usage: ping [OPTION]... [ADDRESS]...

Informational options:
__GLOSSARY_GNU__(19)

Options marked with an * are available only to super-user

Report bugs to <__STRING__(PACKAGE_BUGREPORT)>.
#usage_end
