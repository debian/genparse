/* rm.c */

#include <stdio.h>
#include "rm_clp.h"

int main (int argc, char *argv[])
{
  struct arg_t a;

  Cmdline (&a, argc, argv);

  printf ("This is only a demo program. Nothing will be removed.\n");

  return 0;
}
