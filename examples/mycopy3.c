/* mycopy3.c */

#include <stdio.h>
#include <stdlib.h>
#include "mycopy3_clp.h"

#define VERSION "3.0"

int main (int argc, char *argv[])
{
  int c, i;
  FILE *fp, *ofp;
  struct arg_t a;

  Cmdline (&a, argc, argv);

  if (a.v)
    {
      printf ("%s version %s\n", argv[0], VERSION);
      exit (0);
    }

  if (a.o)
    ofp = fopen (a.o, "w");
  else
    ofp = stdout;
  
  if (!a.optind)
    fp = stdin;
  else
    fp = fopen (argv[a.optind],"r");

  for (i = 0; i < a.i; i++)
    {
      while ((c = fgetc (fp)) != EOF)
	fputc (c, ofp);
      rewind (fp);
    }
  
  fclose (fp);
  return 0;
}
