/******************************************************************************
**
** ls_clp.h
**
** Header file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <stdio.h>
#include "coreutils.h"
#include "ls.h"

#ifndef bool
typedef enum bool_t
{
  false = 0, true
} bool;
#endif

/* customized structure for command line parameters */
struct arg_t
{
  bool all;
  bool almost_all;
  bool author;
  bool escape;
  char * block_size;
  int block_size_li;
  bool ignore_backups;
  bool c;
  bool C;
  char * color;
  bool color_flag;
  bool directory;
  bool dired;
  bool f;
  bool classify;
  bool file_type;
  char * format;
  bool full_time;
  bool g;
  bool group_directories_first;
  bool no_group;
  bool human_readable;
  bool si;
  bool dereference_command_line;
  bool dereference_command_line_symlink_to_dir;
  char * hide;
  char * indicator_style;
  bool inode;
  char * ignore;
  bool k;
  bool kilobytes;
  bool l;
  bool dereference;
  bool m;
  bool numeric_uid_gid;
  bool literal;
  bool o;
  bool p;
  bool hide_control_chars;
  bool show_control_chars;
  bool quote_name;
  char * quoting_style;
  bool reverse;
  bool recursive;
  bool size;
  bool S;
  char * sort;
  char * time;
  char * time_style;
  bool t;
  unsigned long int tabsize;
  bool u;
  bool U;
  bool v;
  unsigned long int width;
  bool x;
  bool X;
  bool context;
  bool _1;
  bool help;
  bool version;
  int optind;
};

/* function prototypes */
void Cmdline (struct arg_t *my_args, int argc, char *argv[]);
void usage (int status, char *program_name);
