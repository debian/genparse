/******************************************************************************
**
** rm_clp.c
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "rm_clp.h"

static struct option const long_options[] =
{
  {"directory", no_argument, NULL, 'd'},
  {"force", no_argument, NULL, 'f'},
  {"interactive", no_argument, NULL, 'i'},
  {"recursive", no_argument, NULL, 'r'},
  {"verbose", no_argument, NULL, 'v'},
  {"help", no_argument, NULL, 'h'},
  {"version", no_argument, NULL, 256},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->directory = false;
  my_args->force = false;
  my_args->interactive = false;
  my_args->recursive = false;
  my_args->verbose = false;
  my_args->help = false;
  my_args->version = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "dfirRvh", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'd':
          my_args->directory = true;
          break;

        case 'f':
          my_args->force = true;
          break;

        case 'i':
          my_args->interactive = true;
          break;

        case 'r':
        case 'R':
          my_args->recursive = true;
          break;

        case 'v':
          my_args->verbose = true;
          break;

        case 'h':
          my_args->help = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 256:
          my_args->version = true;
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, _("Try `%s --help' for more information.\n"),
            program_name);
  else
    {
      printf (_("\
Remove (unlink) the FILE(s).\n\
\n\
Usage: %s [OPTION]... FILE...\n\
  -d, --directory       unlink directory, even if non-empty (super-user only)\n\
  -f, --force           ignore nonexistant files, never prompt\n\
  -i, --interactive     prompt before any removal\n\
  -r -R, --recursive    remove the contents of directories recursively\n\
  -v, --verbose         explain what is being done\n\
  -h, --help            print this help and exit\n\
      --version         print version information and exit\n\
\n\
To remove a file whose name starts with a `-', for example `-foo',\n\
use one of these commands:\n\
  %s -- -foo\n\
\n\
  %s ./-foo\n\
\n\
Note that if you use rm to remove a file, it is usually possible to recover\n\
the contents of that file.  If you want more assurance that the contents are\n\
truly unrecoverable, consider using shred.\n"), program_name, program_name, program_name);
      emit_bug_reporting_address ();
    }
  exit (status);
}
