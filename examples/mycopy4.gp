/* mycopy4.gp */
#include "mycopy4.h"
my_callback()
i / iterations 	int	1  	[1..MAX]  "Number of times to output <file>." 
	"do it like this"
o / outfile     string  {""} outfile_cb () "Output file."  

#usage_begin
usage: __PROGRAM_NAME__ __OPTIONS_SHORT__ file
Print a file for a number of times to stdout.
__GLOSSARY__
#usage_end
