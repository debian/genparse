/******************************************************************************
**
** tail_clp.c
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "error.h"
#include "xstrtol.h"
#include "xstrtod.h"
#include "c-strtod.h"
#include "tail_clp.h"

static struct option const long_options[] =
{
  {"retry", no_argument, NULL, 256},
  {"bytes", required_argument, NULL, 'c'},
  {"follow", optional_argument, NULL, 'f'},
  {"lines", required_argument, NULL, 'n'},
  {"max-unchanged-stats", required_argument, NULL, 257},
  {"pid", required_argument, NULL, 258},
  {"quiet", no_argument, NULL, 'q'},
  {"silent", no_argument, NULL, 259},
  {"sleep-interval", required_argument, NULL, 's'},
  {"verbose", no_argument, NULL, 'v'},
  {"help", no_argument, NULL, 260},
  {"version", no_argument, NULL, 261},
  {"-presume-input-pipe", no_argument, NULL, 262},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->retry = false;
  my_args->bytes = NULL;
  my_args->follow = NULL;
  my_args->follow_flag = false;
  my_args->F = false;
  my_args->lines = NULL;
  my_args->max_unchanged_stats = 0;
  my_args->pid = 0;
  my_args->quiet = false;
  my_args->silent = false;
  my_args->sleep_interval = 0;
  my_args->verbose = false;
  my_args->help = false;
  my_args->version = false;
  my_args->_presume_input_pipe = false;
  my_args->_0 = false;
  my_args->_1 = false;
  my_args->_2 = false;
  my_args->_3 = false;
  my_args->_4 = false;
  my_args->_5 = false;
  my_args->_6 = false;
  my_args->_7 = false;
  my_args->_8 = false;
  my_args->_9 = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "c:fFn:qs:v0123456789", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 256:
          my_args->retry = true;
          break;

        case 'c':
          my_args->bytes = optarg;
          break;

        case 'f':
          my_args->follow_flag = true;
          if (optarg != NULL)
            my_args->follow = optarg;
          break;

        case 'F':
          my_args->F = true;
          break;

        case 'n':
          my_args->lines = optarg;
          break;

        case 257:
          if (xstrtoumax (optarg, NULL, 10, &my_args->max_unchanged_stats, NULL) != LONGINT_OK)
            error (EXIT_FAILURE, 0, _("%s: invalid maximum number of unchanged stats between opens"), optarg);
          break;

        case 258:
          if (xstrtoul (optarg, NULL, 10, &my_args->pid, NULL) != LONGINT_OK
              || my_args->pid > PID_T_MAX)
            error (EXIT_FAILURE, 0, _("%s: invalid PID"), optarg);
          break;

        case 'q':
          my_args->quiet = true;
          break;

        case 259:
          my_args->silent = true;
          break;

        case 's':
          if (! xstrtod (optarg, NULL, &my_args->sleep_interval, c_strtod)
              || my_args->sleep_interval < 0)
            error (EXIT_FAILURE, 0, _("%s: invalid number of seconds"), optarg);
          break;

        case 'v':
          my_args->verbose = true;
          break;

        case 260:
          my_args->help = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 261:
          my_args->version = true;
          break;

        case 262:
          my_args->_presume_input_pipe = true;
          break;

        case '0':
          my_args->_0 = true;
          break;

        case '1':
          my_args->_1 = true;
          break;

        case '2':
          my_args->_2 = true;
          break;

        case '3':
          my_args->_3 = true;
          break;

        case '4':
          my_args->_4 = true;
          break;

        case '5':
          my_args->_5 = true;
          break;

        case '6':
          my_args->_6 = true;
          break;

        case '7':
          my_args->_7 = true;
          break;

        case '8':
          my_args->_8 = true;
          break;

        case '9':
          my_args->_9 = true;
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, _("Try `%s --help' for more information.\n"),
            program_name);
  else
    {
      printf (_("\
Usage: %s [OPTION]... [FILE]...\n\
Print the last %d lines of each FILE to standard output.\n\
With more than one FILE, precede each with a header giving the file name.\n\
With no FILE, or when FILE is -, read standard input.\n"), program_name, DEFAULT_N_LINES);
      puts (_("\
Mandatory arguments to long options are mandatory for short options too."));
      puts (_("\
      --retry              keep trying to open a file even if it is\n\
                           inaccessible when tail starts or if it becomes\n\
                           inaccessible later; useful when following by name,\n\
                           i.e., with --follow=name\n\
  -c, --bytes=N            output the last N bytes; alternatively, use +N to\n\
                           output bytes starting with the Nth of each file"));
      puts (_("\
  -f, --follow[={name|descriptor}]\n\
                           output appended data as the file grows;\n\
                           -f, --follow, and --follow=descriptor are\n\
                           equivalent\n\
  -F                       same as --follow=name --retry"));
      printf (_("\
  -n, --lines=N            output the last N lines, instead of the last %d;\n\
                           or use +N to output lines starting with the Nth\n\
      --max-unchanged-stats=N\n\
                           with --follow=name, reopen a FILE which has not\n\
                           changed size after N (default %d) iterations\n\
                           to see if it has been unlinked or renamed\n\
                           (this is the usual case of rotated log files)\n"), DEFAULT_N_LINES, DEFAULT_MAX_N_UNCHANGED_STATS_BETWEEN_OPENS);
      puts (_("\
      --pid=PID            with -f, terminate after process ID, PID dies\n\
  -q, --quiet              never output headers giving file names\n\
      --silent             same as --quiet\n\
  -s, --sleep-interval=S   with -f, sleep for approximately S seconds\n\
                           (default 1.0) between iterations.\n\
  -v, --verbose            always output headers giving file names"));
      puts (_("\
      --help               display this help and exit"));
      puts (_("\
      --version            output version information and exit"));
      puts (_("\
\n\
If the first character of N (the number of bytes or lines) is a `+',\n\
print beginning with the Nth item from the start of each file, otherwise,\n\
print the last N items in the file.  N may have a multiplier suffix:\n\
b 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,\n\
GB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y."));
      puts (_("\
\n\
With --follow (-f), tail defaults to following the file descriptor, which\n\
means that even if a tail'ed file is renamed, tail will continue to track\n\
its end.  This default behavior is not desirable when you really want to\n\
track the actual name of the file, not the file descriptor (e.g., log\n\
rotation).  Use --follow=name in that case.  That causes tail to track the\n\
named file by reopening it periodically to see if it has been removed and\n\
recreated by some other program."));
      emit_bug_reporting_address ();
    }
  exit (status);
}
