/******************************************************************************
**
** mycopy3_clp.h
**
** Sun Nov 13 11:28:45 2016
** Linux 4.6.0 (#7 Fri Jun 17 22:37:23 CEST 2016) i686
** linux@mgpc (Michael Geng)
**
** Header file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <stdio.h>

#ifndef bool
typedef enum bool_t
{
  false = 0, true
} bool;
#endif

/* customized structure for command line parameters */
struct arg_t
{
  int i;
  char * o;
  bool h;
  bool v;
  int optind;
};

/* function prototypes */
void Cmdline (struct arg_t *my_args, int argc, char *argv[]);
void usage (int status, char *program_name);
