#define _(msgid) msgid

#define PACKAGE_BUGREPORT "bug-coreutils@gnu.org"

static inline void
emit_bug_reporting_address (void)
{
  /* TRANSLATORS: The placeholder indicates the bug-reporting address
     for this package.  Please add _another line_ saying
     "Report translation bugs to <...>\n" with the address for translation
     bugs (typically your translation team's web or email address).  */
  printf (_("\nReport bugs to <%s>.\n"), PACKAGE_BUGREPORT);
}

static inline char *quotearg (char *s)
{
  static char rval[10];

  sprintf (rval, "\"\\\"%s\\\"\"", s);
  return (rval);
}
