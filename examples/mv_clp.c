/******************************************************************************
**
** mv_clp.c
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "mv_clp.h"

static struct option const long_options[] =
{
  {"backup", required_argument, NULL, 256},
  {"force", no_argument, NULL, 'f'},
  {"interactive", no_argument, NULL, 'i'},
  {"strip-trailing-slashes", no_argument, NULL, 257},
  {"suffix", no_argument, NULL, 's'},
  {"target-directory", no_argument, NULL, 258},
  {"update", no_argument, NULL, 'u'},
  {"verbose", no_argument, NULL, 'v'},
  {"help", no_argument, NULL, 'h'},
  {"version", no_argument, NULL, 259},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->backup = NULL;
  my_args->b = false;
  my_args->force = false;
  my_args->interactive = false;
  my_args->strip_trailing_slashes = false;
  my_args->suffix = false;
  my_args->target_directory = false;
  my_args->update = false;
  my_args->verbose = false;
  my_args->help = false;
  my_args->version = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "bfisuvh", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 256:
          my_args->backup = optarg;
          break;

        case 'b':
          my_args->b = true;
          break;

        case 'f':
          my_args->force = true;
          break;

        case 'i':
          my_args->interactive = true;
          break;

        case 257:
          my_args->strip_trailing_slashes = true;
          break;

        case 's':
          my_args->suffix = true;
          break;

        case 258:
          my_args->target_directory = true;
          break;

        case 'u':
          my_args->update = true;
          break;

        case 'v':
          my_args->verbose = true;
          break;

        case 'h':
          my_args->help = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 259:
          my_args->version = true;
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, _("Try `%s --help' for more information.\n"),
            program_name);
  else
    {
      printf (_("\
Usage: %s [OPTION]... SOURCE DEST\n\
  or:  %s [OPTION]... SOURCE... DIRECTORY\n\
  or:  %s [OPTION]... --target-directory=DIRECTORY SOURCE...\n\
Rename SOURCE to DEST, or move SOURCE(s) to DIRECTORY.\n\
\n\
      --backup=CONTROL         make a backup of each existing destination file\n\
  -b                           like --backup but does not accept an argument\n\
  -f, --force                  never prompt before overwriting\n\
  -i, --interactive            prompt before overwrite\n\
      --strip-trailing-slashes  remove any trailing slashes from each SOURCE\n\
                                 argument\n\
  -s, --suffix=SUFFIX          override the usual backup suffix\n\
      --target-directory       move all SOURCE arguments into DIRECTORY\n\
  -u, --update                 move only older or brand new non-directories\n\
  -v, --verbose                explain what is being done\n\
  -h, --help                   display this help and exit\n\
      --version                output version information and exit\n\
\n\
The backup suffix is `~', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.\n\
The version control method may be selected via the --backup option or through\n\
the VERSION_CONTROL environment variable.  Here are the values:\n\
\n\
  none, off       never make backups (even if --backup is given)\n\
  numbered, t     make numbered backups\n\
  existing, nil   numbered if numbered backups exist, simple otherwise\n\
  simple, never   always make simple backups\n"), program_name, program_name, program_name);
      emit_bug_reporting_address ();
    }
  exit (status);
}
