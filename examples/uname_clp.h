/******************************************************************************
**
** uname_clp.h
**
** Header file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <stdio.h>
#include "coreutils.h"

#ifndef bool
typedef enum bool_t
{
  false = 0, true
} bool;
#endif

/* customized structure for command line parameters */
struct arg_t
{
  bool all;
  bool kernel_name;
  bool nodename;
  bool kernel_version;
  bool kernel_release;
  bool machine;
  bool processor;
  bool version;
  bool help;
  int optind;
};

/* function prototypes */
void Cmdline (struct arg_t *my_args, int argc, char *argv[]);
void usage (int status, char *program_name);
