/******************************************************************************
**
** mv_clp.h
**
** Header file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <stdio.h>
#include "coreutils.h"

#ifndef bool
typedef enum bool_t
{
  false = 0, true
} bool;
#endif

/* customized structure for command line parameters */
struct arg_t
{
  char * backup;
  bool b;
  bool force;
  bool interactive;
  bool strip_trailing_slashes;
  bool suffix;
  bool target_directory;
  bool update;
  bool verbose;
  bool help;
  bool version;
  int optind;
};

/* function prototypes */
void Cmdline (struct arg_t *my_args, int argc, char *argv[]);
void usage (int status, char *program_name);
