/* tail.c */

#include <stdio.h>
#include "tail_clp.h"

int main (int argc, char *argv[])
{
  struct arg_t a;

  Cmdline (&a, argc, argv);

  if (a.help)
    usage (0, argv[0]);
  else
    printf ("This is only a demo program.\n");

  return 0;
}
