/******************************************************************************
**
** echo_clp.c
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "echo_clp.h"

static struct option const long_options[] =
{
  {"help", no_argument, NULL, 'h'},
  {"version", no_argument, NULL, 'v'},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->n = false;
  my_args->e = false;
  my_args->E = false;
  my_args->help = false;
  my_args->version = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "neEhv", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'n':
          my_args->n = true;
          break;

        case 'e':
          my_args->e = true;
          break;

        case 'E':
          my_args->E = true;
          break;

        case 'h':
          my_args->help = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 'v':
          my_args->version = true;
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, _("Try `%s --help' for more information.\n"),
            program_name);
  else
    {
      printf (_("\
Usage: %s [OPTION]... [STRING]...\n\
Echo the STRING(s) to standard output.\n\
\n\
  -n                    do not output the trailing newline\n\
  -e                    enable interpretation of backslash escapes (default)\n\
  -E                    disable interpretation of backslash escapes\n\
  -h, --help            display this help and exit\n\
  -v, --version         output version information and exit\n\
\n\
If -e is in effect, the following sequences are recognized:\n\
\n\
  \\0NNN   the character whose ASCII code is NNN (octal)\n\
  \\\\     backslash\n\
  \\a     alert (BEL)\n\
  \\b     backspace\n\
  \\c     suppress trailing newline\n\
  \\f     form feed\n\
  \\n     new line\n\
  \\r     carriage return\n\
  \\t     horizontal tab\n\
  \\v     vertical tab\n\
\n\
NOTE: your shell may have its own version of %s, which usually supersedes\n\
the version described here.  Please refer to your shell's documentation\n\
for details about the options it supports.\n"), program_name, program_name);
      emit_bug_reporting_address ();
    }
  exit (status);
}
