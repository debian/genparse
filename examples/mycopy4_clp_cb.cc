/******************************************************************************
**
** mycopy4_clp_cb.cc
**
** Sun Nov 13 11:28:46 2016
** Linux 4.6.0 (#7 Fri Jun 17 22:37:23 CEST 2016) i686
** linux@mgpc (Michael Geng)
**
** Callbacks for command line parser class
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include "mycopy4_clp.h"

/*----------------------------------------------------------------------------
**
** Cmdline::my_callback ()
**
** Global callback.
**
**--------------------------------------------------------------------------*/

bool Cmdline::my_callback ()
{
  return true;
}

/*----------------------------------------------------------------------------
**
** Cmdline::outfile_cb ()
**
** Parameter callback.
**
**--------------------------------------------------------------------------*/

bool Cmdline::outfile_cb ()
{
  return true;
}
