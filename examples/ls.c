/* ls.c */

#include <stdio.h>
#include <stdlib.h>
#include "ls_clp.h"

int main (int argc, char *argv[])
{
  struct arg_t a;

  Cmdline (&a, argc, argv);

  if (a.help)
    usage (EXIT_SUCCESS, argv[0]);
  else
    printf ("This is only a demo program.\n");

  return 0;
}
