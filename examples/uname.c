/* uname.c */

#include <stdio.h>
#include "uname_clp.h"

int main (int argc, char *argv[])
{
  struct arg_t a;

  Cmdline (&a, argc, argv);

  printf ("This is only a demo program.\n");

  return 0;
}
