/******************************************************************************
**
** ls_clp.c
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "error.h"
#include "xstrtol.h"
#include "ls_clp.h"

static struct option const long_options[] =
{
  {"all", no_argument, NULL, 'a'},
  {"almost-all", no_argument, NULL, 'A'},
  {"author", no_argument, NULL, 256},
  {"escape", no_argument, NULL, 'b'},
  {"block-size", required_argument, NULL, 257},
  {"ignore-backups", no_argument, NULL, 'B'},
  {"color", optional_argument, NULL, 258},
  {"directory", no_argument, NULL, 'd'},
  {"dired", no_argument, NULL, 'D'},
  {"classify", no_argument, NULL, 'F'},
  {"file-type", no_argument, NULL, 259},
  {"format", required_argument, NULL, 260},
  {"full-time", no_argument, NULL, 261},
  {"group-directories-first", no_argument, NULL, 262},
  {"no-group", no_argument, NULL, 'G'},
  {"human-readable", no_argument, NULL, 'h'},
  {"si", no_argument, NULL, 263},
  {"dereference-command-line", no_argument, NULL, 'H'},
  {"dereference-command-line-symlink-to-dir", no_argument, NULL, 264},
  {"hide", required_argument, NULL, 265},
  {"indicator-style", required_argument, NULL, 266},
  {"inode", no_argument, NULL, 'i'},
  {"ignore", required_argument, NULL, 'I'},
  {"kilobytes", no_argument, NULL, 267},
  {"dereference", no_argument, NULL, 'L'},
  {"numeric-uid-gid", no_argument, NULL, 'n'},
  {"literal", no_argument, NULL, 'N'},
  {"hide-control-chars", no_argument, NULL, 'q'},
  {"show-control-chars", no_argument, NULL, 268},
  {"quote-name", no_argument, NULL, 'Q'},
  {"quoting-style", required_argument, NULL, 269},
  {"reverse", no_argument, NULL, 'r'},
  {"recursive", no_argument, NULL, 'R'},
  {"size", no_argument, NULL, 's'},
  {"sort", required_argument, NULL, 270},
  {"time", required_argument, NULL, 271},
  {"time-style", required_argument, NULL, 272},
  {"tabsize", required_argument, NULL, 'T'},
  {"width", required_argument, NULL, 'w'},
  {"context", no_argument, NULL, 'Z'},
  {"help", no_argument, NULL, 273},
  {"version", no_argument, NULL, 274},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->all = false;
  my_args->almost_all = false;
  my_args->author = false;
  my_args->escape = false;
  my_args->block_size = NULL;
  my_args->block_size_li = 0;
  my_args->ignore_backups = false;
  my_args->c = false;
  my_args->C = false;
  my_args->color = NULL;
  my_args->color_flag = false;
  my_args->directory = false;
  my_args->dired = false;
  my_args->f = false;
  my_args->classify = false;
  my_args->file_type = false;
  my_args->format = NULL;
  my_args->full_time = false;
  my_args->g = false;
  my_args->group_directories_first = false;
  my_args->no_group = false;
  my_args->human_readable = false;
  my_args->si = false;
  my_args->dereference_command_line = false;
  my_args->dereference_command_line_symlink_to_dir = false;
  my_args->hide = NULL;
  my_args->indicator_style = NULL;
  my_args->inode = false;
  my_args->ignore = NULL;
  my_args->k = false;
  my_args->kilobytes = false;
  my_args->l = false;
  my_args->dereference = false;
  my_args->m = false;
  my_args->numeric_uid_gid = false;
  my_args->literal = false;
  my_args->o = false;
  my_args->p = false;
  my_args->hide_control_chars = false;
  my_args->show_control_chars = false;
  my_args->quote_name = false;
  my_args->quoting_style = NULL;
  my_args->reverse = false;
  my_args->recursive = false;
  my_args->size = false;
  my_args->S = false;
  my_args->sort = NULL;
  my_args->time = NULL;
  my_args->time_style = NULL;
  my_args->t = false;
  my_args->u = false;
  my_args->U = false;
  my_args->v = false;
  my_args->x = false;
  my_args->X = false;
  my_args->context = false;
  my_args->_1 = false;
  my_args->help = false;
  my_args->version = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "aAbBcCdDfFgGhHiI:klLmnNopqQrRsStT:uUvw:xXZ1", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'a':
          my_args->all = true;
          break;

        case 'A':
          my_args->almost_all = true;
          break;

        case 256:
          my_args->author = true;
          break;

        case 'b':
          my_args->escape = true;
          break;

        case 257:
          my_args->block_size = optarg;
          my_args->block_size_li = optind;
          break;

        case 'B':
          my_args->ignore_backups = true;
          break;

        case 'c':
          my_args->c = true;
          break;

        case 'C':
          my_args->C = true;
          break;

        case 258:
          my_args->color_flag = true;
          if (optarg != NULL)
            my_args->color = optarg;
          break;

        case 'd':
          my_args->directory = true;
          break;

        case 'D':
          my_args->dired = true;
          break;

        case 'f':
          my_args->f = true;
          break;

        case 'F':
          my_args->classify = true;
          break;

        case 259:
          my_args->file_type = true;
          break;

        case 260:
          my_args->format = optarg;
          break;

        case 261:
          my_args->full_time = true;
          break;

        case 'g':
          my_args->g = true;
          break;

        case 262:
          my_args->group_directories_first = true;
          break;

        case 'G':
          my_args->no_group = true;
          break;

        case 'h':
          my_args->human_readable = true;
          break;

        case 263:
          my_args->si = true;
          break;

        case 'H':
          my_args->dereference_command_line = true;
          break;

        case 264:
          my_args->dereference_command_line_symlink_to_dir = true;
          break;

        case 265:
          my_args->hide = optarg;
          break;

        case 266:
          my_args->indicator_style = optarg;
          break;

        case 'i':
          my_args->inode = true;
          break;

        case 'I':
          my_args->ignore = optarg;
          break;

        case 'k':
          my_args->k = true;
          break;

        case 267:
          my_args->kilobytes = true;
          break;

        case 'l':
          my_args->l = true;
          break;

        case 'L':
          my_args->dereference = true;
          break;

        case 'm':
          my_args->m = true;
          break;

        case 'n':
          my_args->numeric_uid_gid = true;
          break;

        case 'N':
          my_args->literal = true;
          break;

        case 'o':
          my_args->o = true;
          break;

        case 'p':
          my_args->p = true;
          break;

        case 'q':
          my_args->hide_control_chars = true;
          break;

        case 268:
          my_args->show_control_chars = true;
          break;

        case 'Q':
          my_args->quote_name = true;
          break;

        case 269:
          my_args->quoting_style = optarg;
          break;

        case 'r':
          my_args->reverse = true;
          break;

        case 'R':
          my_args->recursive = true;
          break;

        case 's':
          my_args->size = true;
          break;

        case 'S':
          my_args->S = true;
          break;

        case 270:
          my_args->sort = optarg;
          break;

        case 271:
          my_args->time = optarg;
          break;

        case 272:
          my_args->time_style = optarg;
          break;

        case 't':
          my_args->t = true;
          break;

        case 'T':
          if (xstrtoul (optarg, NULL, 10, &my_args->tabsize, NULL) != LONGINT_OK
              || my_args->tabsize > SIZE_MAX)
            error (LS_FAILURE, 0, _("invalid tab size: %s"), quotearg (optarg));
          break;

        case 'u':
          my_args->u = true;
          break;

        case 'U':
          my_args->U = true;
          break;

        case 'v':
          my_args->v = true;
          break;

        case 'w':
          if (xstrtoul (optarg, NULL, 10, &my_args->width, NULL) != LONGINT_OK
              || my_args->width < 1
              || my_args->width > SIZE_MAX)
            error (LS_FAILURE, 0, _("invalid line width: %s"), quotearg (optarg));
          break;

        case 'x':
          my_args->x = true;
          break;

        case 'X':
          my_args->X = true;
          break;

        case 'Z':
          my_args->context = true;
          break;

        case '1':
          my_args->_1 = true;
          break;

        case 273:
          my_args->help = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 274:
          my_args->version = true;
          break;

        default:
          usage (LS_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (LS_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, _("Try `%s --help' for more information.\n"),
            program_name);
  else
    {
      printf (_("\
Usage: %s [OPTION]... [FILE]...\n"), program_name);
      puts (_("\
List information about the FILEs (the current directory by default).\n\
Sort entries alphabetically if none of -cftuvSUX nor --sort."));
      puts (_("\
Mandatory arguments to long options are mandatory for short options too."));
      puts (_("\
  -a, --all                  do not ignore entries starting with .\n\
  -A, --almost-all           do not list implied . and ..\n\
      --author               with -l, print the author of each file\n\
  -b, --escape               print octal escapes for nongraphic characters"));
      puts (_("\
      --block-size=SIZE      use SIZE-byte blocks\n\
  -B, --ignore-backups       do not list implied entries ending with ~\n\
  -c                         with -lt: sort by, and show, ctime (time of last\n\
                               modification of file status information)\n\
                               with -l: show ctime and sort by name\n\
                               otherwise: sort by ctime"));
      puts (_("\
  -C                         list entries by columns\n\
      --color[=WHEN]         control whether color is used to distinguish file\n\
                               types.  WHEN may be `never', `always', or `auto'\n\
  -d, --directory            list directory entries instead of contents,\n\
                               and do not dereference symbolic links\n\
  -D, --dired                generate output designed for Emacs' dired mode"));
      puts (_("\
  -f                         do not sort, enable -aU, disable -ls --color\n\
  -F, --classify             append indicator (one of /=*>@|) to entries\n\
      --file-type            likewise, except do not append `*'\n\
      --format=WORD          across -x, commas -m, horizontal -x, long -l,\n\
                               single-column -1, verbose -l, vertical -C\n\
      --full-time            like -l --time-style=full-iso"));
      puts (_("\
  -g                         like -l, but do not list owner"));
      puts (_("\
      --group-directories-first\n\
                             group directories before files"));
      puts (_("\
  -G, --no-group             in a long listing, don't print group names\n\
  -h, --human-readable       with -l, print sizes in human readable format\n\
                               (e.g., 1K 234M 2G)\n\
      --si                   likewise, but use powers of 1000 not 1024"));
      puts (_("\
  -H, --dereference-command-line\n\
                             follow symbolic links listed on the command line\n\
      --dereference-command-line-symlink-to-dir\n\
                             follow each command line symbolic link\n\
                             that points to a directory\n\
      --hide=PATTERN         do not list implied entries matching shell PATTERN\n\
                               (overridden by -a or -A)"));
      puts (_("\
      --indicator-style=WORD  append indicator with style WORD to entry names:\n\
                               none (default), slash (-p),\n\
                               file-type (--file-type), classify (-F)\n\
  -i, --inode                print the index number of each file\n\
  -I, --ignore=PATTERN       do not list implied entries matching shell PATTERN\n\
  -k                         like --block-size=1K"));
      puts (_("\
  -l                         use a long listing format\n\
  -L, --dereference          when showing file information for a symbolic\n\
                               link, show information for the file the link\n\
                               references rather than for the link itself\n\
  -m                         fill width with a comma separated list of entries"));
      puts (_("\
  -n, --numeric-uid-gid      like -l, but list numeric user and group IDs\n\
  -N, --literal              print raw entry names (don't treat e.g. control\n\
                               characters specially)\n\
  -o                         like -l, but do not list group information\n\
  -p                         like --indicator-style=slash\n\
                               append / indicator to directories"));
      puts (_("\
  -q, --hide-control-chars   print ? instead of non graphic characters\n\
      --show-control-chars   show non graphic characters as-is (default\n\
                             unless program is `ls' and output is a terminal)\n\
  -Q, --quote-name           enclose entry names in double quotes\n\
      --quoting-style=WORD   use quoting style WORD for entry names:\n\
                               literal, locale, shell, shell-always, c, escape"));
      puts (_("\
  -r, --reverse              reverse order while sorting\n\
  -R, --recursive            list subdirectories recursively\n\
  -s, --size                 print the size of each file, in blocks"));
      puts (_("\
  -S                         sort by file size\n\
      --sort=WORD            sort by WORD instead of name: none -U,\n\
                             extension -X, size -S, time -t, version -v\n\
      --time=WORD            with -l, show time as WORD instead of modification\n\
                             time: atime -u, access -u, use -u, ctime -c,\n\
                             or status -c; use specified time as sort key\n\
                             if --sort=time"));
      puts (_("\
      --time-style=STYLE     with -l, show times using style STYLE:\n\
                             full-iso, long-iso, iso, locale, +FORMAT.\n\
                             FORMAT is interpreted like `date'; if FORMAT is\n\
                             FORMAT1<newline>FORMAT2, FORMAT1 applies to\n\
                             non-recent files and FORMAT2 to recent files;\n\
                             if STYLE is prefixed with `posix-', STYLE\n\
                             takes effect only outside the POSIX locale"));
      puts (_("\
  -t                         sort by modification time\n\
  -T, --tabsize=COLS         assume tab stops at each COLS instead of 8"));
      puts (_("\
  -u                         with -lt: sort by, and show, access time\n\
                               with -l: show access time and sort by name\n\
                               otherwise: sort by access time\n\
  -U                         do not sort; list entries in directory order\n\
  -v                         sort by version"));
      puts (_("\
  -w, --width=COLS           assume screen width instead of current value\n\
  -x                         list entries by lines instead of by columns\n\
  -X                         sort alphabetically by entry extension\n\
  -Z, --context              print any SELinux security context of each file\n\
  -1                         list one file per line"));
      puts (_("\
      --help                 display this help and exit"));
      puts (_("\
      --version              output version information and exit"));
      puts (_("\
\n\
SIZE may be (or may be an integer optionally followed by) one of following:\n\
kB 1000, K 1024, MB 1000*1000, M 1024*1024, and so on for G, T, P, E, Z, Y."));
      puts (_("\
\n\
By default, color is not used to distinguish types of files.  That is\n\
equivalent to using --color=none.  Using the --color option without the\n\
optional WHEN argument is equivalent to using --color=always.  With\n\
--color=auto, color codes are output only if standard output is connected\n\
to a terminal (tty).  The environment variable LS_COLORS can influence the\n\
colors, and can be set easily by the dircolors command."));
      puts (_("\
\n\
Exit status is 0 if OK, 1 if minor problems, 2 if serious trouble."));
      emit_bug_reporting_address ();
    }
  exit (status);
}
