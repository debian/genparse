/******************************************************************************
**
** ping_clp.h
**
** Header file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <stdio.h>
#include <limits.h>
#include "ping.h"

#ifndef bool
typedef enum bool_t
{
  false = 0, true
} bool;
#endif

/* customized structure for command line parameters */
struct arg_t
{
  bool help;
  bool license;
  bool version;
  bool echo;
  bool address;
  bool timestamp;
  char * type;
  bool router;
  char * count;
  bool debug;
  double interval;
  bool interval_flag;
  bool numeric;
  bool ignore_routing;
  bool verbose;
  bool flood;
  unsigned long int preload;
  char * pattern;
  bool quiet;
  bool route;
  char * size;
  int optind;
};

/* function prototypes */
void Cmdline (struct arg_t *my_args, int argc, char *argv[]);
void usage (int status, char *program_name);
