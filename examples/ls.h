/* Exit statuses.  */
enum
  {
    /* "ls" had a minor problem (e.g., it could not stat a directory
       entry).  */
    LS_MINOR_PROBLEM = 1,

    /* "ls" had more serious trouble.  */
    LS_FAILURE = 2
  };
