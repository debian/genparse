#include "coreutils.h"

c / bytes		flag	"print the byte counts"
m / chars		flag	"print the character counts"
l / lines		flag	"print the newline counts__NEW_PRINT__"
NONE / files0-from=F	string	"read input from the files specified by"
				"  NUL-terminated names in file F"
L / max-line-length	flag	"print the length of the longest line"
w / words		flag	"print the word counts__NEW_PRINT__"
#gp_include help_version.gp

#usage_begin
Usage: __PROGRAM_NAME__ [OPTION]... [FILE]...
  or:  __PROGRAM_NAME__ [OPTION]... --files0-from=F__NEW_PRINT__
Print newline, word, and byte counts for each FILE, and a total line if
more than one FILE is specified.  With no FILE, or when FILE is -,
read standard input.
__GLOSSARY_GNU__(25)
__CODE__(emit_bug_reporting_address ();)
#usage_end
