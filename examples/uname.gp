#include "coreutils.h"

a / all			flag	"print all information, in the following order,"
				"  except omit -p and -i if unknown:"
s / kernel-name		flag	"print the kernel name"
n / nodename		flag	"print the network node hostname"
v / kernel-version	flag	"print the kernel name"
r / kernel-release	flag	"print the kernel version"
m / machine		flag	"print the kernel machine hardware name"
p / processor		flag	"print the processor type or "
NONE / version		flag	"print version information and exit"

#usage_begin
Usage: __PROGRAM_NAME__ [OPTION]...
Print certain system information.  With no OPTION, same as -s.

__GLOSSARY_GNU__
__CODE__(emit_bug_reporting_address ();)
#usage_end
