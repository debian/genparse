#include "coreutils.h"
#include "ls.h"

#exit_value LS_FAILURE

a / all				flag	"do not ignore entries starting with ."
A / almost-all			flag	"do not list implied . and .."
NONE / author			flag	"with -l, print the author of each file"
b / escape			flag	"print octal escapes for nongraphic characters__NEW_PRINT__"
NONE / block-size=SIZE		string	"use SIZE-byte blocks"	__STORE_LONGINDEX__
B / ignore-backups		flag	"do not list implied entries ending with ~"
c				flag	"with -lt: sort by, and show, ctime (time of last"
					"  modification of file status information)"
					"  with -l: show ctime and sort by name"
					"  otherwise: sort by ctime__NEW_PRINT__"
C				flag	"list entries by columns"
NONE / color*[=WHEN]		string	"control whether color is used to distinguish file"
					"  types.  WHEN may be `never', `always', or `auto'"
d / directory			flag	"list directory entries instead of contents,"
					"  and do not dereference symbolic links"
D / dired			flag	"generate output designed for Emacs' dired mode__NEW_PRINT__"
f				flag	"do not sort, enable -aU, disable -ls --color"
F / classify			flag	"append indicator (one of /=*>@|) to entries"
NONE / file-type		flag	"likewise, except do not append `*'"
NONE / format=WORD		string	"across -x, commas -m, horizontal -x, long -l,"
					"  single-column -1, verbose -l, vertical -C"
NONE / full-time		flag	"like -l --time-style=full-iso__NEW_PRINT__"
g				flag	"like -l, but do not list owner__NEW_PRINT__"
NONE / group-directories-first	flag	""
					"group directories before files__NEW_PRINT__"
G / no-group			flag	"in a long listing, don't print group names"
h / human-readable		flag	"with -l, print sizes in human readable format"
					"  (e.g., 1K 234M 2G)"
NONE / si			flag	"likewise, but use powers of 1000 not 1024__NEW_PRINT__"
H / dereference-command-line	flag	""
					"follow symbolic links listed on the command line"
NONE / dereference-command-line-symlink-to-dir	flag	""
					"follow each command line symbolic link"
					"that points to a directory"
NONE / hide=PATTERN		string	"do not list implied entries matching shell PATTERN"
					"  (overridden by -a or -A)__NEW_PRINT__"
NONE / indicator-style=WORD	string	" append indicator with style WORD to entry names:"
					"  none (default), slash (-p),"
					"  file-type (--file-type), classify (-F)"
i / inode			flag	"print the index number of each file"
I / ignore=PATTERN		string	"do not list implied entries matching shell PATTERN"
k				flag	"like --block-size=1K__NEW_PRINT__"
NONE / kilobytes		flag	"__DO_NOT_DOCUMENT__"
l				flag	"use a long listing format"
L / dereference			flag	"when showing file information for a symbolic"
					"  link, show information for the file the link"
					"  references rather than for the link itself"
m				flag	"fill width with a comma separated list of entries__NEW_PRINT__"
n / numeric-uid-gid		flag	"like -l, but list numeric user and group IDs"
N / literal			flag	"print raw entry names (don't treat e.g. control"
					"  characters specially)"
o				flag	"like -l, but do not list group information"
p				flag	"like --indicator-style=slash"
					"  append / indicator to directories__NEW_PRINT__"
q / hide-control-chars		flag	"print ? instead of non graphic characters"
NONE / show-control-chars	flag	"show non graphic characters as-is (default"
					"unless program is `ls' and output is a terminal)"
Q / quote-name			flag	"enclose entry names in double quotes"
NONE / quoting-style=WORD	string	"use quoting style WORD for entry names:"
					"  literal, locale, shell, shell-always, c, escape__NEW_PRINT__"
r / reverse			flag	"reverse order while sorting"
R / recursive			flag	"list subdirectories recursively"
s / size			flag	"print the size of each file, in blocks__NEW_PRINT__"
S				flag	"sort by file size"
NONE / sort=WORD		string	"sort by WORD instead of name: none -U,"
					"extension -X, size -S, time -t, version -v"
NONE / time=WORD		string	"with -l, show time as WORD instead of modification"
					"time: atime -u, access -u, use -u, ctime -c,"
					"or status -c; use specified time as sort key"
					"if --sort=time__NEW_PRINT__"
NONE / time-style=STYLE		string	"with -l, show times using style STYLE:"
					"full-iso, long-iso, iso, locale, +FORMAT."
					"FORMAT is interpreted like `date'; if FORMAT is"
					"FORMAT1<newline>FORMAT2, FORMAT1 applies to"
					"non-recent files and FORMAT2 to recent files;"
					"if STYLE is prefixed with `posix-', STYLE"
					"takes effect only outside the POSIX locale__NEW_PRINT__"
t				flag	"sort by modification time"
T / tabsize=COLS		ulong	[..SIZE_MAX]
					"assume tab stops at each COLS instead of 8__NEW_PRINT__"
					__ERR_MSG__("invalid tab size: %s", quotearg)
u				flag	"with -lt: sort by, and show, access time"
					"  with -l: show access time and sort by name"
					"  otherwise: sort by access time"
U				flag	"do not sort; list entries in directory order"
v				flag	"sort by version__NEW_PRINT__"
w / width=COLS			ulong	[1..SIZE_MAX]
					"assume screen width instead of current value"
					__ERR_MSG__("invalid line width: %s", quotearg)
x				flag	"list entries by lines instead of by columns"
X				flag	"sort alphabetically by entry extension"
Z / context			flag	"print any SELinux security context of each file"
1				flag	"list one file per line__NEW_PRINT__"
#gp_include help_version.gp

#usage_begin
Usage: __PROGRAM_NAME__ [OPTION]... [FILE]...__NEW_PRINT__
List information about the FILEs (the current directory by default).
Sort entries alphabetically if none of -cftuvSUX nor --sort.
__NEW_PRINT__
Mandatory arguments to long options are mandatory for short options too.__NEW_PRINT__
__GLOSSARY_GNU__(29)

SIZE may be (or may be an integer optionally followed by) one of following:
kB 1000, K 1024, MB 1000*1000, M 1024*1024, and so on for G, T, P, E, Z, Y.__NEW_PRINT__

By default, color is not used to distinguish types of files.  That is
equivalent to using --color=none.  Using the --color option without the
optional WHEN argument is equivalent to using --color=always.  With
--color=auto, color codes are output only if standard output is connected
to a terminal (tty).  The environment variable LS_COLORS can influence the
colors, and can be set easily by the dircolors command.__NEW_PRINT__

Exit status is 0 if OK, 1 if minor problems, 2 if serious trouble.
__CODE__(emit_bug_reporting_address ();)
#usage_end
