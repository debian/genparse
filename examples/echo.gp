#include "coreutils.h"

n		flag	"do not output the trailing newline"
e		flag	"enable interpretation of backslash escapes (default)"
E		flag	"disable interpretation of backslash escapes"
h / help	flag	"display this help and exit"
v / version	flag	"output version information and exit"

#usage_begin
Usage: __PROGRAM_NAME__ [OPTION]... [STRING]...
Echo the STRING(s) to standard output.

__GLOSSARY_GNU__

If -e is in effect, the following sequences are recognized:

  \\0NNN   the character whose ASCII code is NNN (octal)
  \\\\     backslash
  \\a     alert (BEL)
  \\b     backspace
  \\c     suppress trailing newline
  \\f     form feed
  \\n     new line
  \\r     carriage return
  \\t     horizontal tab
  \\v     vertical tab

NOTE: your shell may have its own version of __PROGRAM_NAME__, which usually supersedes
the version described here.  Please refer to your shell's documentation
for details about the options it supports.
__CODE__(emit_bug_reporting_address ();)
#usage_end
