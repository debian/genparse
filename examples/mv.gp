#include "coreutils.h"

NONE / backup=CONTROL		string	"make a backup of each existing destination file"
b				flag	"like --backup but does not accept an argument"
f / force			flag	"never prompt before overwriting"
i / interactive			flag	"prompt before overwrite"
NONE / strip-trailing-slashes	flag	" remove any trailing slashes from each SOURCE"
					"  argument"
s / suffix=SUFFIX		flag	"override the usual backup suffix"
NONE / target-directory		flag	"move all SOURCE arguments into DIRECTORY"
u / update			flag	"move only older or brand new non-directories"
v / verbose			flag	"explain what is being done"
h / help			flag	"display this help and exit"
NONE / version			flag	"output version information and exit"

#usage_begin
Usage: __PROGRAM_NAME__ [OPTION]... SOURCE DEST
  or:  __PROGRAM_NAME__ [OPTION]... SOURCE... DIRECTORY
  or:  __PROGRAM_NAME__ [OPTION]... --target-directory=DIRECTORY SOURCE...
Rename SOURCE to DEST, or move SOURCE(s) to DIRECTORY.

__GLOSSARY_GNU__(31)

The backup suffix is `~', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.
The version control method may be selected via the --backup option or through
the VERSION_CONTROL environment variable.  Here are the values:

  none, off       never make backups (even if --backup is given)
  numbered, t     make numbered backups
  existing, nil   numbered if numbered backups exist, simple otherwise
  simple, never   always make simple backups
__CODE__(emit_bug_reporting_address ();)
#usage_end
