/* mycopy3.gp */
i / iterations 	int	1	[0...]	"Number of times to output <file>."
					"File should be text format!"
o / outfile	string	{""}		"Output file name."

#usage_begin
usage: __PROGRAM_NAME__ __OPTIONS_SHORT__ file
Print a file for a number of times to stdout.
__GLOSSARY__
#usage_end
