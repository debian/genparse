/*
 * Test program for testing the C output of genparse using a CUnit
 * (http://sourceforge.net/projects/cunit).
 *
 * Copyright (C) 2006 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include "longmembers_clp.h"
#include "genparse_test.h"

static int argc;
static char *argv[100];

void AddTests (void)
{
  if (CU_register_suites (suites) != CUE_SUCCESS) {
    fprintf (stderr, "suite registration failed - %s\n", CU_get_error_msg ());
    exit (- 1);
  }
}

void testNoArgs (void)
{
  struct arg_t Options;
  argc = 0;
  argv[argc++] = "genparse_test";

  Cmdline (&Options, argc, argv);
  CU_ASSERT_EQUAL (          'c', Options.cparam);
  CU_ASSERT_EQUAL (            0, Options.cparam_li);
  CU_ASSERT_EQUAL (       '\013', Options.dparam);
  CU_ASSERT_EQUAL (         '\n', Options.eparam);
  CU_ASSERT_EQUAL (   (float)1.2, Options.fparam);
  CU_ASSERT_EQUAL (            0, Options.fparam_li);
  CU_ASSERT_EQUAL (        false, Options.gparam);
  CU_ASSERT_EQUAL (        false, Options.help);
  CU_ASSERT_EQUAL (            1, Options.iparam);
  CU_ASSERT_EQUAL (            3, Options.oparam);
  CU_ASSERT_EQUAL (        false, Options.oparam_flag);
  CU_ASSERT_EQUAL (         NULL, Options.pparam);
  CU_ASSERT_EQUAL (        false, Options.pparam_flag);
  CU_ASSERT_EQUAL (            4, Options.qparam);
  CU_ASSERT_EQUAL (        false, Options.qparam_flag);
  CU_ASSERT_EQUAL (            7, Options.Pparam);
  CU_ASSERT_EQUAL (        false, Options.Pparam_flag);
  CU_ASSERT_STRING_EQUAL (   "S", Options.Qparam);
  CU_ASSERT_EQUAL (        false, Options.Qparam_flag);
  CU_ASSERT_STRING_EQUAL ( "abc", Options.sparam);
  CU_ASSERT_EQUAL (        false, Options.version);
  CU_ASSERT_STRING_EQUAL ("haha", Options.a_ha);
  CU_ASSERT_EQUAL (        false, Options._1);
  CU_ASSERT_EQUAL (        false, Options.wparam_flag);
  CU_ASSERT_EQUAL (            0, Options.optind);
}

void testShortOptions (void)
{
  struct arg_t Options;
  argc = 0;
  argv[argc++] = "genparse_test";
  argv[argc++] = "-ca";
  argv[argc++] = "-f2";
  argv[argc++] = "-g";
  argv[argc++] = "-i7";
  argv[argc++] = "-o11";
  argv[argc++] = "-pquak";
  argv[argc++] = "-q500";
  argv[argc++] = "-P12";
  argv[argc++] = "-Q";
  argv[argc++] = "-sbla";
  argv[argc++] = "-t17";
  argv[argc++] = "-1";
  argv[argc++] = "-w9";
  argv[argc++] = "myfilename";

  Cmdline (&Options, argc, argv);
  CU_ASSERT_EQUAL (          'a', Options.cparam);
  CU_ASSERT_EQUAL (            2, Options.cparam_li);
  CU_ASSERT_EQUAL (     (float)2, Options.fparam);
  CU_ASSERT_EQUAL (            3, Options.fparam_li);
  CU_ASSERT_EQUAL (         true, Options.gparam);
  CU_ASSERT_EQUAL (        false, Options.help);
  CU_ASSERT_EQUAL (            7, Options.iparam);
  CU_ASSERT_EQUAL (           11, Options.oparam);
  CU_ASSERT_EQUAL (         true, Options.oparam_flag);
  CU_ASSERT_STRING_EQUAL ("quak", Options.pparam);
  CU_ASSERT_EQUAL (         true, Options.pparam_flag);
  CU_ASSERT_EQUAL (          500, Options.qparam);
  CU_ASSERT_EQUAL (         true, Options.qparam_flag);
  CU_ASSERT_EQUAL (           12, Options.Pparam);
  CU_ASSERT_EQUAL (         true, Options.Pparam_flag);
  CU_ASSERT_STRING_EQUAL (   "S", Options.Qparam);
  CU_ASSERT_EQUAL (         true, Options.Qparam_flag);
  CU_ASSERT_STRING_EQUAL ( "bla", Options.sparam);
  CU_ASSERT_EQUAL (           17, Options.t);
  CU_ASSERT_EQUAL (         true, Options._1);
  CU_ASSERT_EQUAL (            9, Options.wparam);
  CU_ASSERT_EQUAL (         true, Options.wparam_flag);
  CU_ASSERT_EQUAL (     argc - 1, Options.optind);
}

void testLongOptions (void)
{
  struct arg_t Options;
  argc = 0;
  argv[argc++] = "genparse_test";
  argv[argc++] = "--cparam=b";
  argv[argc++] = "--fparam=1.5";
  argv[argc++] = "--gparam";
  argv[argc++] = "--iparam=8";
  argv[argc++] = "--oparam=9";
  argv[argc++] = "--pparam=pp";
  argv[argc++] = "--qparam=111";
  argv[argc++] = "--Pparam";
  argv[argc++] = "--Qparam=AAA";
  argv[argc++] = "--sparam=aaa";
  argv[argc++] = "--uparam=18";
  argv[argc++] = "--vparam=19";
  argv[argc++] = "--a-ha=hihi";
  argv[argc++] = "--wparam=99";
  argv[argc++] = "myfilename";

  Cmdline (&Options, argc, argv);
  CU_ASSERT_EQUAL (          'b', Options.cparam);
  CU_ASSERT_EQUAL (            2, Options.cparam_li);
  CU_ASSERT_EQUAL (   (float)1.5, Options.fparam);
  CU_ASSERT_EQUAL (            3, Options.fparam_li);
  CU_ASSERT_EQUAL (         true, Options.gparam);
  CU_ASSERT_EQUAL (        false, Options.help);
  CU_ASSERT_EQUAL (            8, Options.iparam);
  CU_ASSERT_EQUAL (            9, Options.oparam);
  CU_ASSERT_EQUAL (         true, Options.oparam_flag);
  CU_ASSERT_STRING_EQUAL (  "pp", Options.pparam);
  CU_ASSERT_EQUAL (         true, Options.pparam_flag);
  CU_ASSERT_EQUAL (          111, Options.qparam);
  CU_ASSERT_EQUAL (         true, Options.qparam_flag);
  CU_ASSERT_EQUAL (            7, Options.Pparam);
  CU_ASSERT_EQUAL (         true, Options.Pparam_flag);
  CU_ASSERT_STRING_EQUAL ( "AAA", Options.Qparam);
  CU_ASSERT_EQUAL (         true, Options.Qparam_flag);
  CU_ASSERT_STRING_EQUAL ( "aaa", Options.sparam);
  CU_ASSERT_EQUAL (           18, Options.uparam);
  CU_ASSERT_EQUAL (           19, Options.vparam);
  CU_ASSERT_STRING_EQUAL ("hihi", Options.a_ha);
  CU_ASSERT_EQUAL (           99, Options.wparam);
  CU_ASSERT_EQUAL (         true, Options.wparam_flag);
  CU_ASSERT_EQUAL (     argc - 1, Options.optind);
}

void testCallbacks (void)
{
  struct arg_t Options;
  argc = 0;
  argv[argc++] = "genparse_test";
  argv[argc++] = "-c0";
  test_g = 0;
  test_c = 0;
  test_s = 0;
  test_c_g = 0;

  Cmdline (&Options, argc, argv);
  CU_ASSERT_EQUAL ( 1, test_g);
  CU_ASSERT_EQUAL (48, test_c);
  CU_ASSERT_EQUAL ( 0, test_s);
  CU_ASSERT_EQUAL ( 1, test_c_g);
}

void testMultipleShortOptions (void)
{
  struct arg_t Options;
  argc = 0;
  argv[argc++] = "genparse_test";
  argv[argc++] = "-R";

  Cmdline (&Options, argc, argv);
  CU_ASSERT_EQUAL (true, Options.rparam);
}

int my_function (void)
{
}
