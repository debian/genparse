/******************************************************************************
**
** double_clp.c
**
** Sun Nov 13 12:56:09 2016
** Linux 4.6.0 (#7 Fri Jun 17 22:37:23 CEST 2016) i686
** linux@mgpc (Michael Geng)
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "error.h"
#include "xstrtod.h"
#include "xstrtod.h"
#include "double_clp.h"

static struct option const long_options[] =
{
  {"aparam", required_argument, NULL, 'a'},
  {"bparam", required_argument, NULL, 'b'},
  {"cparam", required_argument, NULL, 'c'},
  {"help", no_argument, NULL, 'h'},
  {"version", no_argument, NULL, 'v'},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->a = 0;
  my_args->b = 2;
  my_args->h = false;
  my_args->v = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "a:b:c:hv", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'a':
          if (! xstrtod (optarg, NULL, &my_args->a, strtod))
            error (EXIT_FAILURE, 0, "could not convert %s to double", optarg);
          break;

        case 'b':
          if (! xstrtod (optarg, NULL, &my_args->b, strtod))
            error (EXIT_FAILURE, 0, "could not convert %s to double", optarg);
          if (my_args->b < 2)
            {
              fprintf (stderr, "parameter range error: b must be >= 2\n");
              errflg++;
            }
          if (my_args->b > 5)
            {
              fprintf (stderr, "parameter range error: b must be <= 5\n");
              errflg++;
            }
          break;

        case 'c':
          if (! xstrtod (optarg, NULL, &my_args->c, strtod))
            error (EXIT_FAILURE, 0, "could not convert %s to double", optarg);
          if (my_args->c < 2)
            {
              fprintf (stderr, "parameter range error: c must be >= 2\n");
              errflg++;
            }
          if (my_args->c > 5)
            {
              fprintf (stderr, "parameter range error: c must be <= 5\n");
              errflg++;
            }
          break;

        case 'h':
          my_args->h = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 'v':
          my_args->v = true;
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, "Try `%s --help' for more information.\n",
            program_name);
  else
    {
      printf ("\
usage: %s [ -abchv ] \n\
   [ -a ] [ --aparam ] (type=DOUBLE, default=0)\n\
          This is a double parameter\n\
   [ -b ] [ --bparam ] (type=DOUBLE, range=2...5, default=2)\n\
          This parameter must be in the range 2...5\n\
   [ -c ] [ --cparam ] (type=DOUBLE, range=2...5,)\n\
          This parameter has no default value\n\
   [ -h ] [ --help ] (type=FLAG)\n\
          Display this help and exit.\n\
   [ -v ] [ --version ] (type=FLAG)\n\
          Output version information and exit.\n", program_name);
    }
  exit (status);
}
