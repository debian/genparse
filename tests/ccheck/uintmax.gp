a / aparam	uintmax		0	"This is a uintmax_t parameter"
b / bparam	uintmax	[2...5]	2	"This parameter must be in the range 2...5"
c / cparam	uintmax	[2...5]		"This parameter has no default value"
