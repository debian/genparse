#define MAX 10

#define TEST_TEXT_MACRO "test text"

#define TEST_INT_MACRO 17

/* Undo the internationalization macro */
#define _(x) (x)

extern int my_function(void);
