/*
 * Main program for testing the C output of genparse using a CUnit
 * (http://sourceforge.net/projects/cunit).
 *
 * Copyright (C) 2006 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <CUnit/Basic.h>
#include "gnulib_test.h"

int main (int argc, char* argv[])
{
  if (CU_initialize_registry ()) {
    printf ("\nInitialization of Test Registry failed.");
  }
  else {
    AddTests ();
    CU_set_error_action (CUEA_FAIL);
    printf ("\nTests completed with return value %d.\n", CU_basic_run_tests ());
    CU_cleanup_registry ();
  }

  return 0;
}
