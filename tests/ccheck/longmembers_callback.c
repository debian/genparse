/*
 * Hand edited version of longmembers_clp_cb.c which is generated 
 * automatically by genparse from test.gp.
 *
 * Copyright (C) 2006 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "longmembers_clp.h"

int test_g;
int test_c;
int test_s;
int test_c_g;

/*----------------------------------------------------------------------------
**
** my_callback ()
**
** User defined global callback.
**
**--------------------------------------------------------------------------*/

int my_callback (struct arg_t *a)
{
  test_g = 1;
  test_c_g = 1;
  return true;
}

/*----------------------------------------------------------------------------
**
** char_cb ()
**
** User defined parameter callback.
**
**--------------------------------------------------------------------------*/

int char_cb (char var)
{
  test_c_g = (int)var;
  test_c = (int)var;
  return true;
}

/*----------------------------------------------------------------------------
**
** string_cb ()
**
** User defined parameter callback.
**
**--------------------------------------------------------------------------*/

int string_cb (char * var)
{
  test_s = 3;
  return true;
}

