a / aparam	intmax		0	"This is an intmax_t parameter"
b / bparam	intmax	[2...5]	2	"This parameter must be in the range 2...5"
c / cparam	intmax	[2...5]		"This parameter has no default value"
