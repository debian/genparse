/*
 * Test program for testing the C output of genparse using a CUnit
 * (http://sourceforge.net/projects/cunit).
 *
 * Copyright (C) 2006 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include "ulong_clp.h"
#include "gnulib_test.h"

static int argc;
static char *argv[100];

void AddTests (void)
{
  if (CU_register_suites (suites) != CUE_SUCCESS) {
    fprintf (stderr, "suite registration failed - %s\n", CU_get_error_msg ());
    exit (- 1);
  }
}

void testNoArgs (void)
{
  struct arg_t Options;
  argc = 0;
  argv[argc++] = "genparse_test";

  Cmdline (&Options, argc, argv);
  CU_ASSERT_EQUAL (0, Options.a);
  CU_ASSERT_EQUAL (2, Options.b);
}

void testShortOptions (void)
{
  struct arg_t Options;
  argc = 0;
  argv[argc++] = "genparse_test";
  argv[argc++] = "-a 1";
  argv[argc++] = "-b 2";
  argv[argc++] = "-c 5";

  Cmdline (&Options, argc, argv);
  CU_ASSERT_EQUAL (1, Options.a);
  CU_ASSERT_EQUAL (2, Options.b);
  CU_ASSERT_EQUAL (5, Options.c);
}
