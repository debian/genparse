/******************************************************************************
**
** longmembers_clp.c
**
** Sun Nov 13 12:56:09 2016
** Linux 4.6.0 (#7 Fri Jun 17 22:37:23 CEST 2016) i686
** linux@mgpc (Michael Geng)
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "longmembers_clp.h"

static struct option const long_options[] =
{
  {"cparam", required_argument, NULL, 'c'},
  {"dparam", required_argument, NULL, 'd'},
  {"eparam", required_argument, NULL, 'e'},
  {"fparam", required_argument, NULL, 'f'},
  {"gparam", no_argument, NULL, 'g'},
  {"iparam", required_argument, NULL, 'i'},
  {"oparam", optional_argument, NULL, 'o'},
  {"pparam", required_argument, NULL, 'p'},
  {"qparam", optional_argument, NULL, 'q'},
  {"Pparam", no_argument, NULL, 'P'},
  {"Qparam", optional_argument, NULL, 'Q'},
  {"rparam", no_argument, NULL, 'r'},
  {"sparam", required_argument, NULL, 's'},
  {"uparam", required_argument, NULL, 256},
  {"vparam", required_argument, NULL, 257},
  {"a-ha", required_argument, NULL, 258},
  {"wparam", required_argument, NULL, 'w'},
  {"help", no_argument, NULL, 'h'},
  {"version", no_argument, NULL, 'v'},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->cparam = 'c';
  my_args->cparam_li = 0;
  my_args->dparam = '\013';
  my_args->eparam = '\n';
  my_args->fparam = 1.2;
  my_args->fparam_li = 0;
  my_args->gparam = false;
  my_args->iparam = 1;
  my_args->oparam = 3;
  my_args->oparam_flag = false;
  my_args->pparam = NULL;
  my_args->pparam_flag = false;
  my_args->qparam = 4;
  my_args->qparam_flag = false;
  my_args->Pparam = 7;
  my_args->Pparam_flag = false;
  my_args->Qparam = "S";
  my_args->Qparam_flag = false;
  my_args->rparam = false;
  my_args->sparam = "abc";
  my_args->a_ha = "haha";
  my_args->_1 = false;
  my_args->wparam_flag = false;
  my_args->help = false;
  my_args->version = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "c:d:e:f:gi:o::p::q:P::QrRs:t:1w:hv", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'c':
          my_args->cparam = *optarg;
          my_args->cparam_li = optind;
          if (!char_cb (my_args->cparam))
            usage (EXIT_FAILURE, argv[0]);
          break;

        case 'd':
          my_args->dparam = *optarg;
          if (!char_cb (my_args->dparam))
            usage (EXIT_FAILURE, argv[0]);
          break;

        case 'e':
          my_args->eparam = *optarg;
          break;

        case 'f':
          my_args->fparam = atof (optarg);
          my_args->fparam_li = optind;
          if (my_args->fparam < 1)
            {
              fprintf (stderr, _("parameter range error: fparam must be >= 1\n"));
              errflg++;
            }
          if (my_args->fparam > 2.3)
            {
              fprintf (stderr, _("parameter range error: fparam must be <= 2.3\n"));
              errflg++;
            }
          break;

        case 'g':
          my_args->gparam = true;
          break;

        case 'i':
          my_args->iparam = atoi (optarg);
          if (my_args->iparam < 1)
            {
              fprintf (stderr, _("parameter range error: iparam must be >= 1\n"));
              errflg++;
            }
          if (my_args->iparam > MAX)
            {
              fprintf (stderr, _("parameter range error: iparam must be <= MAX\n"));
              errflg++;
            }
          break;

        case 'o':
          my_args->oparam_flag = true;
          if (optarg != NULL)
            my_args->oparam = atoi (optarg);
          break;

        case 'p':
          my_args->pparam_flag = true;
          if (optarg != NULL)
            my_args->pparam = optarg;
          break;

        case 'q':
          my_args->qparam_flag = true;
          if (optarg != NULL)
            my_args->qparam = atoi (optarg);
          break;

        case 'P':
          my_args->Pparam_flag = true;
          if (optarg != NULL)
            my_args->Pparam = atoi (optarg);
          break;

        case 'Q':
          my_args->Qparam_flag = true;
          if (optarg != NULL)
            my_args->Qparam = optarg;
          break;

        case 'r':
        case 'R':
          my_args->rparam = true;
          break;

        case 's':
          my_args->sparam = optarg;
          if (!string_cb (my_args->sparam))
            usage (EXIT_FAILURE, argv[0]);
          break;

        case 't':
          my_args->t = atoi (optarg);
          break;

        case 256:
          my_args->uparam = atoi (optarg);
          break;

        case 257:
          my_args->vparam = atoi (optarg);
          break;

        case 258:
          my_args->a_ha = optarg;
          break;

        case '1':
          my_args->_1 = true;
          break;

        case 'w':
          my_args->wparam_flag = true;
          if (optarg != NULL)
            my_args->wparam = atoi (optarg);
          break;

        case 'h':
          my_args->help = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 'v':
          my_args->version = true;
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (!my_callback (my_args))
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, _("Try `%s --help' for more information.\n"),
            program_name);
  else
    {
      printf (_("\
%s\n\
\n\
  -c, --cparam          This is a char parameter.\n\
  -d, --dparam          This is a char parameter initialized with an\n\
                        octal value.\n\
  -e, --eparam          This is a char parameter initialized with newline.\n\
  -f, --fparam          This is a float parameter.\n\
  -g, --gparam          This is a flag parameter.\n\
  -i, --iparam          This is an integer parameter.\n\
  -o, --oparam          Both short and long option have an optional\n\
                        argument\n\
  -p, --pparam          Short option has an optional argument,\n\
                        long option requires an argument.\n\
  -q, --qparam          Short option reqires an argument,\n\
                        long option has an optional argument.\n\
  -P, --Pparam          Short option has an optional argument,\n\
                        long option none.\n\
  -Q, --Qparam          Short option has no argument, long option has an\n\
                        optional argument.\n\
  -r -R, --rparam       This parameter has 2 short representations.\n\
  -s, --sparam          This is a string parameter.\n\
  -t                    This parameter only has a short representation\n\
      --uparam          This parameter only has a long representation\n\
      --vparam=V        This parameter has a designation\n\
      --a-ha            This parameter has a dash in its name\n\
  -1                    This is a single digit parameter\n\
  -w, --wparam          This parameter also sets a flag\n\
  -h, --help            Display this help and exit.\n\
  -v, --version         Output version information and exit.\n\
\n\
%s\n\
\n\
%d\n\
\n\
__COMMAND__(my_function ())\n"), program_name, TEST_TEXT_MACRO, TEST_INT_MACRO);
    }
  exit (status);
}
