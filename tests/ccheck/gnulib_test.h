/*
 * Test program for testing the C output of genparse using a CUnit
 * (http://sourceforge.net/projects/cunit).
 *
 * Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENPARSE_TEST_H
#define GENPARSE_TEST_H

#include <CUnit/CUnit.h>

void AddTests (void);

void testNoArgs (void);
void testShortOptions (void);

static CU_TestInfo test_genparse[] = {
  { "testNoArgs",          testNoArgs },
  { "testShortOptions",    testShortOptions },
  CU_TEST_INFO_NULL,
};

static CU_SuiteInfo suites[] = {
  { "TestGenparse", NULL, NULL, NULL, NULL, test_genparse },
  CU_SUITE_INFO_NULL,
};

#endif	// GENPARSE_TEST_H
