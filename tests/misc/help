#!/bin/bash
# Test if -h / --help is generated automatically, even if not 
# specified in the genparse file.

# Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

if test "$VERBOSE" = yes; then
  set -x
fi

. test-lib.sh

echo "" > empty.gp

echo "usage: parse_cl [ -hv ] 
   [ -h ] [ --help ] (type=FLAG)
          Display this help and exit.
   [ -v ] [ --version ] (type=FLAG)
          Output version information and exit." > exp

for language in $all_languages
do
  gen_code empty.gp $language
  for option in -h --help
  do
    if test $language = java; then
      java  -classpath $CLASSPATH:classes simple $option > out || fail=1
    else
      parse_cl $option > out || fail=1
    fi
    cmp out exp || fail=1
  done
done

# -------------------------------------------------------

echo "h flag \"Something else than help\"" > h.gp

echo "usage: parse_cl [ -hv ] 
   [ -h ] (type=FLAG)
          Something else than help
   [ --help ] (type=FLAG)
          Display this help and exit.
   [ -v ] [ --version ] (type=FLAG)
          Output version information and exit." > exp

touch empty

for language in $all_languages
do
  gen_code h.gp $language

  # --help must print a help screen
  if test $language = java; then
    java  -classpath $CLASSPATH:classes simple --help > out || fail=1
  else
    parse_cl --help > out || fail=1
  fi
  cmp out exp || fail=1

  # -h is a user defined function which does nothing in this simple test program
  if test $language = java; then
    java  -classpath $CLASSPATH:classes simple -h > out || fail=1
  else
    parse_cl -h > out || fail=1
  fi
  cmp out empty || fail=1
done

# -------------------------------------------------------

echo "NONE / help flag \"Only --help is defined, not -h\"" > help.gp

echo "usage: parse_cl [ -v ] 
   [ --help ] (type=FLAG)
          Only --help is defined, not -h
   [ -v ] [ --version ] (type=FLAG)
          Output version information and exit." > exp

echo "parse_cl: invalid option -- 'h'
Try \`parse_cl --help' for more information." > exp2

echo "parse_cl: invalid option -- h
Try \`parse_cl --help' for more information." > java_exp2

for language in $all_languages
do
  gen_code help.gp $language

  if test $language = java; then
    # --help must print a help screen
    java  -classpath $CLASSPATH:classes simple --help > out || fail=1
  else
    parse_cl --help > out || fail=1
  fi
  cmp out exp || fail=1

  if test $language = java; then
    # -h is not defined
    java  -classpath $CLASSPATH:classes simple -h 2> out && fail=1
    cmp out java_exp2                                    || fail=1
  else
    parse_cl -h 2>out && fail=1
    cmp out exp2      || fail=1
  fi
done

# -------------------------------------------------------

echo "h / help flag \"Both --help and -h redefined\"" > help.gp

echo "usage: parse_cl [ -hv ] 
   [ -h ] [ --help ] (type=FLAG)
          Both --help and -h redefined
   [ -v ] [ --version ] (type=FLAG)
          Output version information and exit." > exp

for language in $all_languages
do
  gen_code help.gp $language
  for option in -h --help
  do
    if test $language = java; then
      java  -classpath $CLASSPATH:classes simple $option > out || fail=1
    else
      parse_cl $option > out || fail=1
    fi
    cmp out exp || fail=1
  done
done

rm -f h.gp help.gp

exit $fail
