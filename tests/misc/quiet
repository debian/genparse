#!/bin/bash
# Test the -q / --quiet options

# Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

if test "$VERBOSE" = yes; then
  set -x
fi

. test-lib.sh

echo "x flag x_callback()" > x.gp

echo "creating parse_cl.h...done
creating parse_cl.c...done
creating parse_cl_cb.c...done" > c_exp

echo "creating parse_cl.h...done
creating parse_cl.cc...done
creating parse_cl_cb.cc...done" > cc_exp

echo "creating parse_clInterface.java...done
creating CmdlineEx.java...done
creating parse_cl.java...done" > java_exp

touch empty

for option in -q --quiet
do
  $GENPARSE --language c x.gp >out            || fail=1
  cmp out c_exp                               || fail=1

  $GENPARSE --language cc x.gp >out           || fail=1
  cmp out cc_exp                              || fail=1

  $GENPARSE --language java x.gp >out         || fail=1
  cmp out java_exp                            || fail=1

  $GENPARSE $option --language c x.gp >out    || fail=1
  cmp out empty                               || fail=1

  $GENPARSE $option --language cc x.gp >out   || fail=1
  cmp out empty                               || fail=1

  $GENPARSE $option --language java x.gp >out || fail=1
  cmp out empty                               || fail=1
done

rm -f parse_cl* c_exp cc_exp java_exp

exit $fail
