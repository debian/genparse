/******************************************************************************
**
** parse_cl.java
**
** Definition of command line parser class
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

import gnu.getopt.LongOpt;
import gnu.getopt.Getopt;

/*----------------------------------------------------------------------------
**
** class Cmdline ()
**
** Command line parser class.
**
**--------------------------------------------------------------------------*/

public class Cmdline implements CmdlineInterface
{
  /* parameters */
  private boolean _a;
  private boolean _h;
  private boolean _v;

  /* Name of the calling program */
  private String _executable;

  /* next (non-option) parameter */
  private int _optind;

  /* Must be constructed with parameters. */
  public Cmdline (String[] argv) throws CmdlineEx
  {
    /* character returned by optind () */
    int c;

    LongOpt[] longopts = new LongOpt[3];
    longopts[0] = new LongOpt ("help", LongOpt.NO_ARGUMENT, null, 'h');
    longopts[1] = new LongOpt ("version", LongOpt.NO_ARGUMENT, null, 'v');

    _executable = Cmdline.class.getName ();

    /* default values */
    _a = false;
    _h = false;
    _v = false;

    Getopt g = new Getopt (_executable, argv, "ahv", longopts);
    while ((c = g.getopt ()) != -1)
    {
      switch (c)
      {
        case 'a': 
          _a = true;
          break;

        case 'h': 
          _h = true;
          usage (0, _executable);
          break;

        case 'v': 
          _v = true;
          break;

        default:
          usage (-1, _executable);

        }
    } /* while */

    _optind = g.getOptind ();
  }

  public void usage (int status, String program_name)
  {
    if (status != 0)
      {
        System.err.println ("Try `" + program_name + " --help' for more information.");
      }
    else
      {
        /* "comment for parameter a" */
        System.out.println (
"   [ -a ] (type=FLAG)\n" +
"          param a line 1");
        System.out.println (
"          param a line 2\n" +
"   [ -h ] [ --help ] (type=FLAG)\n" +
"          Display this help and exit.\n" +
"   [ -v ] [ --version ] (type=FLAG)\n" +
"          Output version information and exit.");
        /* "comment for parameter a" */
        System.out.println (
"  -a                    param a line 1");
        System.out.println (
"                        param a line 2\n" +
"  -h, --help            Display this help and exit.\n" +
"  -v, --version         Output version information and exit.\n" +
"abc");
        System.out.println (
"def");
      }
    System.exit (status);
  }

  /* return next (non-option) parameter */
  public int next_param () { return _optind; }

  /* getter functions for command line parameters */
  public boolean a () { return _a; }
  public boolean h () { return _h; }
  public boolean v () { return _v; }
}
