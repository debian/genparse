/******************************************************************************
**
** parse_cl.c
**
** C file for command line parser
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "parse_cl.h"

static struct option const long_options[] =
{
  {"help", no_argument, NULL, 'h'},
  {"version", no_argument, NULL, 'v'},
  {NULL, 0, NULL, 0}
};

/*----------------------------------------------------------------------------
**
** Cmdline ()
**
** Parse the argv array of command line parameters
**
**--------------------------------------------------------------------------*/

void Cmdline (struct arg_t *my_args, int argc, char *argv[])
{
  extern char *optarg;
  extern int optind;
  int c;
  int errflg = 0;

  my_args->a = false;
  my_args->h = false;
  my_args->v = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "ahv", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'a':
          my_args->a = true;
          break;

        case 'h':
          my_args->h = true;
          usage (EXIT_SUCCESS, argv[0]);
          break;

        case 'v':
          my_args->v = true;
          break;

        default:
          usage (EXIT_FAILURE, argv[0]);

        }
    } /* while */

  if (errflg)
    usage (EXIT_FAILURE, argv[0]);

  if (optind >= argc)
    my_args->optind = 0;
  else
    my_args->optind = optind;
}

/*----------------------------------------------------------------------------
**
** usage ()
**
** Print out usage information, then exit
**
**--------------------------------------------------------------------------*/

void usage (int status, char *program_name)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, "Try `%s --help' for more information.\n",
            program_name);
  else
    {
      /* "comment for parameter a" */
      puts ("\
   [ -a ] (type=FLAG)\n\
          param a line 1");
      puts ("\
          param a line 2\n\
   [ -h ] [ --help ] (type=FLAG)\n\
          Display this help and exit.\n\
   [ -v ] [ --version ] (type=FLAG)\n\
          Output version information and exit.");
      /* "comment for parameter a" */
      puts ("\
  -a                    param a line 1");
      puts ("\
                        param a line 2\n\
  -h, --help            Display this help and exit.\n\
  -v, --version         Output version information and exit.\n\
abc");
      puts ("\
def");
    }
  exit (status);
}
