/******************************************************************************
**
** parse_cl.cc
**
** Definition of command line parser class
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <getopt.h>
#include <stdlib.h>
#include "parse_cl.h"

/*----------------------------------------------------------------------------
**
** Cmdline::Cmdline ()
**
** Constructor method.
**
**--------------------------------------------------------------------------*/

Cmdline::Cmdline (int argc, char *argv[]) throw (std::string )
{
  extern char *optarg;
  extern int optind;
  int c;

  static struct option long_options[] =
  {
    {"help", no_argument, NULL, 'h'},
    {"version", no_argument, NULL, 'v'},
    {NULL, 0, NULL, 0}
  };

  _program_name += argv[0];

  /* default values */
  _a = false;
  _h = false;
  _v = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "ahv", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'a': 
          _a = true;
          break;

        case 'h': 
          _h = true;
          this->usage (EXIT_SUCCESS);
          break;

        case 'v': 
          _v = true;
          break;

        default:
          this->usage (EXIT_FAILURE);

        }
    } /* while */

  _optind = optind;
}

/*----------------------------------------------------------------------------
**
** Cmdline::usage ()
**
** Print out usage information, then exit.
**
**--------------------------------------------------------------------------*/

void Cmdline::usage (int status)
{
  if (status != EXIT_SUCCESS)
    std::cerr << "Try `" << _program_name << " --help' for more information.\n";
  else
    {
      // "comment for parameter a"
      std::cout << "\
   [ -a ] (type=FLAG)\n\
          param a line 1\n";
      std::cout << "\
          param a line 2\n\
   [ -h ] [ --help ] (type=FLAG)\n\
          Display this help and exit.\n\
   [ -v ] [ --version ] (type=FLAG)\n\
          Output version information and exit.\n";
      // "comment for parameter a"
      std::cout << "\
  -a                    param a line 1\n";
      std::cout << "\
                        param a line 2\n\
  -h, --help            Display this help and exit.\n\
  -v, --version         Output version information and exit.\n\
abc\n";
      std::cout << "\
def\n";
    }
  exit (status);
}
