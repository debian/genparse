#!/bin/bash
# Test the -d (debug) and -f / --logfile options

# Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

if test "$VERBOSE" = yes; then
  set -x
fi

. test-lib.sh

echo "" > empty.gp

# test if -d works
rm -f log genparse.log
$GENPARSE -d empty.gp >/dev/null 2>log || fail=1
test -s log                            || fail=1
test -s genparse.log                   || fail=1

# test if -f works
rm -f log mylog.log
$GENPARSE -d -f mylog.log empty.gp >/dev/null 2>log || fail=1
test -s log                                         || fail=1
test -s mylog.log                                   || fail=1

# test if --logfile works
rm -f log mylog.log
$GENPARSE -d --logfile mylog.log empty.gp >/dev/null 2>log || fail=1
test -s log                                                || fail=1
test -s mylog.log                                          || fail=1

rm -f genparse.log mylog.log log

exit $fail
