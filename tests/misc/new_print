#!/bin/bash
# Test __NEW_PRINT__ and __COMMENT__

# Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

if test "$VERBOSE" = yes; then
  set -x
fi

. test-lib.sh

echo "a flag __COMMENT__(\"comment for parameter a\")
       \"param a line 1__NEW_PRINT__\"
       \"param a line 2\"
#usage_begin
__GLOSSARY__
__GLOSSARY_GNU__
abc__NEW_PRINT__
def
#usage_end" > x.gp

# __NEW_PRINT__ and __COMMENT__ can hardly be checked other than by 
# comparing to a reference output file
$GENPARSE --language c --static x.gp >/dev/null
cmp parse_cl.c $srcdir/new_print.c || fail=1

$GENPARSE --language cc --static x.gp >/dev/null
cmp parse_cl.cc $srcdir/new_print.cc || fail=1

$GENPARSE --language java --static x.gp >/dev/null
cmp parse_cl.java $srcdir/new_print.java || fail=1

exit $fail
