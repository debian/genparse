#!/bin/bash
# Test the -D / --directory options

# Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

if test "$VERBOSE" = yes; then
  set -x
fi

. test-lib.sh

echo "x flag x_callback()" > x.gp

for option in -D --directory
do
  rm -rf d
  mkdir d
  $GENPARSE --language c $option d x.gp >/dev/null    || fail=1
  test -s d/parse_cl.h                                || fail=1
  test -s d/parse_cl.c                                || fail=1
  test -s d/parse_cl_cb.c                             || fail=1

  rm -rf d
  mkdir d
  $GENPARSE --language cc $option d x.gp >/dev/null   || fail=1
  test -s d/parse_cl.h                                || fail=1
  test -s d/parse_cl.cc                               || fail=1
  test -s d/parse_cl_cb.cc                            || fail=1

  rm -rf d
  mkdir d
  $GENPARSE --language java $option d x.gp >/dev/null || fail=1
  test -s d/parse_clInterface.java                    || fail=1
  test -s d/parse_cl.java                             || fail=1
  test -s d/CmdlineEx.java                            || fail=1
done

rm -rf d

exit $fail
