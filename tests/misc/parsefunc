#!/bin/bash
# Test the -p / --parsefunc options

# Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

if test "$VERBOSE" = yes; then
  set -x
fi

. test-lib.sh

echo "x flag callback_x()" > x.gp

echo "usage: my_parsefunc [ -xhv ] 
   [ -x ] (type=FLAG)
   [ -h ] [ --help ] (type=FLAG)
          Display this help and exit.
   [ -v ] [ --version ] (type=FLAG)
          Output version information and exit." > exp

sed s/Cmdline/my_parsefunc/g $srcdir/simple.c  > my_test.c
sed s/Cmdline/my_parsefunc/g $srcdir/simple.cc > my_test.cc
sed -e s/parse_cl/my_parsefunc/g -e s/simple/my_test/g $srcdir/simple.java > my_test.java

for option in -p --parsefunc
do
  $GENPARSE $option my_parsefunc --language c x.gp >/dev/null    || fail=1
  gcc $CFLAGS -o my_parsefunc parse_cl.c parse_cl_cb.c my_test.c || fail=1
  my_parsefunc --help > out                                      || fail=1
  cmp out exp                                                    || fail=1

  $GENPARSE $option my_parsefunc --language cc x.gp >/dev/null        || fail=1
  g++ $CXXFLAGS -o my_parsefunc parse_cl.cc parse_cl_cb.cc my_test.cc || fail=1
  my_parsefunc --help > out                                           || fail=1
  cmp out exp                                                         || fail=1

  $GENPARSE $option my_parsefunc --language java -o my_parsefunc x.gp >/dev/null || fail=1
  rm -f classes/*                                                                || fail=1
  mkdir -p classes/                                                              || fail=1
  javac -classpath $CLASSPATH:classes -d classes my_parsefuncInterface.java      || fail=1
  javac -classpath $CLASSPATH:classes -d classes my_parsefuncEx.java             || fail=1
  javac -classpath $CLASSPATH:classes -d classes my_parsefunc.java               || fail=1
  javac -classpath $CLASSPATH:classes -d classes my_test.java                    || fail=1
  java  -classpath $CLASSPATH:classes my_test --help > out                       || fail=1
  cmp out exp                                                                    || fail=1
done

rm -f my_parsefunc* my_test.*

exit $fail
