#!/bin/bash
# Test __STRING__

# Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

if test "$VERBOSE" = yes; then
  set -x
fi

. test-lib.sh

echo "#define STRING_MACRO \"text B\"" > x.h

echo "#include \"x.h\"
a flag \"__STRING__(STRING_MACRO)\"
#usage_begin
__GLOSSARY__
__GLOSSARY_GNU__
__STRING__(\"text A\")
__STRING__(STRING_MACRO)
__STRING__ ( \"text A\" )
__STRING__ ( STRING_MACRO )
__STRING__(\"text A\") __STRING__(STRING_MACRO)
#usage_end" > x.gp

echo "   [ -a ] (type=FLAG)
          text B
   [ -h ] [ --help ] (type=FLAG)
          Display this help and exit.
   [ -v ] [ --version ] (type=FLAG)
          Output version information and exit.
  -a                    text B
  -h, --help            Display this help and exit.
  -v, --version         Output version information and exit.
text A
text B
text A
text B
text A text B" > exp

for language in c cc
do
  gen_code x.gp $language
  parse_cl --help > out || fail=1
  cmp out exp           || fail=1
done

rm -f x.h

exit $fail
