#!/bin/bash
# Common functions or definitions used by all test scripts

# Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

LC_ALL=C
export LC_ALL

all_languages="c cc java"

GENPARSE=../../src/genparse

error_() { echo "$0: $@" 1>&2; exit 1; }
framework_failure() { error_ 'failure in testing framework'; }

CFLAGS=-I.
CXXFLAGS=-I.
GNULIB_CPPFLAGS=-I$srcdir/../../gnulib/lib
GNULIB_LDFLAGS="-L../../gnulib/lib -lgnu"

# Generate a simple commad line parsing program.
# Parameters: 
# $1: genparse file
# $2: programming language
gen_code()
{
  genparse_file=$1
  language=$2

  if test $language = java; then
    GENPARSE_EXTRA_OPTS="--parsefunc parse_cl"
  else
    GENPARSE_EXTRA_OPTS=""
  fi

  $GENPARSE --language $language --outfile parse_cl \
    $GENPARSE_EXTRA_OPTS $genparse_file >/dev/null                              || fail=1

  case $language in
    c)    gcc $CFLAGS -o parse_cl parse_cl.c  $srcdir/simple.c                  || fail=1
          test -f parse_cl.c                                                    || fail=1
          test -x parse_cl                                                      || fail=1 ;;
    cc)   g++ $CXXFLAGS -o parse_cl parse_cl.cc $srcdir/simple.cc               || fail=1
          test -f parse_cl.cc                                                   || fail=1
          test -x parse_cl                                                      || fail=1 ;;
    java) rm -f classes/*                                                       || fail=1
          mkdir -p classes/                                                     || fail=1
          javac -classpath $CLASSPATH:classes -d classes parse_clInterface.java || fail=1
          javac -classpath $CLASSPATH:classes -d classes parse_clEx.java        || fail=1
          javac -classpath $CLASSPATH:classes -d classes parse_cl.java          || fail=1
          javac -classpath $CLASSPATH:classes -d classes $srcdir/simple.java    || fail=1
          java  -classpath $CLASSPATH:classes simple                            || fail=1 ;;
    *)    fail=1 ;;
  esac
}
