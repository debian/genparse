#!/bin/bash
# Test the -i / --internationalize and -P / --manyprints options

# Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

if test "$VERBOSE" = yes; then
  set -x
fi

. test-lib.sh

echo "#define _(x) x \" y\"" > i.h

echo "#include \"i.h\"
x flag \"Description for x\"
#usage_begin
Introduction__NEW_PRINT__
__GLOSSARY____NEW_PRINT__
extra text
#usage_end" > i.gp

echo "Introduction y
   [ -x ] (type=FLAG)
          Description for x y
   [ -h ] [ --help ] (type=FLAG)
          Display this help and exit. y
   [ -v ] [ --version ] (type=FLAG)
          Output version information and exit. y
extra text y" > exp

for p_option in -P --manyprints
do
  for i_option in -i --internationalize
  do
    $GENPARSE $i_option $p_option --outfile parse_cl i.gp >/dev/null || fail=1
    gcc $CFLAGS -o parse_cl parse_cl.c $srcdir/simple.c              || fail=1
    parse_cl --help > out                                            || fail=1
    cmp out exp                                                      || fail=1
  done
done

echo "Warning: Internationalization not implemented for C++ output" > exp
$GENPARSE --language cpp --internationalize --manyprints --outfile parse_cl i.gp\
  >/dev/null 2>out || fail=1
cmp out exp || fail=1

echo "Warning: Internationalization not implemented for Java output" > exp
$GENPARSE --language java --internationalize --manyprints --outfile parse_cl i.gp\
  >/dev/null 2>out || fail=1
cmp out exp || fail=1

rm -f i.gp i.h

exit $fail
