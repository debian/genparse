/******************************************************************************
**
** longmembers_clp.cc
**
** Sun Nov 13 12:56:09 2016
** Linux 4.6.0 (#7 Fri Jun 17 22:37:23 CEST 2016) i686
** linux@mgpc (Michael Geng)
**
** Definition of command line parser class
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <getopt.h>
#include <stdlib.h>
#include "longmembers_clp.h"

/*----------------------------------------------------------------------------
**
** Cmdline::Cmdline ()
**
** Constructor method.
**
**--------------------------------------------------------------------------*/

Cmdline::Cmdline (int argc, char *argv[]) throw (std::string )
{
  extern char *optarg;
  extern int optind;
  int c;

  static struct option long_options[] =
  {
    {"cparam", required_argument, NULL, 'c'},
    {"dparam", required_argument, NULL, 'd'},
    {"eparam", required_argument, NULL, 'e'},
    {"fparam", required_argument, NULL, 'f'},
    {"gparam", no_argument, NULL, 'g'},
    {"iparam", required_argument, NULL, 'i'},
    {"oparam", optional_argument, NULL, 'o'},
    {"pparam", required_argument, NULL, 'p'},
    {"qparam", optional_argument, NULL, 'q'},
    {"Pparam", no_argument, NULL, 'P'},
    {"Qparam", optional_argument, NULL, 'Q'},
    {"rparam", no_argument, NULL, 'r'},
    {"sparam", required_argument, NULL, 's'},
    {"uparam", required_argument, NULL, 256},
    {"vparam", required_argument, NULL, 257},
    {"a-ha", required_argument, NULL, 258},
    {"wparam", required_argument, NULL, 'w'},
    {"help", no_argument, NULL, 'h'},
    {"version", no_argument, NULL, 'v'},
    {NULL, 0, NULL, 0}
  };

  _program_name += argv[0];

  /* default values */
  _cparam = 'c';
  _cparam_li = 0;
  _dparam = '\013';
  _eparam = '\n';
  _fparam = 1.2;
  _fparam_li = 0;
  _gparam = false;
  _iparam = 1;
  _oparam = 3;
  _oparam_flag = false;
  _pparam_flag = false;
  _qparam = 4;
  _qparam_flag = false;
  _Pparam = 7;
  _Pparam_flag = false;
  _Qparam = "S";
  _Qparam_flag = false;
  _rparam = false;
  _sparam = "abc";
  _a_ha = "haha";
  __1 = false;
  _wparam_flag = false;
  _help = false;
  _version = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "c:d:e:f:gi:o::p::q:P::QrRs:t:1w:hv", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'c': 
          _cparam = *optarg;
          _cparam_li = optind;
          if (!char_cb ())
            this->usage (EXIT_FAILURE);
          break;

        case 'd': 
          _dparam = *optarg;
          if (!char_cb ())
            this->usage (EXIT_FAILURE);
          break;

        case 'e': 
          _eparam = *optarg;
          break;

        case 'f': 
          _fparam = atof (optarg);
          _fparam_li = optind;
          if (_fparam < 1)
            {
              std::string s;
              s += "parameter range error: fparam must be >= 1";
              throw (s);
            }
          if (_fparam > 2.3)
            {
              std::string s;
              s += "parameter range error: fparam must be <= 2.3";
              throw (s);
            }
          break;

        case 'g': 
          _gparam = true;
          break;

        case 'i': 
          _iparam = atoi (optarg);
          if (_iparam < 1)
            {
              std::string s;
              s += "parameter range error: iparam must be >= 1";
              throw (s);
            }
          if (_iparam > MAX)
            {
              std::string s;
              s += "parameter range error: iparam must be <= MAX";
              throw (s);
            }
          break;

        case 'o': 
          _oparam_flag = true;
          if (optarg != NULL)
            _oparam = atoi (optarg);
            break;

        case 'p': 
          _pparam_flag = true;
          if (optarg != NULL)
            _pparam = optarg;
            break;

        case 'q': 
          _qparam_flag = true;
          if (optarg != NULL)
            _qparam = atoi (optarg);
            break;

        case 'P': 
          _Pparam_flag = true;
          if (optarg != NULL)
            _Pparam = atoi (optarg);
            break;

        case 'Q': 
          _Qparam_flag = true;
          if (optarg != NULL)
            _Qparam = optarg;
            break;

        case 'r': 
        case 'R': 
          _rparam = true;
          break;

        case 's': 
          _sparam = optarg;
          if (!string_cb ())
            this->usage (EXIT_FAILURE);
          break;

        case 't': 
          _t = atoi (optarg);
          break;

        case 256: 
          _uparam = atoi (optarg);
          break;

        case 257: 
          _vparam = atoi (optarg);
          break;

        case 258: 
          _a_ha = optarg;
          break;

        case '1': 
          __1 = true;
          break;

        case 'w': 
          _wparam_flag = true;
          if (optarg != NULL)
            _wparam = atoi (optarg);
            break;

        case 'h': 
          _help = true;
          this->usage (EXIT_SUCCESS);
          break;

        case 'v': 
          _version = true;
          break;

        default:
          this->usage (EXIT_FAILURE);

        }
    } /* while */

  _optind = optind;
  if (!my_callback ())
    usage (EXIT_FAILURE);

}

/*----------------------------------------------------------------------------
**
** Cmdline::usage ()
**
** Print out usage information, then exit.
**
**--------------------------------------------------------------------------*/

void Cmdline::usage (int status)
{
  if (status != EXIT_SUCCESS)
    std::cerr << "Try `" << _program_name << " --help' for more information.\n";
  else
    {
      std::cout << "\
" << _program_name << "\n\
\n\
  -c, --cparam          This is a char parameter.\n\
  -d, --dparam          This is a char parameter initialized with an\n\
                        octal value.\n\
  -e, --eparam          This is a char parameter initialized with newline.\n\
  -f, --fparam          This is a float parameter.\n\
  -g, --gparam          This is a flag parameter.\n\
  -i, --iparam          This is an integer parameter.\n\
  -o, --oparam          Both short and long option have an optional\n\
                        argument\n\
  -p, --pparam          Short option has an optional argument,\n\
                        long option requires an argument.\n\
  -q, --qparam          Short option reqires an argument,\n\
                        long option has an optional argument.\n\
  -P, --Pparam          Short option has an optional argument,\n\
                        long option none.\n\
  -Q, --Qparam          Short option has no argument, long option has an\n\
                        optional argument.\n\
  -r -R, --rparam       This parameter has 2 short representations.\n\
  -s, --sparam          This is a string parameter.\n\
  -t                    This parameter only has a short representation\n\
      --uparam          This parameter only has a long representation\n\
      --vparam=V        This parameter has a designation\n\
      --a-ha            This parameter has a dash in its name\n\
  -1                    This is a single digit parameter\n\
  -w, --wparam          This parameter also sets a flag\n\
  -h, --help            Display this help and exit.\n\
  -v, --version         Output version information and exit.\n\
\n\
" << TEST_TEXT_MACRO << "\n\
\n\
" << TEST_INT_MACRO << "\n\
\n\
__COMMAND__(my_function ())\n";
    }
  exit (status);
}
