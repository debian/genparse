#include "test.h"
#mandatory file
#exit_value EXIT_FAILURE
my_callback ()
c / cparam	char	'c'	char_cb ()	"This is a char parameter."	__STORE_LONGINDEX__
d / dparam	char	'\013'	char_cb ()	"This is a char parameter initialized with an"
						"octal value."
e / eparam	char	'\n'			"This is a char parameter initialized with newline."
f / fparam 	float	1.2	[1..2.3]	"This is a float parameter."	__STORE_LONGINDEX__
g / gparam	flag				"This is a flag parameter."
i / iparam	int	1	[1..MAX]	"This is an integer parameter."
o* / oparam*	int	3			"Both short and long option have an optional"
						"argument"
p* / pparam	string				"Short option has an optional argument,"
						"long option requires an argument."
q / qparam*	int	4			"Short option reqires an argument,"
						"long option has an optional argument."
P* / Pparam!	int	7			"Short option has an optional argument,"
						"long option none."
Q!/ Qparam*	string	{"S"}			"Short option has no argument, long option has an"
						"optional argument."
rR / rparam	flag				"This parameter has 2 short representations."
s / sparam	string	{"abc"}	string_cb ()	"This is a string parameter."
t		int				"This parameter only has a short representation"
NONE / uparam	int				"This parameter only has a long representation"
NONE / vparam=V	int				"This parameter has a designation"
NONE / a-ha	string	{"haha"}		"This parameter has a dash in its name"
1		flag				"This is a single digit parameter"
w / wparam	int				"This parameter also sets a flag"
						__ADD_FLAG__

#usage_begin
__PROGRAM_NAME__

__GLOSSARY_GNU__

__STRING__(TEST_TEXT_MACRO)

__INT__(TEST_INT_MACRO)

__COMMAND__(my_function ())
#usage_end
