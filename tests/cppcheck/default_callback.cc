/*
 * Hand edited version of default_clp_cb.cc which is generated 
 * automatically by genparse from test.gp.
 *
 * Copyright (C) 2006 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "default_clp.h"

int test_g;
int test_c;
int test_s;
int test_c_g;

/*----------------------------------------------------------------------------
**
** Cmdline::my_callback ()
**
** Global callback.
**
**--------------------------------------------------------------------------*/

bool Cmdline::my_callback ()
{
  test_g   = 1;
  test_c_g = 1;
  return true;
}

/*----------------------------------------------------------------------------
**
** Cmdline::char_cb ()
**
** Parameter callback.
**
**--------------------------------------------------------------------------*/

bool Cmdline::char_cb ()
{
  test_c_g = (int)c ();
  test_c   = (int)c ();
  return true;
}

/*----------------------------------------------------------------------------
**
** Cmdline::string_cb ()
**
** Parameter callback.
**
**--------------------------------------------------------------------------*/

bool Cmdline::string_cb ()
{
  test_s = 3;
  return true;
}

