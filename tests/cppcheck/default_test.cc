/*
 * Test program for testing the C++ output of genparse using a CppUnit
 * (http://sourceforge.net/projects/cppunit).
 *
 * Copyright (C) 2006 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "default_clp.h"
#include "genparse_test.h"

CPPUNIT_TEST_SUITE_REGISTRATION (genparse_test);

void genparse_test::setUp (void)
{
  argc = 0;
}

void genparse_test::testNoArgs (void)
{
  argv[argc++] = "default_test";

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL (            'c', Options.c ());
  CPPUNIT_ASSERT_EQUAL (              0, Options.c_li ());
  CPPUNIT_ASSERT_EQUAL (         '\013', Options.d ());
  CPPUNIT_ASSERT_EQUAL (           '\n', Options.e ());
  CPPUNIT_ASSERT_EQUAL (     (float)1.2, Options.f ());
  CPPUNIT_ASSERT_EQUAL (              0, Options.f_li ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.g ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.h ());
  CPPUNIT_ASSERT_EQUAL (              1, Options.i ());
  CPPUNIT_ASSERT_EQUAL (              3, Options.o ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.o_flag ());
  CPPUNIT_ASSERT_EQUAL (    string (""), Options.p ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.p_flag ());
  CPPUNIT_ASSERT_EQUAL (              4, Options.q ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.q_flag ());
  CPPUNIT_ASSERT_EQUAL (              7, Options.P ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.P_flag ());
  CPPUNIT_ASSERT_EQUAL (   string ("S"), Options.Q ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.Q_flag ());
  CPPUNIT_ASSERT_EQUAL ( string ("abc"), Options.s ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.v ());
  CPPUNIT_ASSERT_EQUAL (string ("haha"), Options.a_ha ());
  CPPUNIT_ASSERT_EQUAL (          false, Options._1 ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.w_flag ());
  CPPUNIT_ASSERT_EQUAL (              1, Options.next_param ());
}

void genparse_test::testShortOptions (void)
{
  argv[argc++] = "default_test";
  argv[argc++] = "-ca";
  argv[argc++] = "-f2";
  argv[argc++] = "-g";
  argv[argc++] = "-i7";
  argv[argc++] = "-o11";
  argv[argc++] = "-pquak";
  argv[argc++] = "-q500";
  argv[argc++] = "-P12";
  argv[argc++] = "-Q";
  argv[argc++] = "-sbla";
  argv[argc++] = "-t17";
  argv[argc++] = "-1";
  argv[argc++] = "-w9";
  argv[argc++] = "myfilename";

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL (            'a', Options.c ());
  CPPUNIT_ASSERT_EQUAL (              2, Options.c_li ());
  CPPUNIT_ASSERT_EQUAL (       (float)2, Options.f ());
  CPPUNIT_ASSERT_EQUAL (              3, Options.f_li ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.g ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.h ());
  CPPUNIT_ASSERT_EQUAL (              7, Options.i ());
  CPPUNIT_ASSERT_EQUAL (             11, Options.o ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.o_flag ());
  CPPUNIT_ASSERT_EQUAL (string ("quak"), Options.p ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.p_flag ());
  CPPUNIT_ASSERT_EQUAL (            500, Options.q ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.q_flag ());
  CPPUNIT_ASSERT_EQUAL (             12, Options.P ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.P_flag ());
  CPPUNIT_ASSERT_EQUAL (   string ("S"), Options.Q ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.Q_flag ());
  CPPUNIT_ASSERT_EQUAL ( string ("bla"), Options.s ());
  CPPUNIT_ASSERT_EQUAL (             17, Options.t ());
  CPPUNIT_ASSERT_EQUAL (           true, Options._1 ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.w_flag ());
  CPPUNIT_ASSERT_EQUAL (              9, Options.w ());
  CPPUNIT_ASSERT_EQUAL (       argc - 1, Options.next_param ());
}

void genparse_test::testLongOptions (void)
{
  argv[argc++] = "default_test";
  argv[argc++] = "--cparam=b";
  argv[argc++] = "--fparam=1.5";
  argv[argc++] = "--gparam";
  argv[argc++] = "--iparam=8";
  argv[argc++] = "--oparam=9";
  argv[argc++] = "--pparam=pp";
  argv[argc++] = "--qparam=111";
  argv[argc++] = "--Pparam";
  argv[argc++] = "--Qparam=AAA";
  argv[argc++] = "--sparam=aaa";
  argv[argc++] = "--uparam=18";
  argv[argc++] = "--vparam=19";
  argv[argc++] = "--a-ha=hihi";
  argv[argc++] = "--wparam=99";
  argv[argc++] = "myfilename";

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL (            'b', Options.c ());
  CPPUNIT_ASSERT_EQUAL (              2, Options.c_li ());
  CPPUNIT_ASSERT_EQUAL (     (float)1.5, Options.f ());
  CPPUNIT_ASSERT_EQUAL (              3, Options.f_li ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.g ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.h ());
  CPPUNIT_ASSERT_EQUAL (              8, Options.i ());
  CPPUNIT_ASSERT_EQUAL (              9, Options.o ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.o_flag ());
  CPPUNIT_ASSERT_EQUAL (  string ("pp"), Options.p ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.p_flag ());
  CPPUNIT_ASSERT_EQUAL (            111, Options.q ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.q_flag ());
  CPPUNIT_ASSERT_EQUAL (              7, Options.P ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.P_flag ());
  CPPUNIT_ASSERT_EQUAL ( string ("AAA"), Options.Q ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.Q_flag ());
  CPPUNIT_ASSERT_EQUAL ( string ("aaa"), Options.s ());
  CPPUNIT_ASSERT_EQUAL (             18, Options.uparam ());
  CPPUNIT_ASSERT_EQUAL (             19, Options.vparam ());
  CPPUNIT_ASSERT_EQUAL (string ("hihi"), Options.a_ha ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.w_flag ());
  CPPUNIT_ASSERT_EQUAL (             99, Options.w ());
  CPPUNIT_ASSERT_EQUAL (       argc - 1, Options.next_param ());
}

void genparse_test::testOutOfRange1 (void)
{
  argv[argc++] = "default_test";
  argv[argc++] = "-i0";
  CPPUNIT_ASSERT_THROW (Cmdline Options (argc, (char **)argv), string);
}

void genparse_test::testOutOfRange2 (void)
{
  argv[argc++] = "default_test";
  argv[argc++] = "-i11";
  CPPUNIT_ASSERT_THROW (Cmdline Options (argc, (char **)argv), string);
}

void genparse_test::testOutOfRange3 (void)
{
  argv[argc++] = "default_test";
  argv[argc++] = "-f0";
  CPPUNIT_ASSERT_THROW (Cmdline Options (argc, (char **)argv), string);
}

void genparse_test::testOutOfRange4 (void)
{
  argv[argc++] = "default_test";
  argv[argc++] = "-f3";
  CPPUNIT_ASSERT_THROW (Cmdline Options (argc, (char **)argv), string);
}

void genparse_test::testCallbacks (void)
{
  argv[argc++] = "default_test";
  argv[argc++] = "-c2";
  test_g = 0;
  test_c = 0;
  test_s = 0;
  test_c_g = 0;

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL ( 1, test_g);
  CPPUNIT_ASSERT_EQUAL (50, test_c);
  CPPUNIT_ASSERT_EQUAL ( 0, test_s);
  CPPUNIT_ASSERT_EQUAL ( 1, test_c_g);
}

void genparse_test::testMultipleShortOptions (void)
{
  argv[argc++] = "default_test";
  argv[argc++] = "-R";

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL (true, Options.r ());
}

int my_function (void)
{
}
