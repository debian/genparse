/*
 * Test program for testing the C++ output of genparse using a CppUnit
 * (http://sourceforge.net/projects/cppunit).
 *
 * Copyright (C) 2006 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEFAULT_TEST_H
#define DEFAULT_TEST_H

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

using namespace std;

class genparse_test : public CPPUNIT_NS :: TestFixture
{
  CPPUNIT_TEST_SUITE (genparse_test);
  CPPUNIT_TEST (testNoArgs);
  CPPUNIT_TEST (testShortOptions);
  CPPUNIT_TEST (testLongOptions);
  CPPUNIT_TEST (testOutOfRange1);
  CPPUNIT_TEST (testOutOfRange2);
  CPPUNIT_TEST (testOutOfRange3);
  CPPUNIT_TEST (testOutOfRange4);
  CPPUNIT_TEST (testCallbacks);
  CPPUNIT_TEST (testMultipleShortOptions);
  CPPUNIT_TEST_SUITE_END ();

  public:
    void setUp (void);

  protected:
    void testNoArgs (void);
    void testShortOptions (void);
    void testLongOptions (void);
    void testOutOfRange1 (void);
    void testOutOfRange2 (void);
    void testOutOfRange3 (void);
    void testOutOfRange4 (void);
    void testCallbacks (void);
    void testMultipleShortOptions (void);

  private:
    int argc;
    const char *argv[100];
};

extern int test_g;
extern int test_c;
extern int test_s;
extern int test_c_g;

#endif	// DEFAULT_TEST_H
