/*
 * Test program for testing the C++ output of genparse using a CppUnit
 * (http://sourceforge.net/projects/cppunit).
 *
 * Copyright (C) 2006 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "longmembers_clp.h"
#include "genparse_test.h"

CPPUNIT_TEST_SUITE_REGISTRATION (genparse_test);

void genparse_test::setUp (void)
{
  argc = 0;
}

void genparse_test::testNoArgs (void)
{
  argv[argc++] = "longmembers_test";

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL (            'c', Options.cparam ());
  CPPUNIT_ASSERT_EQUAL (              0, Options.cparam_li ());
  CPPUNIT_ASSERT_EQUAL (         '\013', Options.dparam ());
  CPPUNIT_ASSERT_EQUAL (           '\n', Options.eparam ());
  CPPUNIT_ASSERT_EQUAL (     (float)1.2, Options.fparam ());
  CPPUNIT_ASSERT_EQUAL (              0, Options.fparam_li ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.gparam ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.help ());
  CPPUNIT_ASSERT_EQUAL (              1, Options.iparam ());
  CPPUNIT_ASSERT_EQUAL (              3, Options.oparam ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.oparam_flag ());
  CPPUNIT_ASSERT_EQUAL (    string (""), Options.pparam ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.pparam_flag ());
  CPPUNIT_ASSERT_EQUAL (              4, Options.qparam ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.qparam_flag ());
  CPPUNIT_ASSERT_EQUAL (              7, Options.Pparam ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.Pparam_flag ());
  CPPUNIT_ASSERT_EQUAL (   string ("S"), Options.Qparam ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.Qparam_flag ());
  CPPUNIT_ASSERT_EQUAL ( string ("abc"), Options.sparam ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.version ());
  CPPUNIT_ASSERT_EQUAL (string ("haha"), Options.a_ha ());
  CPPUNIT_ASSERT_EQUAL (          false, Options._1 ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.wparam_flag ());
  CPPUNIT_ASSERT_EQUAL (              1, Options.next_param ());
}

void genparse_test::testShortOptions (void)
{
  argv[argc++] = "longmembers_test";
  argv[argc++] = "-ca";
  argv[argc++] = "-f2";
  argv[argc++] = "-g";
  argv[argc++] = "-i7";
  argv[argc++] = "-o11";
  argv[argc++] = "-pquak";
  argv[argc++] = "-q500";
  argv[argc++] = "-P12";
  argv[argc++] = "-Q";
  argv[argc++] = "-sbla";
  argv[argc++] = "-t17";
  argv[argc++] = "-1";
  argv[argc++] = "-w9";
  argv[argc++] = "myfilename";

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL (            'a', Options.cparam ());
  CPPUNIT_ASSERT_EQUAL (              2, Options.cparam_li ());
  CPPUNIT_ASSERT_EQUAL (       (float)2, Options.fparam ());
  CPPUNIT_ASSERT_EQUAL (              3, Options.fparam_li ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.gparam ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.help ());
  CPPUNIT_ASSERT_EQUAL (              7, Options.iparam ());
  CPPUNIT_ASSERT_EQUAL (             11, Options.oparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.oparam_flag ());
  CPPUNIT_ASSERT_EQUAL (string ("quak"), Options.pparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.pparam_flag ());
  CPPUNIT_ASSERT_EQUAL (            500, Options.qparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.qparam_flag ());
  CPPUNIT_ASSERT_EQUAL (             12, Options.Pparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.Pparam_flag ());
  CPPUNIT_ASSERT_EQUAL (   string ("S"), Options.Qparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.Qparam_flag ());
  CPPUNIT_ASSERT_EQUAL ( string ("bla"), Options.sparam ());
  CPPUNIT_ASSERT_EQUAL (             17, Options.t ());
  CPPUNIT_ASSERT_EQUAL (           true, Options._1 ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.wparam_flag ());
  CPPUNIT_ASSERT_EQUAL (              9, Options.wparam ());
  CPPUNIT_ASSERT_EQUAL (       argc - 1, Options.next_param ());
}

void genparse_test::testLongOptions (void)
{
  argv[argc++] = "longmembers_test";
  argv[argc++] = "--cparam=b";
  argv[argc++] = "--fparam=1.5";
  argv[argc++] = "--gparam";
  argv[argc++] = "--iparam=8";
  argv[argc++] = "--oparam=9";
  argv[argc++] = "--pparam=pp";
  argv[argc++] = "--qparam=111";
  argv[argc++] = "--Pparam";
  argv[argc++] = "--Qparam=AAA";
  argv[argc++] = "--sparam=aaa";
  argv[argc++] = "--uparam=18";
  argv[argc++] = "--vparam=19";
  argv[argc++] = "--a-ha=hihi";
  argv[argc++] = "--wparam=99";
  argv[argc++] = "myfilename";

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL (            'b', Options.cparam ());
  CPPUNIT_ASSERT_EQUAL (              2, Options.cparam_li ());
  CPPUNIT_ASSERT_EQUAL (     (float)1.5, Options.fparam ());
  CPPUNIT_ASSERT_EQUAL (              3, Options.fparam_li ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.gparam ());
  CPPUNIT_ASSERT_EQUAL (          false, Options.help ());
  CPPUNIT_ASSERT_EQUAL (              8, Options.iparam ());
  CPPUNIT_ASSERT_EQUAL (              9, Options.oparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.oparam_flag ());
  CPPUNIT_ASSERT_EQUAL (  string ("pp"), Options.pparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.pparam_flag ());
  CPPUNIT_ASSERT_EQUAL (            111, Options.qparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.qparam_flag ());
  CPPUNIT_ASSERT_EQUAL (              7, Options.Pparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.Pparam_flag ());
  CPPUNIT_ASSERT_EQUAL ( string ("AAA"), Options.Qparam ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.Qparam_flag ());
  CPPUNIT_ASSERT_EQUAL ( string ("aaa"), Options.sparam ());
  CPPUNIT_ASSERT_EQUAL (             18, Options.uparam ());
  CPPUNIT_ASSERT_EQUAL (             19, Options.vparam ());
  CPPUNIT_ASSERT_EQUAL (string ("hihi"), Options.a_ha ());
  CPPUNIT_ASSERT_EQUAL (           true, Options.wparam_flag ());
  CPPUNIT_ASSERT_EQUAL (             99, Options.wparam ());
  CPPUNIT_ASSERT_EQUAL (       argc - 1, Options.next_param ());
}

void genparse_test::testOutOfRange1 (void)
{
  argv[argc++] = "longmembers_test";
  argv[argc++] = "-i0";
  CPPUNIT_ASSERT_THROW (Cmdline Options (argc, (char **)argv), string);
}

void genparse_test::testOutOfRange2 (void)
{
  argv[argc++] = "longmembers_test";
  argv[argc++] = "-i11";
  CPPUNIT_ASSERT_THROW (Cmdline Options (argc, (char **)argv), string);
}

void genparse_test::testOutOfRange3 (void)
{
  argv[argc++] = "longmembers_test";
  argv[argc++] = "-f0";
  CPPUNIT_ASSERT_THROW (Cmdline Options (argc, (char **)argv), string);
}

void genparse_test::testOutOfRange4 (void)
{
  argv[argc++] = "longmembers_test";
  argv[argc++] = "-f3";
  CPPUNIT_ASSERT_THROW (Cmdline Options (argc, (char **)argv), string);
}

void genparse_test::testCallbacks (void)
{
  argv[argc++] = "longmembers_test";
  argv[argc++] = "-c2";
  test_g = 0;
  test_c = 0;
  test_s = 0;
  test_c_g = 0;

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL ( 1, test_g);
  CPPUNIT_ASSERT_EQUAL (50, test_c);
  CPPUNIT_ASSERT_EQUAL ( 0, test_s);
  CPPUNIT_ASSERT_EQUAL ( 1, test_c_g);
}

void genparse_test::testMultipleShortOptions (void)
{
  argv[argc++] = "default_test";
  argv[argc++] = "-R";

  Cmdline Options (argc, (char **)argv);
  CPPUNIT_ASSERT_EQUAL (true, Options.rparam ());
}

int my_function (void)
{
}
