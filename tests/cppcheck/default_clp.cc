/******************************************************************************
**
** default_clp.cc
**
** Sun Nov 13 12:56:09 2016
** Linux 4.6.0 (#7 Fri Jun 17 22:37:23 CEST 2016) i686
** linux@mgpc (Michael Geng)
**
** Definition of command line parser class
**
** Automatically created by genparse v0.9.3
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <getopt.h>
#include <stdlib.h>
#include "default_clp.h"

/*----------------------------------------------------------------------------
**
** Cmdline::Cmdline ()
**
** Constructor method.
**
**--------------------------------------------------------------------------*/

Cmdline::Cmdline (int argc, char *argv[]) throw (std::string )
{
  extern char *optarg;
  extern int optind;
  int c;

  static struct option long_options[] =
  {
    {"cparam", required_argument, NULL, 'c'},
    {"dparam", required_argument, NULL, 'd'},
    {"eparam", required_argument, NULL, 'e'},
    {"fparam", required_argument, NULL, 'f'},
    {"gparam", no_argument, NULL, 'g'},
    {"iparam", required_argument, NULL, 'i'},
    {"oparam", optional_argument, NULL, 'o'},
    {"pparam", required_argument, NULL, 'p'},
    {"qparam", optional_argument, NULL, 'q'},
    {"Pparam", no_argument, NULL, 'P'},
    {"Qparam", optional_argument, NULL, 'Q'},
    {"rparam", no_argument, NULL, 'r'},
    {"sparam", required_argument, NULL, 's'},
    {"uparam", required_argument, NULL, 256},
    {"vparam", required_argument, NULL, 257},
    {"a-ha", required_argument, NULL, 258},
    {"wparam", required_argument, NULL, 'w'},
    {"help", no_argument, NULL, 'h'},
    {"version", no_argument, NULL, 'v'},
    {NULL, 0, NULL, 0}
  };

  _program_name += argv[0];

  /* default values */
  _c = 'c';
  _c_li = 0;
  _d = '\013';
  _e = '\n';
  _f = 1.2;
  _f_li = 0;
  _g = false;
  _i = 1;
  _o = 3;
  _o_flag = false;
  _p_flag = false;
  _q = 4;
  _q_flag = false;
  _P = 7;
  _P_flag = false;
  _Q = "S";
  _Q_flag = false;
  _r = false;
  _s = "abc";
  _a_ha = "haha";
  __1 = false;
  _w_flag = false;
  _h = false;
  _v = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "c:d:e:f:gi:o::p::q:P::QrRs:t:1w:hv", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'c': 
          _c = *optarg;
          _c_li = optind;
          if (!char_cb ())
            this->usage (EXIT_FAILURE);
          break;

        case 'd': 
          _d = *optarg;
          if (!char_cb ())
            this->usage (EXIT_FAILURE);
          break;

        case 'e': 
          _e = *optarg;
          break;

        case 'f': 
          _f = atof (optarg);
          _f_li = optind;
          if (_f < 1)
            {
              std::string s;
              s += "parameter range error: f must be >= 1";
              throw (s);
            }
          if (_f > 2.3)
            {
              std::string s;
              s += "parameter range error: f must be <= 2.3";
              throw (s);
            }
          break;

        case 'g': 
          _g = true;
          break;

        case 'i': 
          _i = atoi (optarg);
          if (_i < 1)
            {
              std::string s;
              s += "parameter range error: i must be >= 1";
              throw (s);
            }
          if (_i > MAX)
            {
              std::string s;
              s += "parameter range error: i must be <= MAX";
              throw (s);
            }
          break;

        case 'o': 
          _o_flag = true;
          if (optarg != NULL)
            _o = atoi (optarg);
            break;

        case 'p': 
          _p_flag = true;
          if (optarg != NULL)
            _p = optarg;
            break;

        case 'q': 
          _q_flag = true;
          if (optarg != NULL)
            _q = atoi (optarg);
            break;

        case 'P': 
          _P_flag = true;
          if (optarg != NULL)
            _P = atoi (optarg);
            break;

        case 'Q': 
          _Q_flag = true;
          if (optarg != NULL)
            _Q = optarg;
            break;

        case 'r': 
        case 'R': 
          _r = true;
          break;

        case 's': 
          _s = optarg;
          if (!string_cb ())
            this->usage (EXIT_FAILURE);
          break;

        case 't': 
          _t = atoi (optarg);
          break;

        case 256: 
          _uparam = atoi (optarg);
          break;

        case 257: 
          _vparam = atoi (optarg);
          break;

        case 258: 
          _a_ha = optarg;
          break;

        case '1': 
          __1 = true;
          break;

        case 'w': 
          _w_flag = true;
          if (optarg != NULL)
            _w = atoi (optarg);
            break;

        case 'h': 
          _h = true;
          this->usage (EXIT_SUCCESS);
          break;

        case 'v': 
          _v = true;
          break;

        default:
          this->usage (EXIT_FAILURE);

        }
    } /* while */

  _optind = optind;
  if (!my_callback ())
    usage (EXIT_FAILURE);

}

/*----------------------------------------------------------------------------
**
** Cmdline::usage ()
**
** Print out usage information, then exit.
**
**--------------------------------------------------------------------------*/

void Cmdline::usage (int status)
{
  if (status != EXIT_SUCCESS)
    std::cerr << "Try `" << _program_name << " --help' for more information.\n";
  else
    {
      std::cout << "\
" << _program_name << "\n\
\n\
  -c, --cparam          This is a char parameter.\n\
  -d, --dparam          This is a char parameter initialized with an\n\
                        octal value.\n\
  -e, --eparam          This is a char parameter initialized with newline.\n\
  -f, --fparam          This is a float parameter.\n\
  -g, --gparam          This is a flag parameter.\n\
  -i, --iparam          This is an integer parameter.\n\
  -o, --oparam          Both short and long option have an optional\n\
                        argument\n\
  -p, --pparam          Short option has an optional argument,\n\
                        long option requires an argument.\n\
  -q, --qparam          Short option reqires an argument,\n\
                        long option has an optional argument.\n\
  -P, --Pparam          Short option has an optional argument,\n\
                        long option none.\n\
  -Q, --Qparam          Short option has no argument, long option has an\n\
                        optional argument.\n\
  -r -R, --rparam       This parameter has 2 short representations.\n\
  -s, --sparam          This is a string parameter.\n\
  -t                    This parameter only has a short representation\n\
      --uparam          This parameter only has a long representation\n\
      --vparam=V        This parameter has a designation\n\
      --a-ha            This parameter has a dash in its name\n\
  -1                    This is a single digit parameter\n\
  -w, --wparam          This parameter also sets a flag\n\
  -h, --help            Display this help and exit.\n\
  -v, --version         Output version information and exit.\n\
\n\
" << TEST_TEXT_MACRO << "\n\
\n\
" << TEST_INT_MACRO << "\n\
\n\
__COMMAND__(my_function ())\n";
    }
  exit (status);
}
