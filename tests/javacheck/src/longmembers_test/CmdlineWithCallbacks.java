package longmembers_test;
/*
 * Callback function implementation. The parent class Cmdline
 * was generated automatically by genparse.
 *
 * Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class CmdlineWithCallbacks extends Cmdline
{
  int test_g;
  int test_c;
  int test_s;
  int test_c_g;

  CmdlineWithCallbacks (String[] argv) throws CmdlineEx
  {
    super (argv);
  }

  public boolean my_callback () 
  {
    test_g   = 1;
    test_c_g = 1;
    return true;
  }

  public boolean char_cb ()
  {
    test_c_g = (int)cparam ();
    test_c   = (int)cparam ();
    return true;
  }

  public boolean string_cb ()
  {
    test_s = 3;
    return true;
  }

  int getTest_g ()   { return test_g;   }
  int getTest_c ()   { return test_c;   }
  int getTest_s ()   { return test_s;   }
  int getTest_c_g () { return test_c_g; }
}
