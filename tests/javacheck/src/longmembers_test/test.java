/*
 * Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package longmembers_test;
import junit.framework.*;

public class test extends TestCase
{
  int argc;
  String[] args100 = new String[100];
  String[] args;

  public void setUp ()
  {
  }

  public void tearDown ()
  {
    args = null;
    argc = 0;
  }

  /* Allocate an array of size argc and copy 1st argc elements of args100 into it. 
     The returned array contains argc elements. That's the only difference between the result
     array and the parameter args100. */
  String[] genArgs (int argc, String[] args100)
  {
    args = new String[argc];

    for (int i = 0;i < argc; i++)
    {
      args[i] = args100[i];
    }

    return args;
  }

  public void testNoArgs ()
  {
    args = new String[0];
    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (   'c', Options.cparam ());
    assertEquals ('\013', Options.dparam ());
    assertEquals (  '\n', Options.eparam ());
    assertEquals (  1.2f, Options.fparam (), 0);
    assertEquals ( false, Options.gparam ());
    assertEquals ( false, Options.help ());
    assertEquals (     1, Options.iparam ());
    assertEquals (     3, Options.oparam ());
    assertEquals ( false, Options.oparam_flag ());
    assertEquals (    "", Options.pparam ());
    assertEquals ( false, Options.pparam_flag ());
    assertEquals (     4, Options.qparam ());
    assertEquals ( false, Options.qparam_flag ());
    assertEquals (     7, Options.Pparam ());
    assertEquals ( false, Options.Pparam_flag ());
    assertEquals (   "S", Options.Qparam ());
    assertEquals ( false, Options.Qparam_flag ());
    assertEquals ( "abc", Options.sparam ());
    assertEquals ( false, Options.version ());
    assertEquals ("haha", Options.a_ha ());
    assertEquals ( false, Options._1 ());
    assertEquals ( false, Options.wparam_flag ());
    assertEquals (     0, Options.next_param ());
  }

  public void testShortOptions ()
  {
    args100[argc++] = new String ("-ca");
    args100[argc++] = new String ("-f2");
    args100[argc++] = new String ("-g");
    args100[argc++] = new String ("-i7");
    args100[argc++] = new String ("-o11");
    args100[argc++] = new String ("-pquak");
    args100[argc++] = new String ("-q500");
    args100[argc++] = new String ("-P12");
    args100[argc++] = new String ("-Q");
    args100[argc++] = new String ("-sbla");
    args100[argc++] = new String ("-t17");
    args100[argc++] = new String ("-1");
    args100[argc++] = new String ("-w9");
    args100[argc++] = new String ("myfilename");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (     'a', Options.cparam ());
    assertEquals (      2f, Options.fparam (), 0);
    assertEquals (    true, Options.gparam ());
    assertEquals (   false, Options.help ());
    assertEquals (       7, Options.iparam ());
    assertEquals (      11, Options.oparam ());
    assertEquals (    true, Options.oparam_flag ());
    assertEquals (  "quak", Options.pparam ());
    assertEquals (    true, Options.pparam_flag ());
    assertEquals (     500, Options.qparam ());
    assertEquals (    true, Options.qparam_flag ());
    assertEquals (      12, Options.Pparam ());
    assertEquals (    true, Options.Pparam_flag ());
    assertEquals (     "S", Options.Qparam ());
    assertEquals (    true, Options.Qparam_flag ());
    assertEquals (   "bla", Options.sparam ());
    assertEquals (      17, Options.t ());
    assertEquals (    true, Options._1 ());
    assertEquals (    true, Options.wparam_flag ());
    assertEquals (       9, Options.wparam ());
    assertEquals (argc - 1, Options.next_param ());
  }

  public void testLongOptions ()
  {
    args100[argc++] = new String ("--cparam=b");
    args100[argc++] = new String ("--fparam=1.5");
    args100[argc++] = new String ("--gparam");
    args100[argc++] = new String ("--iparam=8");
    args100[argc++] = new String ("--oparam=9");
    args100[argc++] = new String ("--pparam=pp");
    args100[argc++] = new String ("--qparam=111");
    args100[argc++] = new String ("--Pparam");
    args100[argc++] = new String ("--Qparam=AAA");
    args100[argc++] = new String ("--sparam=aaa");
    args100[argc++] = new String ("--uparam=18");
    args100[argc++] = new String ("--vparam=19");
    args100[argc++] = new String ("--a-ha=hihi");
    args100[argc++] = new String ("--wparam=99");
    args100[argc++] = new String ("myfilename");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (     'b', Options.cparam ());
    assertEquals (    1.5f, Options.fparam (), 0);
    assertEquals (    true, Options.gparam ());
    assertEquals (   false, Options.help ());
    assertEquals (       8, Options.iparam ());
    assertEquals (       9, Options.oparam ());
    assertEquals (    true, Options.oparam_flag ());
    assertEquals (    "pp", Options.pparam ());
    assertEquals (    true, Options.pparam_flag ());
    assertEquals (     111, Options.qparam ());
    assertEquals (    true, Options.qparam_flag ());
    assertEquals (       7, Options.Pparam ());
    assertEquals (    true, Options.Pparam_flag ());
    assertEquals (   "AAA", Options.Qparam ());
    assertEquals (    true, Options.Qparam_flag ());
    assertEquals (   "aaa", Options.sparam ());
    assertEquals (      18, Options.uparam ());
    assertEquals (      19, Options.vparam ());
    assertEquals (  "hihi", Options.a_ha ());
    assertEquals (    true, Options.wparam_flag ());
    assertEquals (      99, Options.wparam ());
    assertEquals (argc - 1, Options.next_param ());
  }

  public void testOutOfRange1 ()
  {
    args100[argc++] = new String ("-i0");
    args = genArgs (argc, args100);

    try
    {
      CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);
      fail ();
    }
    catch (CmdlineEx e) {}
  }

  public void testOutOfRange2 ()
  {
    args100[argc++] = new String ("-i11");
    args = genArgs (argc, args100);

    try
    {
      CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);
      fail ();
    }
    catch (CmdlineEx e) {}
  }

  public void testOutOfRange3 ()
  {
    args100[argc++] = new String ("-f0");
    args = genArgs (argc, args100);

    try
    {
      CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);
      fail ();
    }
    catch (CmdlineEx e) {}
  }

  public void testOutOfRange4 ()
  {
    args100[argc++] = new String ("-f3");
    args = genArgs (argc, args100);

    try
    {
      CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);
      fail ();
    }
    catch (CmdlineEx e) {}
  }

  public void testCallbacks ()
  {
    args100[argc++] = new String ("-c0");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals ( 1, Options.getTest_g ());
    assertEquals (48, Options.getTest_c ());
    assertEquals ( 0, Options.getTest_s ());
    assertEquals ( 1, Options.getTest_c_g ());
  }

  public void testMultipleShortOptions ()
  {
    args100[argc++] = new String ("-R");
    args100[argc++] = new String ("myfilename");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (true, Options.rparam ());
  }
};
