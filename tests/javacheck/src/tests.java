/*
 * Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.*;

public class tests extends TestSuite
{
  public static Test suite ()
  {
    TestSuite suite = new TestSuite ();
    suite.addTest (new TestSuite (default_test.test.class));
    suite.addTest (new TestSuite (longmembers_test.test.class));
    return suite;
  }
};
