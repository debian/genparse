/*
 * Copyright (C) 2007 - 2016 Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package default_test;
import junit.framework.*;

public class test extends TestCase
{
  int argc;
  String[] args100 = new String[100];
  String[] args;

  public void setUp ()
  {
  }

  public void tearDown ()
  {
    args = null;
    argc = 0;
  }

  /* Allocate an array of size argc and copy 1st argc elements of args100 into it. 
     The returned array contains argc elements. That's the only difference between the result
     array and the parameter args100. */
  String[] genArgs (int argc, String[] args100)
  {
    args = new String[argc];

    for (int i = 0;i < argc; i++)
    {
      args[i] = args100[i];
    }

    return args;
  }

  public void testNoArgs ()
  {
    args = new String[0];
    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (   'c', Options.c ());
    assertEquals (     0, Options.c_li ());
    assertEquals ('\013', Options.d ());
    assertEquals (  '\n', Options.e ());
    assertEquals (  1.2f, Options.f (), 0);
    assertEquals (     0, Options.f_li ());
    assertEquals ( false, Options.g ());
    assertEquals ( false, Options.h ());
    assertEquals (     1, Options.i ());
    assertEquals (     3, Options.o ());
    assertEquals ( false, Options.o_flag ());
    assertEquals (    "", Options.p ());
    assertEquals ( false, Options.p_flag ());
    assertEquals (     4, Options.q ());
    assertEquals ( false, Options.q_flag ());
    assertEquals (     7, Options.P ());
    assertEquals ( false, Options.P_flag ());
    assertEquals (   "S", Options.Q ());
    assertEquals ( false, Options.Q_flag ());
    assertEquals ( "abc", Options.s ());
    assertEquals ( false, Options.v ());
    assertEquals ("haha", Options.a_ha ());
    assertEquals ( false, Options._1 ());
    assertEquals ( false, Options.w_flag ());
    assertEquals (     0, Options.next_param ());
  }

  public void testShortOptions ()
  {
    args100[argc++] = new String ("-ca");
    args100[argc++] = new String ("-f2");
    args100[argc++] = new String ("-g");
    args100[argc++] = new String ("-i7");
    args100[argc++] = new String ("-o11");
    args100[argc++] = new String ("-pquak");
    args100[argc++] = new String ("-q500");
    args100[argc++] = new String ("-P12");
    args100[argc++] = new String ("-Q");
    args100[argc++] = new String ("-sbla");
    args100[argc++] = new String ("-t17");
    args100[argc++] = new String ("-1");
    args100[argc++] = new String ("-w9");
    args100[argc++] = new String ("myfilename");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (     'a', Options.c ());
    assertEquals (      2f, Options.f (), 0);
    assertEquals (    true, Options.g ());
    assertEquals (   false, Options.h ());
    assertEquals (       7, Options.i ());
    assertEquals (      11, Options.o ());
    assertEquals (    true, Options.o_flag ());
    assertEquals (  "quak", Options.p ());
    assertEquals (    true, Options.p_flag ());
    assertEquals (     500, Options.q ());
    assertEquals (    true, Options.q_flag ());
    assertEquals (      12, Options.P ());
    assertEquals (    true, Options.P_flag ());
    assertEquals (     "S", Options.Q ());
    assertEquals (    true, Options.Q_flag ());
    assertEquals (   "bla", Options.s ());
    assertEquals (      17, Options.t ());
    assertEquals (    true, Options._1 ());
    assertEquals (    true, Options.w_flag ());
    assertEquals (       9, Options.w ());
    assertEquals (argc - 1, Options.next_param ());
  }

  public void testLongOptions ()
  {
    args100[argc++] = new String ("--cparam=b");
    args100[argc++] = new String ("--fparam=1.5");
    args100[argc++] = new String ("--gparam");
    args100[argc++] = new String ("--iparam=8");
    args100[argc++] = new String ("--oparam=9");
    args100[argc++] = new String ("--pparam=pp");
    args100[argc++] = new String ("--qparam=111");
    args100[argc++] = new String ("--Pparam");
    args100[argc++] = new String ("--Qparam=AAA");
    args100[argc++] = new String ("--sparam=aaa");
    args100[argc++] = new String ("--uparam=18");
    args100[argc++] = new String ("--vparam=19");
    args100[argc++] = new String ("--a-ha=hihi");
    args100[argc++] = new String ("--wparam=99");
    args100[argc++] = new String ("myfilename");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (     'b', Options.c ());
    assertEquals (       0, Options.c_li ());
    assertEquals (    1.5f, Options.f (), 0);
    assertEquals (       3, Options.f_li ());
    assertEquals (    true, Options.g ());
    assertEquals (   false, Options.h ());
    assertEquals (       8, Options.i ());
    assertEquals (       9, Options.o ());
    assertEquals (    true, Options.o_flag ());
    assertEquals (    "pp", Options.p ());
    assertEquals (    true, Options.p_flag ());
    assertEquals (     111, Options.q ());
    assertEquals (    true, Options.q_flag ());
    assertEquals (       7, Options.P ());
    assertEquals (    true, Options.P_flag ());
    assertEquals (   "AAA", Options.Q ());
    assertEquals (    true, Options.Q_flag ());
    assertEquals (   "aaa", Options.s ());
    assertEquals (      18, Options.uparam ());
    assertEquals (      19, Options.vparam ());
    assertEquals (  "hihi", Options.a_ha ());
    assertEquals (    true, Options.w_flag ());
    assertEquals (      99, Options.w ());
    assertEquals (argc - 1, Options.next_param ());
  }

  public void testOutOfRange1 ()
  {
    args100[argc++] = new String ("-i0");
    args = genArgs (argc, args100);

    try
    {
      CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);
      fail ();
    }
    catch (CmdlineEx e) {}
  }

  public void testOutOfRange2 ()
  {
    args100[argc++] = new String ("-i11");
    args = genArgs (argc, args100);

    try
    {
      CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);
      fail ();
    }
    catch (CmdlineEx e) {}
  }

  public void testOutOfRange3 ()
  {
    args100[argc++] = new String ("-f0");
    args = genArgs (argc, args100);

    try
    {
      CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);
      fail ();
    }
    catch (CmdlineEx e) {}
  }

  public void testOutOfRange4 ()
  {
    args100[argc++] = new String ("-f3");
    args = genArgs (argc, args100);

    try
    {
      CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);
      fail ();
    }
    catch (CmdlineEx e) {}
  }

  public void testCallbacks ()
  {
    args100[argc++] = new String ("-c0");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals ( 1, Options.getTest_g ());
    assertEquals (48, Options.getTest_c ());
    assertEquals ( 0, Options.getTest_s ());
    assertEquals ( 1, Options.getTest_c_g ());
  }

  public void testMultipleShortOptions1 ()
  {
    args100[argc++] = new String ("-R");
    args100[argc++] = new String ("myfilename");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (true, Options.r ());
  }

  public void testMultipleShortOptions2 ()
  {
    args100[argc++] = new String ("-r");
    args100[argc++] = new String ("-R");
    args100[argc++] = new String ("myfilename");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (true, Options.r ());
  }

  public void testMultipleShortOptions3 ()
  {
    args100[argc++] = new String ("-R");
    args100[argc++] = new String ("-r");
    args100[argc++] = new String ("myfilename");
    args = genArgs (argc, args100);

    CmdlineWithCallbacks Options = new CmdlineWithCallbacks (args);

    assertEquals (true, Options.r ());
  }
};
