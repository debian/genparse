/** @file eh_system.h
\brief Class definition system call error handler */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EH_SYSTEM_H
#define EH_SYSTEM_H

#include <string>
#include "eh.h"

//---------------------------------------------------------------------------
//
// class EH_system
//
// Derived from EH 
//
//---------------------------------------------------------------------------

class EH_system: public EH
{
public:
  // Constructor, destructor
  EH_system (string);
  EH_system (string, string, int);
  ~EH_system (){}
};

#endif


