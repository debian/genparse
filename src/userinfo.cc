/** @file userinfo.cc
\brief Methods for the user information class */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <algorithm>
#include <unistd.h>
#include <sys/types.h>
#include <sys/param.h>
#include <pwd.h>
#include "userinfo.h"

#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 256
#endif

//---------------------------------------------------------------------------
//
// Userinfo::Userinfo ()
//
// Constructor method.  Does most of the work
//
//---------------------------------------------------------------------------

Userinfo::Userinfo () throw (EH_system)
{
  struct passwd *pw;
  char host[MAXHOSTNAMELEN];
  char domain[MAXHOSTNAMELEN];
  string::size_type annoying_comma;

  // Get userid and name
  pw = getpwuid (getuid ());
  if (pw == NULL)
    {
      EH_system e ("cannot get user info");
      throw e;
    }

  // Store locally
  _userid = pw->pw_name;
  _username = pw->pw_gecos;
  
  // Get rid of annoying ,,, if its in the name
  annoying_comma = _username.find (',');
  if (annoying_comma != string::npos)
    _username.erase (annoying_comma);

  // Get host name
  if (gethostname (host, MAXHOSTNAMELEN) < 0)
    {
      EH_system e ("cannot get host name");
      throw e;
    }
  _hostname = host;

  // Get the domain name
  _domainname = "";

  /*
   * The getdomainame () call is not supported on many systems, so we make
   * sure to check for it.  In particular, since autoconf is broken under
   * cygwin and getdomainname () is not present, we specifically check for 
   * __CYGWIN__
   */

#if defined (HAVE_GETDOMAINNAME) && !defined (__CYGWIN__)
  if (getdomainname (domain, MAXHOSTNAMELEN) == -1)
    {
      EH_system e ("cannot get domain name");
      throw e;
    }
  _domainname = domain;
  if (_domainname == "(none)")
    _domainname = "";
  else
    _domainname = "." + _domainname;
#endif
}
