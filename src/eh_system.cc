/** @file eh_system.cc
\brief Methods for the sytem call error handler class */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstring>
#include <errno.h>
#include "eh_system.h"

//---------------------------------------------------------------------------
//
// EH_system::EH_system ()
//
// Constructor method.  Get system call error message.
//
//---------------------------------------------------------------------------

EH_system::EH_system (string s)
{
  char *syserr;

  // First, get the error string from the system
  syserr = strerror (errno);

  // add it to the user string
  s += ": ";
  while (*syserr != '\0')
    {
      s += *syserr;
      syserr++;
    }

  action (s, "", 0);
}

//---------------------------------------------------------------------------
//
// EH_system::EH_system (string, string, int)
//
// Constructor method.  Get system call error message, dump file and line #
//
//---------------------------------------------------------------------------

EH_system::EH_system (string s, string f, int l)
{
  char *syserr;

  // First, get the error string from the system
  syserr = strerror (errno);

  // add it to the user string
  s += ": ";
  while (*syserr != '\0')
    {
      s +=*syserr;
      syserr++;
    }

  action (s, f, l);
}
