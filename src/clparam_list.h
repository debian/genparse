/** @file clparam_list.h
\brief Definition of the class which generates the command line parser */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLPARAM_LIST_H
#define CLPARAM_LIST_H

#include <list>
#include <string>
#include <cstdlib>
#include "clparam.h"
#include "eh.h"

/** \brief Gererates the source files which form the command line parser. */
class Clparam_list
{
private:
  list<Clparam> _params;
  string        _global_callback;
  list<string>  _include_list;
  list<string>  _mandatory_list;
  string        _parsefunc;
  string        _directory;
  string        _cppext;
  string        _exit_value;
  bool          _quiet;
  bool          _longmembers;
  bool          _internationalization;
  bool          _static_headers;
  bool          _export_long_options;
  bool          _use_gnulib;
  bool          _many_prints;
  bool          _no_struct;
  int           _break_lines;
  list<string>  _usage_list;

  bool parameters_have_callbacks (void) const;
  bool long_only (void) const;

  string       get_options_string (void) const;
  list<string> get_glossary (void) const;
  list<string> get_glossary_gnu (unsigned int indentation = 24) const;
  string       get_mandatories_string (void) const;

  /** Check if the combination of parameters fit together */
  virtual void check_parameters (void) const {};

  // Language specific methods which must be defined in sub classes in order to let output () work
  virtual void output_interface (     const string filename_without_extension) const throw (EH) {}
  virtual void output_implementation (const string filename_without_extension) const throw (EH) {}
  virtual void output_callbacks (     const string filename_without_extension) const throw (EH) {}

protected:
  bool         have_callbacks (void)         const;
  const string type2str (const param_type t) const;
  bool         contains (const Clparam &p)   const;

  const list<Clparam> &get_params (void) const;

  /** get name of global callback function */
  const string &get_global_callback (void) const { return _global_callback; }

  string const get_parameter_name (const Clparam &param) const;

  virtual const string string_type_str (void)  const { return "char *"; }
  virtual const string boolean_type_str (void) const { return "bool"; }

  string::size_type parse_macro (string::size_type &arg_start_pos, string::size_type &arg_end_pos, 
    string::size_type &closing_bracket_pos, const string &macro, const string &line) const;
  list<string> get_usage (void) const;

  // These are for any language
  void output_comments_header (ofstream& o, const string &filename, const string &purpose) const;
  void output_function_comment (ofstream& o, const string &function_name, 
    const string &comment) const;
  void output_includes (ofstream& o) const;

public:
  // Constructor, destructor and copy constructor
  Clparam_list ();
  virtual ~Clparam_list () {}
  Clparam_list& operator= (const Clparam_list &);

  void add (const Clparam &p) throw (EH);

  /** Add an include file */
  void add_include (const string &s) { _include_list.push_back (s); }

  /** Add a mandatory */
  void add_mandatory (const string &s) { _mandatory_list.push_back (s); }

  /** Set name of global callback function */
  void set_global_callback (const string &s) { _global_callback = s; }

  /** set the parsing function / class name */
  void set_parsefunc (const string s) { _parsefunc = s; }

  /** set the directory for output files */
  void set_directory (const string s) { _directory = s; }

  /** get the parsing function / class name */
  const string &get_parsefunc (void) const { return _parsefunc; }

  /** get the directory for output files */
  const string &get_directory (void) const { return _directory; }

  /** set the c++ extension */
  void set_cppext (const string s) { _cppext = s; }

  /** set the exit value for the case of a failure exit */
  void set_exit_value (const string s) { _exit_value = s; }

  /** get the c++ extension */
  const string &get_cppext (void) const { return _cppext; }

  /** get the exit value for the case of a failure exit */
  const string &get_exit_value (void) const { return _exit_value; }

  /** set longmembers flag (use of long member names in parser class) */
  void set_longmembers (bool b) { _longmembers = b; }

  /** get longmembers flag (use of long member names in parser class) */
  bool get_longmembers (void) const { return _longmembers; }

  /** set internationalization flag (put internationalization macro _() arount help text) */
  void set_internationalization (bool b) { _internationalization = b; }

  /** get internationalization flag (put internationalization macro _() arount help text) */
  bool get_internationalization (void) const { return _internationalization; }

  /** set --static-headers flag (don't add creation date, user name, kernel version etc.) */
  void set_static_headers (bool b) { _static_headers = b; }

  /** get --static-headers flag (don't add creation date, user name, kernel version etc.) */
  bool get_static_headers (void) const { return _static_headers; }

  /** Set width of help scren. Longer lines will be broken. -1 means: don't break lines 
      automatically. */
  void set_break_lines (const char *width) { _break_lines = atoi (width); }

  /** get width of help scren. */
  int get_break_lines (void) const { return _break_lines; }

  /** add a function get_long_options () which exports the long_options array used by getopt_long */
  void set_export_long_options (bool b) { _export_long_options = b; }

  /** get flag for adding a function get_long_options () which exports the long_options array 
      used by getopt_long */
  bool get_export_long_options (void) const { return _export_long_options; }

  /** set flag if Gnu Portability Library (Gnulib) shall be used */
  virtual void set_use_gnulib (bool b) { _use_gnulib = b; }

  /** get flag if Gnu Portability Library (Gnulib) shall be used */
  bool get_use_gnulib (void) const { return _use_gnulib; }

  /** set flag which specifies if every command line parameter shall be output in a separate print 
      command */
  virtual void set_many_prints (bool b) { _many_prints = b; }

  /** get flag which specifies if every command line parameter shall be output in a separate print 
      command */
  bool get_many_prints (void) const { return _many_prints; }

  /** get flag which specifies if no struct / class for parsing results shall be generated */
  bool get_no_struct (void) const { return _no_struct; }

  /** set flag which specifies if no struct / class for parsing results shall be generated */
  void set_no_struct (bool b) { _no_struct = b; }

  /** set quiet mode */
  void set_quiet (void) { _quiet = true; }

  /** get quiet mode */
  bool get_quiet (void) const { return _quiet; }

  /** Appends a line to the explanation of the usage of the generated program */
  void append_usage_line (string s) { _usage_list.push_back (s); }

  /** Return true is a usage text is defined */
  bool has_usage (void) const { return !_usage_list.empty (); }

  bool contains (const char   c) const;
  bool contains (const string s) const;

  void logdump (Logfile&) const;

  void post_parser_adjust (void);

  void output (const string filename_without_extension) const;
};

#endif  // CLPARAM_LIST_H
