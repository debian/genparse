/** @file cldesc.cc
\brief Methods for class Cldesc */
/*
 * Copyright (C) 2007 - 2016
 * Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "cldesc.h"

/** Constructor method. Initialize some of the variables.
*/
Cldesc::Cldesc ()
{
  _start_1st_col = false;
}

/** Constructor method. */
Cldesc::Cldesc (const string &description, bool start_1st_col)
{
  _description   = description;
  _start_1st_col = start_1st_col;
}

/** Copy constructor */
Cldesc& Cldesc::operator=(const Cldesc &rhs)
{
  // guard against self-assignment
  if (this != &rhs)
  {
    _description   = rhs._description;
    _start_1st_col = rhs._start_1st_col;
  }
  
  return *this;
}
