/** @file java_clparam_list.h
\brief C++ implementation of the command line parser */
/*
 * Copyright (C) 2006 - 2016
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JAVA_CLPARAM_LIST_H
#define JAVA_CLPARAM_LIST_H

#include <string>
#include "clparam_list.h"

/**
\brief Java implementation of class Clparam_list

Generates Java output files for parsing a command line.

3 Files can be generated: 
  - a Java source file with the class definition
  - a Java source file containing the parser class implementation
  - a Java source file containing callback functions
*/
class Java_Clparam_list: public Clparam_list
{
private:
  void output_print_command (ofstream& o, const string &text) const;

  void check_parameters (void) const;

  void output_comments_object (ofstream& o, string classname, string comment, 
    string objectname) const;

  const string string_type_str (void)  const { return "String"; }
  const string boolean_type_str (void) const { return "boolean"; }

  /** get the class name of the interface for the parser class */
  const string get_interface_name (void) const { return get_parsefunc () + "Interface"; }

  /** get the class name of the exception class thrown by the parser class */
  const string get_exception_class_name (void) const { return get_parsefunc () + "Ex"; }

  // output Java code
  void output_exception_class (                            void) const throw (EH);
  void output_interface (     string filename_without_extension) const throw (EH);
  void output_implementation (string filename_without_extension) const throw (EH);
  void output_callbacks (     string filename_without_extension) const throw (EH);

public:
  Java_Clparam_list () { set_exit_value ("-1"); }
  ~Java_Clparam_list () {}

  void set_use_gnulib (bool b) throw (EH);
};

#endif  // JAVA_CLPARAM_LIST_H
