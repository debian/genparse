/******************************************************************************
**
** parse_cl.cc
**
** Sun Nov 16 13:14:13 2008
** Linux 2.6.23 (#9 Sun Jan 27 22:29:05 CET 2008) i686
** linux@mgpc (Michael Geng)
**
** Definition of command line parser class
**
** Automatically created by genparse v0.8.1
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#include <getopt.h>
#include <stdlib.h>
#include "parse_cl.h"

/*----------------------------------------------------------------------------
**
** Cmdline::Cmdline ()
**
** Constructor method.
**
**--------------------------------------------------------------------------*/

Cmdline::Cmdline (int argc, char *argv[]) throw (std::string )
{
  extern char *optarg;
  extern int optind;
  int c;

  static struct option long_options[] =
  {
    {"cppext", required_argument, NULL, 'c'},
    {"directory", required_argument, NULL, 'D'},
    {"logfile", required_argument, NULL, 'f'},
    {"gnulib", no_argument, NULL, 'g'},
    {"internationalize", no_argument, NULL, 'i'},
    {"language", required_argument, NULL, 'l'},
    {"longmembers", no_argument, NULL, 'm'},
    {"outfile", required_argument, NULL, 'o'},
    {"parsefunc", required_argument, NULL, 'p'},
    {"manyprints", no_argument, NULL, 'P'},
    {"quiet", no_argument, NULL, 'q'},
    {"static-headers", no_argument, NULL, 's'},
    {"help", no_argument, NULL, 'h'},
    {"version", no_argument, NULL, 'v'},
    {NULL, 0, NULL, 0}
  };

  _program_name += argv[0];

  /* default values */
  _c = "cc";
  _d = false;
  _f = "genparse.log";
  _g = false;
  _i = false;
  _l = "c";
  _m = false;
  _o = "parse_cl";
  _p = "Cmdline";
  _P = false;
  _q = false;
  _s = false;
  _h = false;
  _v = false;

  optind = 0;
  while ((c = getopt_long (argc, argv, "c:dD:f:gil:mo:p:Pqshv", long_options, &optind)) != - 1)
    {
      switch (c)
        {
        case 'c': 
          _c = optarg;
          break;

        case 'd': 
          _d = true;
          break;

        case 'D': 
          _D = optarg;
          break;

        case 'f': 
          _f = optarg;
          break;

        case 'g': 
          _g = true;
          break;

        case 'i': 
          _i = true;
          break;

        case 'l': 
          _l = optarg;
          break;

        case 'm': 
          _m = true;
          break;

        case 'o': 
          _o = optarg;
          break;

        case 'p': 
          _p = optarg;
          break;

        case 'P': 
          _P = true;
          break;

        case 'q': 
          _q = true;
          break;

        case 's': 
          _s = true;
          break;

        case 'h': 
          _h = true;
          this->usage (EXIT_SUCCESS);
          break;

        case 'v': 
          _v = true;
          break;

        default:
          this->usage (EXIT_FAILURE);

        }
    } /* while */

  _optind = optind;
}

/*----------------------------------------------------------------------------
**
** Cmdline::usage ()
**
** Print out usage information, then exit.
**
**--------------------------------------------------------------------------*/

void Cmdline::usage (int status)
{
  if (status != EXIT_SUCCESS)
    std::cerr << "Try `" << _program_name << " --help' for more information.\n";
  else
    {
      std::cout << "\
usage: " << _program_name << " [ -cdDfgilmopPqshv ] file\n\
   [ -c ] [ --cppext ] (type=STRING, default=cc)\n\
          C++ file extension.\n\
   [ -d ] (type=FLAG)\n\
          Turns on logging.\n\
   [ -D ] [ --directory ] (type=STRING)\n\
          Directory for storing results.\n\
   [ -f ] [ --logfile ] (type=STRING, default=genparse.log)\n\
          Log file name.\n\
   [ -g ] [ --gnulib ] (type=FLAG)\n\
          Use GNU Portability Library (Gnulib).\n\
   [ -i ] [ --internationalize ] (type=FLAG)\n\
          Put internationalization macro _() around text output.\n\
   [ -l ] [ --language ] (type=STRING, default=c)\n\
          Output language.\n\
   [ -m ] [ --longmembers ] (type=FLAG)\n\
          Use long names for class members.\n\
   [ -o ] [ --outfile ] (type=STRING, default=parse_cl)\n\
          Output file name.\n\
   [ -p ] [ --parsefunc ] (type=STRING, default=Cmdline)\n\
          Name of parsing function / class.\n\
   [ -P ] [ --manyprints ] (type=FLAG)\n\
          Output help text for every command line parameter in a\n\
          separate print command.\n\
   [ -q ] [ --quiet ] (type=FLAG)\n\
          Turns on quiet mode.\n\
   [ -s ] [ --static-headers ] (type=FLAG)\n\
          Keep headers of generated files static. Don't add\n\
          creation date, username, kernel version etc.\n\
   [ -h ] [ --help ] (type=FLAG)\n\
          Display this help and exit.\n\
   [ -v ] [ --version ] (type=FLAG)\n\
          Output version information and exit.\n";
    }
  exit (status);
}
