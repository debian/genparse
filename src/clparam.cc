/** @file clparam.cc
\brief Methods for the command line parameter class */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include "clparam.h"

/** Constructor method. Initialize some of the variables. Strings should 
initialize to empty. 
*/
Clparam::Clparam ()
{
  _opt_name_in_square_braces = false;
  _type                      = null_type;
  _longopt_value             = 0;
  _long_only                 = false;
  _store_longindex           = false;
  _do_not_document           = false;
  _add_flag                  = false;
  _short_has_arg             = argument_undefined;
  _long_has_arg              = argument_undefined;
}

/** Copy constructor */
Clparam& Clparam::operator=(const Clparam &rhs)
{
  // guard against self-assignment
  if (this != &rhs)
    {
      _short_reps                = rhs._short_reps;
      _long_rep                  = rhs._long_rep;
      _opt_name_in_square_braces = rhs._opt_name_in_square_braces;
      _type                      = rhs._type;
      _default_value             = rhs._default_value;
      _low_range                 = rhs._low_range;
      _high_range                = rhs._high_range;
      _callback                  = rhs._callback;
      _err_msg                   = rhs._err_msg;
      _err_msg_conv              = rhs._err_msg_conv;
      _comment                   = rhs._comment;
      _descriptions              = rhs._descriptions;
      _code_lines                = rhs._code_lines;
      _longopt_value             = rhs._longopt_value;
      _long_only                 = rhs._long_only;
      _store_longindex           = rhs._store_longindex;
      _do_not_document           = rhs._do_not_document;
      _add_flag                  = rhs._add_flag;
      _short_has_arg             = rhs._short_has_arg;
      _long_has_arg              = rhs._long_has_arg;
    }
  
  return *this;
}

void Clparam::add_description (const string &description, bool start_1st_col)
{
  _descriptions.push_back (Cldesc(description, start_1st_col));
}

/** Return true if this parameter must be implemented with 2 arguments:
one for the type declared
another one for the information if it has been specified.

This is required mostly for optional arguments. */
bool Clparam::requires_additional_flag_arg (void) const
{
  if (get_add_flag ())
    return (true);

  return ((get_type () != flag_type) &&
          ((get_short_has_arg () == optional_argument) ||
           (get_long_has_arg () == optional_argument) ||
           (get_long_has_arg () != get_short_has_arg ())));
}

/** Return short representation number i. */
char Clparam::get_short_rep (unsigned int i) const
{
  if (_short_reps.length () > i)
    {
      return (_short_reps.at (i));
    }
    else
    {
      return (0);
    }
}

/** Return true if parameter contains c as one of the short representations */
bool Clparam::contains (char c) const
{
  unsigned int i;

  for (i = 0; i < _short_reps.length (); i++)
    if (_short_reps.at (i) == c)
      return true;

  return false;
}

/** Returns the descriptve text for this command line parameter. Every element in the 
result array is 1 line. 
\param indentation Desired indentation width. This value is required only if 
break_lines is >= 0 for calculating how to break lines. The result will not be
indented however.
\param additional_indentation_following_lines Additional indentation beginning with the 
2nd output line.
\param break_lines If < 0 then an unchanged copy of the descriptive text is returned. 
Otherwise the lines are broken such that the resulting text has a width of 
break_lines - indentation characters.
*/
list<Cldesc> Clparam::get_description (unsigned int indentation, 
  unsigned int additional_indentation_following_lines, int break_lines) const
{
  list<Cldesc> lines;
  string word;
  string line;
  unsigned int length;
  bool first_word;

  length = indentation;
  first_word = true;

  list<Cldesc>::const_iterator iter;
  if (break_lines < 0)
    return _descriptions;
  else
    {
      for (iter = _descriptions.begin (); iter != _descriptions.end (); iter++)
        {
          /* Read word by word */
          istringstream line_stream (iter->get_description ());
          while (line_stream >> word)
            {
              if (!first_word)
                {
                  if (length + word.length () + 1 > (unsigned int)break_lines)
                    {
                      lines.push_back (line);
                      length = indentation + additional_indentation_following_lines;
                      first_word = true;
                      line = string (additional_indentation_following_lines, ' ');
                    }
                  else
                    {
                      line += ' ';
                      length += 1;
                    }
                }

              line += word;
              length += word.length ();
              first_word = false;
            }
        }

      lines.push_back (line);
      return lines;
    }
}

/** Dump contents to a Logfile */
void Clparam::logdump (Logfile& l) const
{
  l.write ("  dumping parameter...");
  l.write (LOG_OPTION_NO_CR, "    short representation: ");
  l.write (LOG_OPTION_NO_TS, _short_reps);
  l.write ("    long representation: " + _long_rep);
  l.write (LOG_OPTION_NO_CR, "    parameter type: ");
  switch (_type)
    {
    case int_type:
      l.write (LOG_OPTION_NO_TS, "int");
      break;
    case long_type:
      l.write (LOG_OPTION_NO_TS, "long int");
      break;
    case ulong_type:
      l.write (LOG_OPTION_NO_TS, "unsigned long int");
      break;
    case intmax_type:
      l.write (LOG_OPTION_NO_TS, "intmax_t");
      break;
    case uintmax_type:
      l.write (LOG_OPTION_NO_TS, "uintmax_t");
      break;
    case float_type:
      l.write (LOG_OPTION_NO_TS, "float");
      break;
    case double_type:
      l.write (LOG_OPTION_NO_TS, "double");
      break;
    case string_type:
      l.write (LOG_OPTION_NO_TS, "string");
      break;
    case char_type:
      l.write (LOG_OPTION_NO_TS, "char");
      break;
    case flag_type:
      l.write (LOG_OPTION_NO_TS, "flag");
      break;
    default:
      break;
    }
  l.write ("    default_value: " + _default_value);
  l.write ("    low range: " + _low_range);
  l.write ("    high range: " + _high_range);
  l.write ("    callback: " + _callback);
  l.write ("    descriptions:");
  
  list<Cldesc>::const_iterator iter;
  if (!_descriptions.empty ())
    for (iter = _descriptions.begin (); iter != _descriptions.end (); iter++)
      l.write ((string) "      " + iter->get_description ());

  ostringstream oss;
  oss << _longopt_value << ends;

  l.write ((string) "    longopt value: " + oss.str ());
  l.write ("  finished dumping parameter");
}

/** Left aligns all code lines. */
void Clparam::normalize_code_lines_indentation (void)
{
  list<string>::iterator iter;
  bool all_start_with_space, all_start_with_tab;

  do
    {
      all_start_with_space = true;
      all_start_with_tab   = true;

      for (iter = _code_lines.begin (); iter != _code_lines.end (); iter++)
        {
          if ((*iter)[0] != ' ')
            all_start_with_space = false;
          if ((*iter)[0] != '\t')
            all_start_with_tab = false;
        }

      if (all_start_with_space || all_start_with_tab)
        for (iter = _code_lines.begin (); iter != _code_lines.end (); iter++)
          iter->erase(0, 1);
    }
  while ((all_start_with_space || all_start_with_tab) && _code_lines.size () > 0);
}
