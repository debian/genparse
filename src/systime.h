/** @file systime.h
\brief Class definition for system time (wall clock time) */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYSTIME_H
#define SYSTIME_H

#include <string>
#include <time.h>
#include <sys/time.h>
#include "eh_system.h"

//---------------------------------------------------------------------------
//
// class Systime
//
//---------------------------------------------------------------------------

class Systime
{
private:
  struct timeval _tv;   // Store absolute time here
  struct tm *    _tp;   // seconds since unix epoch, broken down (relative)
   
public:
  // Constructor and destructor
  Systime ();
  ~Systime (){}

  // Operator overrides
  Systime& operator= (const Systime&);
  Systime operator- (const Systime);
  
  // Read the clock again
  void refresh () throw (EH_system);

  // functions to display the time
  int microseconds () { return _tv.tv_usec; }
  int seconds ()      { return _tv.tv_sec; }
  int sec ()          { return _tp->tm_sec; }
  int minute ()       { return _tp->tm_min; }
  int hour ()         { return _tp->tm_hour; }
  int year ()         { return _tp->tm_year; }
  string hms ();
  string month ();
  string day ();
  string unix_ctime ();
};

#endif
