/** @file genparse_util.h
\brief Utilities used within the genparse project */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENPARSE_UTIL_H
#define GENPARSE_UTIL_H

#include <string>

namespace genparse_util
{
  using namespace std;

  string str2upper (string);
  string C_comment (const string s);
  string full_path (const string directory, const string filename);
  char minus_to_underscore (const char c);
  string str2C (string s);
}

#endif  // GENPARSE_UTIL_H
