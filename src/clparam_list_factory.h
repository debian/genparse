/** @file clparam_list_factory.h
\brief Definition of the class which constructs the command line parser class Clparam_list */
/*
 * Copyright (C) 2006 - 2016
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLPARAM_LIST_FACTORY_H
#define CLPARAM_LIST_FACTORY_H

#include "clparam_list.h"
#include "eh.h"

/** \brief Constructs a language specific command line parser class. */
class Clparam_list_factory
{
public:
  Clparam_list_factory () {}
  ~Clparam_list_factory () {}

  Clparam_list *Create_Clparam_list (const string &language) const throw (EH);
};

#endif  // CLPARAM_LIST_FACTORY_H
