/** @file logfile.cc
\brief Methods for the logfile class */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "sysinfo.h"
#include "userinfo.h"
#include "logfile.h"

const string separator ("--------------------------------------------------------------");

//---------------------------------------------------------------------------
//
// Logfile::Logfile ()
//
// Constructor method.
//
//---------------------------------------------------------------------------

Logfile::Logfile ()
{
  _is_open = false;
  _active = false;
}

//---------------------------------------------------------------------------
//
// Logfile::open ()
//
// Open the file and print the begin banner
//
//---------------------------------------------------------------------------

void Logfile::open (string filename) throw (EH)
{
  // Open the file for writing.  Eventually we'll change this to avoid
  // collisions
  _log.open (filename.c_str ());
  if (!_log)
    throw EH ("can't open file " + filename);

  // turn on the logging ability
  _is_open = true;
  _active = true;
  
  // Assign a local copy of the file name - who knows, we may need it
  _filename = filename;

  // dump the introductory banner
  this->write (separator);
  this->write ("BEGINNING OF LOG");

  // dump the machine info
  Sysinfo si;
  string si_str;
  si_str += "System: " + si.format ();
  this->write (si_str);

  // dump the user info
  Userinfo ui;
  string ui_str;
  ui_str += "User:   " + ui.format ();
  this->write (ui_str);

  // one more line
  this->write (separator);
}

//---------------------------------------------------------------------------
//
// Logfile::close ()
//
// Close the file and print the end banner
//
//---------------------------------------------------------------------------

void Logfile::close ()
{
  if (_is_open && _active)
    {
      this->write (separator);
      this->write ("END OF LOG");
      this->write (separator);
      if (!_log)
        throw EH ("can't write to file " + _filename);
      _log.close ();
      if (!_log)
        throw EH ("can't close file " + _filename);
      _is_open = false;
      _active = false;
    }
}

//---------------------------------------------------------------------------
//
// Logfile::write_separator ()
//
// Write a separator
//
//---------------------------------------------------------------------------

void Logfile::write_separator ()
{
  if (_is_open && _active)
    {
      write_ts ();
      _log << separator << endl;    
      if (!_log)
        throw EH ("can't write to file " + _filename);
    }
}

//---------------------------------------------------------------------------
//
// Logfile::write (string)
//
// Writes a string, prefaced by a timestamp.  First check if we're active 
// and open
//
//---------------------------------------------------------------------------

void Logfile::write (string s)
{
  if (_is_open && _active)
    {
      write_ts ();
      _log << s << endl;    
      if (!_log)
        throw EH ("can't write to file " + _filename);
    }
}

//---------------------------------------------------------------------------
//
// Logfile::write (log_option_t, string)
//
// Writes a string, wrt the option passed
//
//---------------------------------------------------------------------------

void Logfile::write (log_option_t o, string s)
{
  if (_is_open && _active)
    {
      // check for timestamp
      if (o != LOG_OPTION_NO_TS)
	write_ts ();
      
      // write the string
      _log << s;

      if (o != LOG_OPTION_NO_CR)
	_log << endl;
	
      if (!_log)
        throw EH ("can't write to file " + _filename);
    }
}

//---------------------------------------------------------------------------
//
// Logfile::write (char)
//
// Writes a char, prefaced by a timestamp.  First check if we're active 
// and open
//
//---------------------------------------------------------------------------

void Logfile::write (char c)
{
  if (_is_open && _active)
    {
      write_ts ();
      _log << c << endl;    

      if (!_log)
        throw EH ("can't write to file " + _filename);
    }
}

//---------------------------------------------------------------------------
//
// Logfile::write (log_option_t, char)
//
// Writes a char, wrt the option passed
//
//---------------------------------------------------------------------------

void Logfile::write (log_option_t o, char c)
{
  if (_is_open && _active)
    {
      // check for timestamp
      if (o != LOG_OPTION_NO_TS)
	{
	  write_ts ();
	}
      
      // write the string
      _log << c;

      if (o != LOG_OPTION_NO_CR)
	_log << endl;
	
      if (!_log)
        throw EH ("can't write to file " + _filename);
    }
}
