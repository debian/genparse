/** @file userinfo.h
\brief Class definition for UNIX information on the user (caller) */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USERINFO_H
#define USERINFO_H

#include <string>
#include "eh_system.h"

//---------------------------------------------------------------------------
//
// class Userinfo
//
//---------------------------------------------------------------------------

class Userinfo
{
private:
  string _userid;
  string _username;
  string _hostname;
  string _domainname;

public:
  // Constructor and destructor
  Userinfo () throw (EH_system);
  ~Userinfo (){}

  // functions to retrieve the values
  string userid ()        { return _userid; }
  string username ()      { return _username; }
  string hostname ()      { return _hostname; }
  string domainname ()    { return _domainname; }

  // format the output into "userid@host (name)"
  string format () { return _userid + "@" + _hostname + _domainname + 
		      " (" + _username + ")"; }
};

#endif



