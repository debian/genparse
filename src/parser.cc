/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 22 "parser.yy" /* yacc.c:339  */


#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include "genparse.h"
#include "clparam_list.h"
#include "logfile.h"
#include "unique_int.h"

#define YYDEBUG 1

// These  prototypes are necessary for C++ compilation
int yyerror (const string);
int yylex ();
void dump_short_reps (const string);
void add_param_to_list ();
void dump_param_type (param_type);

// These are all defined in genparse.cc
extern Clparam_list *my_params;
extern Unique_Int longopt_value;
extern Logfile mylog;
extern int yylineno;
extern char *yyfilename;

// This is an object we use over and over to read in parameters
Clparam *param;


#line 98 "parser.cc" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    SLASH = 258,
    RANGE_SPEC1 = 259,
    RANGE_SPEC2 = 260,
    FUNC_SPEC = 261,
    NONE = 262,
    OPEN_BRACE = 263,
    CLOSE_BRACE = 264,
    OPEN_SQUARE_BRACE = 265,
    CLOSE_SQUARE_BRACE = 266,
    EQUAL = 267,
    INCLUDE = 268,
    LT = 269,
    GT = 270,
    MANDATORY = 271,
    EXIT_VALUE = 272,
    BREAK_LINES = 273,
    DOUBLEQUOTE = 274,
    ASTERISK = 275,
    EXCLAMATIONMARK = 276,
    STORE_LONGINDEX = 277,
    EXPORT_LONG_OPTIONS = 278,
    GP_INCLUDE = 279,
    ERR_MSG = 280,
    COMMENT = 281,
    DO_NOT_DOCUMENT = 282,
    OPEN_ROUND_BRACE = 283,
    CLOSE_ROUND_BRACE = 284,
    COLON = 285,
    ADD_FLAG = 286,
    NO_STRUCT = 287,
    CODE_END = 288,
    QUOTED = 289,
    FIRST_QUOTED = 290,
    C_VAR = 291,
    ALNUM = 292,
    FILENAME = 293,
    CHAR_VAL = 294,
    FIRST_STR = 295,
    USAGE_LINE = 296,
    CODE_LINE = 297,
    OPT_NAME = 298,
    COMMENT_STR = 299,
    FUNCTION = 300,
    FLAG = 301,
    INT = 302,
    LONG = 303,
    ULONG = 304,
    INTMAX = 305,
    UINTMAX = 306,
    FLOAT = 307,
    DOUBLE = 308,
    CHAR = 309,
    STRING = 310
  };
#endif
/* Tokens.  */
#define SLASH 258
#define RANGE_SPEC1 259
#define RANGE_SPEC2 260
#define FUNC_SPEC 261
#define NONE 262
#define OPEN_BRACE 263
#define CLOSE_BRACE 264
#define OPEN_SQUARE_BRACE 265
#define CLOSE_SQUARE_BRACE 266
#define EQUAL 267
#define INCLUDE 268
#define LT 269
#define GT 270
#define MANDATORY 271
#define EXIT_VALUE 272
#define BREAK_LINES 273
#define DOUBLEQUOTE 274
#define ASTERISK 275
#define EXCLAMATIONMARK 276
#define STORE_LONGINDEX 277
#define EXPORT_LONG_OPTIONS 278
#define GP_INCLUDE 279
#define ERR_MSG 280
#define COMMENT 281
#define DO_NOT_DOCUMENT 282
#define OPEN_ROUND_BRACE 283
#define CLOSE_ROUND_BRACE 284
#define COLON 285
#define ADD_FLAG 286
#define NO_STRUCT 287
#define CODE_END 288
#define QUOTED 289
#define FIRST_QUOTED 290
#define C_VAR 291
#define ALNUM 292
#define FILENAME 293
#define CHAR_VAL 294
#define FIRST_STR 295
#define USAGE_LINE 296
#define CODE_LINE 297
#define OPT_NAME 298
#define COMMENT_STR 299
#define FUNCTION 300
#define FLAG 301
#define INT 302
#define LONG 303
#define ULONG 304
#define INTMAX 305
#define UINTMAX 306
#define FLOAT 307
#define DOUBLE 308
#define CHAR 309
#define STRING 310

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 57 "parser.yy" /* yacc.c:355  */

  char    *str;  /* string */
  int     i;
  param_type p;

#line 254 "parser.cc" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 269 "parser.cc" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  26
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   125

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  56
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  42
/* YYNRULES -- Number of rules.  */
#define YYNRULES  103
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  137

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   310

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    81,    81,    82,    85,    86,    87,    90,    91,    92,
      93,    94,    95,    96,    99,   105,   113,   121,   127,   135,
     143,   150,   157,   164,   165,   169,   169,   176,   179,   186,
     187,   188,   191,   197,   198,   203,   204,   209,   210,   215,
     220,   221,   224,   225,   228,   231,   238,   245,   255,   266,
     273,   280,   287,   292,   300,   301,   305,   311,   312,   313,
     316,   321,   328,   334,   335,   339,   345,   351,   359,   360,
     368,   376,   384,   392,   393,   401,   402,   403,   406,   407,
     408,   411,   414,   420,   424,   430,   436,   439,   440,   443,
     446,   452,   460,   461,   462,   465,   473,   474,   477,   483,
     490,   493,   497,   501
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "SLASH", "RANGE_SPEC1", "RANGE_SPEC2",
  "FUNC_SPEC", "NONE", "OPEN_BRACE", "CLOSE_BRACE", "OPEN_SQUARE_BRACE",
  "CLOSE_SQUARE_BRACE", "EQUAL", "INCLUDE", "LT", "GT", "MANDATORY",
  "EXIT_VALUE", "BREAK_LINES", "DOUBLEQUOTE", "ASTERISK",
  "EXCLAMATIONMARK", "STORE_LONGINDEX", "EXPORT_LONG_OPTIONS",
  "GP_INCLUDE", "ERR_MSG", "COMMENT", "DO_NOT_DOCUMENT",
  "OPEN_ROUND_BRACE", "CLOSE_ROUND_BRACE", "COLON", "ADD_FLAG",
  "NO_STRUCT", "CODE_END", "QUOTED", "FIRST_QUOTED", "C_VAR", "ALNUM",
  "FILENAME", "CHAR_VAL", "FIRST_STR", "USAGE_LINE", "CODE_LINE",
  "OPT_NAME", "COMMENT_STR", "FUNCTION", "FLAG", "INT", "LONG", "ULONG",
  "INTMAX", "UINTMAX", "FLOAT", "DOUBLE", "CHAR", "STRING", "$accept",
  "all", "globals", "global", "include", "mandatory", "exit_value",
  "break_lines", "export_long_options", "no_struct", "global_callback",
  "entries", "entry", "$@1", "new_entry", "options", "option", "descs",
  "err_msg", "err_msg_arg", "comment", "gp_include", "param",
  "short_params", "long_param", "mandatory_opt_name", "optional_opt_name",
  "multi_long_option", "long_option", "type", "default", "range",
  "contiguous_range", "more_range", "range_spec", "callback", "desc",
  "usages", "usage", "code_lines", "code_line", "single_code_line", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310
};
# endif

#define YYPACT_NINF -90

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-90)))

#define YYTABLE_NINF -93

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      51,    19,   -26,    34,   -17,   -90,   -90,    16,    61,     0,
     -90,   -90,   -90,   -90,   -90,   -90,   -90,   -90,   -90,    27,
      59,   -90,   -90,   -90,   -90,   -90,   -90,    60,   -90,   -90,
       1,   -90,    -3,   -90,    -7,   -90,    42,    80,   -90,   -90,
      -7,    97,    52,   -90,    38,    98,   -90,   -90,   -90,    39,
     -90,   -90,   -90,   -90,   -90,   -90,   -90,   -90,   -90,   -90,
     -90,   -90,    21,    39,   -90,   -90,   -90,    -1,    57,    68,
      -2,   -90,    75,    76,   -90,   -90,   -90,   -90,   -90,   -90,
     -90,   -90,   -90,   -90,    21,   -90,    45,   -90,   -90,   -90,
     -90,   -90,   -90,   -21,   -90,   -14,   -90,    93,     8,   -90,
     -90,   -90,   -90,    99,   -90,   -90,    77,    95,    58,    25,
      63,   -90,   -90,   -90,   -90,    11,   100,   -90,   -90,   -90,
      58,   -90,   -90,   -90,   -90,    79,    82,    81,    84,   -90,
     -90,    78,    83,   -90,   -90,   -90,   -90
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       4,     0,     0,     0,     0,    20,    21,     0,     0,    25,
       6,     7,     8,     9,    10,    11,    12,    13,    22,     0,
       0,    16,    17,    18,    19,    89,     1,     0,    95,     5,
      25,    24,     0,    27,     3,    94,     0,     0,    50,    23,
       2,     0,    54,    26,     0,    51,    93,    14,    15,     0,
      55,    56,    77,    68,    69,    70,    71,    72,    73,    74,
      76,    75,    29,     0,    67,    66,    52,    57,    63,     0,
       0,    36,     0,     0,    39,   101,   102,    38,    99,    90,
      91,    78,    79,   100,    28,    31,    35,    37,    40,    32,
      33,    34,    43,    41,    97,     0,    53,     0,     0,    58,
      59,    64,    65,     0,    87,    88,     0,     0,    86,     0,
       0,    30,    42,    96,    98,   103,     0,    60,    61,    80,
      86,    81,    85,    84,    83,    46,    45,     0,     0,    62,
      82,     0,     0,    44,    49,    48,    47
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -90,   -90,   -90,   106,   -90,   -90,   -90,   -90,   -90,   -90,
     -90,   -90,    86,   -90,   -90,   -90,    33,   -90,   -90,   -90,
     -90,   -90,   -90,   -90,    55,    23,   -90,   -90,   -90,   -90,
     -90,   -90,   -90,     2,    15,   -57,    37,    94,    -4,   -90,
      32,   -89
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    30,    31,    32,    43,    84,    85,    86,    87,   127,
      88,    33,    44,    45,    66,    99,   100,    67,    68,    62,
      89,    90,   107,   124,   108,    18,    92,    34,    35,    93,
      94,    95
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     -92,   -92,   104,   105,    41,    91,   115,    75,    76,    97,
      21,    98,    78,     1,    75,    76,     2,     3,     4,   114,
      24,    83,    25,     5,    27,    27,   115,    91,    83,    69,
      46,    70,     6,    19,    28,   106,    46,    42,    20,    75,
      76,    28,    28,    71,   117,     7,    72,    73,    74,    75,
      76,   118,    77,    83,    78,    79,    80,    47,    81,   125,
      82,    26,   126,    83,     1,    36,     7,     2,     3,     4,
      22,    23,    50,    51,     5,    64,    65,   101,   102,    79,
      80,   104,   105,     6,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,   122,   123,     7,    37,    38,    48,
      49,    63,   103,   109,   110,    98,   121,   128,   119,   131,
     133,   129,   132,   134,   135,    29,    39,   111,    96,   136,
     116,   120,   130,   112,    40,   113
};

static const yytype_uint8 yycheck[] =
{
       0,     0,     4,     5,     7,    62,    95,    28,    29,    10,
      36,    12,    33,    13,    28,    29,    16,    17,    18,    33,
      37,    42,     6,    23,    24,    24,   115,    84,    42,     8,
      34,    10,    32,    14,    41,    37,    40,    40,    19,    28,
      29,    41,    41,    22,    36,    45,    25,    26,    27,    28,
      29,    43,    31,    42,    33,    34,    35,    15,    37,    34,
      39,     0,    37,    42,    13,    38,    45,    16,    17,    18,
      36,    37,    20,    21,    23,    36,    37,    20,    21,    34,
      35,     4,     5,    32,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    36,    37,    45,    38,    38,    19,
       3,     3,    34,    28,    28,    12,    11,    44,     9,    30,
      29,    11,    30,    29,    36,     9,    30,    84,    63,    36,
      97,   106,   120,    86,    30,    93
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    13,    16,    17,    18,    23,    32,    45,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    91,    14,
      19,    36,    36,    37,    37,     6,     0,    24,    41,    59,
      67,    68,    69,    77,    93,    94,    38,    38,    38,    68,
      93,     7,    40,    70,    78,    79,    94,    15,    19,     3,
      20,    21,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    85,     3,    36,    37,    80,    83,    84,     8,
      10,    22,    25,    26,    27,    28,    29,    31,    33,    34,
      35,    37,    39,    42,    71,    72,    73,    74,    76,    86,
      87,    91,    92,    95,    96,    97,    80,    10,    12,    81,
      82,    20,    21,    34,     4,     5,    37,    88,    90,    28,
      28,    72,    92,    96,    33,    97,    81,    36,    43,     9,
      90,    11,    36,    37,    89,    34,    37,    75,    44,    11,
      89,    30,    30,    29,    29,    36,    36
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    56,    57,    57,    58,    58,    58,    59,    59,    59,
      59,    59,    59,    59,    60,    60,    61,    62,    62,    63,
      64,    65,    66,    67,    67,    69,    68,    68,    70,    71,
      71,    71,    72,    72,    72,    72,    72,    72,    72,    72,
      72,    72,    73,    73,    74,    75,    75,    75,    75,    76,
      77,    78,    78,    78,    79,    79,    79,    80,    80,    80,
      81,    81,    82,    83,    83,    83,    84,    84,    85,    85,
      85,    85,    85,    85,    85,    85,    85,    85,    86,    86,
      86,    87,    88,    88,    89,    89,    89,    90,    90,    91,
      92,    92,    93,    93,    93,    94,    95,    95,    96,    96,
      97,    97,    97,    97
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     3,     2,     0,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     4,     4,     2,     2,     2,     2,
       1,     1,     1,     2,     1,     0,     2,     1,     3,     0,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     2,     1,     4,     1,     1,     3,     3,     4,
       2,     1,     3,     3,     1,     2,     2,     1,     2,     2,
       2,     2,     3,     1,     2,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     3,     3,     2,     1,     1,     0,     1,     1,     2,
       1,     1,     0,     2,     1,     1,     2,     1,     2,     1,
       1,     1,     1,     2
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 14:
#line 100 "parser.yy" /* yacc.c:1646  */
    {
		my_params->add_include ((string)"<" + (yyvsp[-1].str) + ">");
		mylog.write ((string) "adding include file: <" + (yyvsp[-1].str) + ">");
		free ((yyvsp[-1].str));
	}
#line 1467 "parser.cc" /* yacc.c:1646  */
    break;

  case 15:
#line 106 "parser.yy" /* yacc.c:1646  */
    {
		my_params->add_include ((string)"\"" + (yyvsp[-1].str) + "\"");
		mylog.write ((string) "adding include file: \"" + (yyvsp[-1].str) + "\"");
		free ((yyvsp[-1].str));
	}
#line 1477 "parser.cc" /* yacc.c:1646  */
    break;

  case 16:
#line 114 "parser.yy" /* yacc.c:1646  */
    {
		my_params->add_mandatory ((yyvsp[0].str));
		mylog.write ((string) "adding mandatory: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1487 "parser.cc" /* yacc.c:1646  */
    break;

  case 17:
#line 122 "parser.yy" /* yacc.c:1646  */
    {
		my_params->set_exit_value ((yyvsp[0].str));
		mylog.write ((string) "adding exit value: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1497 "parser.cc" /* yacc.c:1646  */
    break;

  case 18:
#line 128 "parser.yy" /* yacc.c:1646  */
    {
		my_params->set_exit_value ((yyvsp[0].str));
		mylog.write ((string) "adding exit value: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1507 "parser.cc" /* yacc.c:1646  */
    break;

  case 19:
#line 136 "parser.yy" /* yacc.c:1646  */
    {
		my_params->set_break_lines ((yyvsp[0].str));
		mylog.write ((string) "breaking lines to a width of: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1517 "parser.cc" /* yacc.c:1646  */
    break;

  case 20:
#line 144 "parser.yy" /* yacc.c:1646  */
    {
		my_params->set_export_long_options (true);
		mylog.write ((string) "exporting long options struct");
	}
#line 1526 "parser.cc" /* yacc.c:1646  */
    break;

  case 21:
#line 151 "parser.yy" /* yacc.c:1646  */
    {
		my_params->set_no_struct (true);
		mylog.write ((string) "do not auto generate a struct / class for parsing results");
	}
#line 1535 "parser.cc" /* yacc.c:1646  */
    break;

  case 22:
#line 158 "parser.yy" /* yacc.c:1646  */
    {
		my_params->set_global_callback ((yyvsp[0].str));
		mylog.write ((string) "setting global callback: " + (yyvsp[0].str));
	}
#line 1544 "parser.cc" /* yacc.c:1646  */
    break;

  case 25:
#line 169 "parser.yy" /* yacc.c:1646  */
    {
		param = new Clparam; mylog.write ("reading parameter...");
	}
#line 1552 "parser.cc" /* yacc.c:1646  */
    break;

  case 26:
#line 173 "parser.yy" /* yacc.c:1646  */
    {
		add_param_to_list ();
	}
#line 1560 "parser.cc" /* yacc.c:1646  */
    break;

  case 28:
#line 180 "parser.yy" /* yacc.c:1646  */
    {
		param->set_type ((yyvsp[-1].p));
		dump_param_type ((yyvsp[-1].p));
	}
#line 1569 "parser.cc" /* yacc.c:1646  */
    break;

  case 32:
#line 192 "parser.yy" /* yacc.c:1646  */
    {
		param->set_default_value ((yyvsp[0].str));
		mylog.write ((string) "  default value: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1579 "parser.cc" /* yacc.c:1646  */
    break;

  case 34:
#line 199 "parser.yy" /* yacc.c:1646  */
    {
		param->set_callback ((yyvsp[0].str));
		mylog.write ((string) "  callback: " + (yyvsp[0].str));
	}
#line 1588 "parser.cc" /* yacc.c:1646  */
    break;

  case 36:
#line 205 "parser.yy" /* yacc.c:1646  */
    {
		param->set_store_longindex (true);
		mylog.write ((string) "  longindex for this parameter must be stored");
	}
#line 1597 "parser.cc" /* yacc.c:1646  */
    break;

  case 38:
#line 211 "parser.yy" /* yacc.c:1646  */
    {
		param->set_add_flag (true);
		mylog.write ((string) "  this parameter also gets a flag");
	}
#line 1606 "parser.cc" /* yacc.c:1646  */
    break;

  case 39:
#line 216 "parser.yy" /* yacc.c:1646  */
    {
		param->set_do_not_document (true);
		mylog.write ((string) "  do not document this parameter");
	}
#line 1615 "parser.cc" /* yacc.c:1646  */
    break;

  case 45:
#line 232 "parser.yy" /* yacc.c:1646  */
    {
		param->set_err_msg ((yyvsp[0].str));
		mylog.write ((string) "  conversion error message: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1625 "parser.cc" /* yacc.c:1646  */
    break;

  case 46:
#line 239 "parser.yy" /* yacc.c:1646  */
    {
		param->set_err_msg (string("\"") + (yyvsp[0].str) + '\"');
		mylog.write ((string) "  conversion error message: \"" + (yyvsp[0].str) + '\"');
		free ((yyvsp[0].str));
	}
#line 1635 "parser.cc" /* yacc.c:1646  */
    break;

  case 47:
#line 246 "parser.yy" /* yacc.c:1646  */
    {
		param->set_err_msg ((yyvsp[-2].str));
		mylog.write ((string) "  conversion error message: " + (yyvsp[-2].str));
		param->set_err_msg_conv ((yyvsp[0].str));
		mylog.write ((string) "  conversion function for erraneous argument: " + (yyvsp[0].str));
		free ((yyvsp[-2].str));
		free ((yyvsp[0].str));
	}
#line 1648 "parser.cc" /* yacc.c:1646  */
    break;

  case 48:
#line 256 "parser.yy" /* yacc.c:1646  */
    {
		param->set_err_msg (string("\"") + (yyvsp[-2].str) + '\"');
		mylog.write ((string) "  conversion error message: \"" + (yyvsp[-2].str) + '\"');
		param->set_err_msg_conv ((yyvsp[0].str));
		mylog.write ((string) "  conversion function for erraneous argument: " + (yyvsp[0].str));
		free ((yyvsp[-2].str));
		free ((yyvsp[0].str));
	}
#line 1661 "parser.cc" /* yacc.c:1646  */
    break;

  case 49:
#line 267 "parser.yy" /* yacc.c:1646  */
    {
		param->set_comment ((yyvsp[-1].str));
		mylog.write ((string) "  comment: " + (yyvsp[-1].str));
		free ((yyvsp[-1].str));
	}
#line 1671 "parser.cc" /* yacc.c:1646  */
    break;

  case 50:
#line 274 "parser.yy" /* yacc.c:1646  */
    {
		mylog.write ((string) "including genparse file" + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1680 "parser.cc" /* yacc.c:1646  */
    break;

  case 51:
#line 281 "parser.yy" /* yacc.c:1646  */
    {
		param->set_short_reps ((yyvsp[0].str));
		dump_short_reps ((yyvsp[0].str));
		free ((yyvsp[0].str));

	}
#line 1691 "parser.cc" /* yacc.c:1646  */
    break;

  case 52:
#line 288 "parser.yy" /* yacc.c:1646  */
    {
		param->set_longopt_value (longopt_value.get ());
		param->set_long_only (true);
	}
#line 1700 "parser.cc" /* yacc.c:1646  */
    break;

  case 53:
#line 293 "parser.yy" /* yacc.c:1646  */
    {
		param->set_short_reps ((yyvsp[-2].str));
		dump_short_reps ((yyvsp[-2].str));
		free ((yyvsp[-2].str));
	}
#line 1710 "parser.cc" /* yacc.c:1646  */
    break;

  case 55:
#line 302 "parser.yy" /* yacc.c:1646  */
    {
		param->set_short_has_arg (optional_argument);
	}
#line 1718 "parser.cc" /* yacc.c:1646  */
    break;

  case 56:
#line 306 "parser.yy" /* yacc.c:1646  */
    {
		param->set_short_has_arg (no_argument);
	}
#line 1726 "parser.cc" /* yacc.c:1646  */
    break;

  case 60:
#line 317 "parser.yy" /* yacc.c:1646  */
    {
		param->set_opt_name ((yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1735 "parser.cc" /* yacc.c:1646  */
    break;

  case 61:
#line 322 "parser.yy" /* yacc.c:1646  */
    {
		param->set_opt_name ((yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1744 "parser.cc" /* yacc.c:1646  */
    break;

  case 62:
#line 329 "parser.yy" /* yacc.c:1646  */
    {
		param->set_opt_name_in_square_braces (true);
	}
#line 1752 "parser.cc" /* yacc.c:1646  */
    break;

  case 63:
#line 334 "parser.yy" /* yacc.c:1646  */
    {}
#line 1758 "parser.cc" /* yacc.c:1646  */
    break;

  case 64:
#line 336 "parser.yy" /* yacc.c:1646  */
    {
		param->set_long_has_arg (optional_argument);
	}
#line 1766 "parser.cc" /* yacc.c:1646  */
    break;

  case 65:
#line 340 "parser.yy" /* yacc.c:1646  */
    {
		param->set_long_has_arg (no_argument);
	}
#line 1774 "parser.cc" /* yacc.c:1646  */
    break;

  case 66:
#line 346 "parser.yy" /* yacc.c:1646  */
    {
		param->set_long_rep ((yyvsp[0].str));
		mylog.write ((string) "  long representation: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1784 "parser.cc" /* yacc.c:1646  */
    break;

  case 67:
#line 352 "parser.yy" /* yacc.c:1646  */
    {
		param->set_long_rep ((yyvsp[0].str));
		mylog.write ((string) "  long representation: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1794 "parser.cc" /* yacc.c:1646  */
    break;

  case 68:
#line 359 "parser.yy" /* yacc.c:1646  */
    { (yyval.p) = int_type; }
#line 1800 "parser.cc" /* yacc.c:1646  */
    break;

  case 69:
#line 361 "parser.yy" /* yacc.c:1646  */
    {
		if (my_params->get_use_gnulib ())
			(yyval.p) = long_type;
		else
			yyerror ("type long only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
#line 1812 "parser.cc" /* yacc.c:1646  */
    break;

  case 70:
#line 369 "parser.yy" /* yacc.c:1646  */
    {
		if (my_params->get_use_gnulib ())
			(yyval.p) = ulong_type;
		else
			yyerror ("type ulong only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
#line 1824 "parser.cc" /* yacc.c:1646  */
    break;

  case 71:
#line 377 "parser.yy" /* yacc.c:1646  */
    {
		if (my_params->get_use_gnulib ())
			(yyval.p) = intmax_type;
		else
			yyerror ("type intmax_t only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
#line 1836 "parser.cc" /* yacc.c:1646  */
    break;

  case 72:
#line 385 "parser.yy" /* yacc.c:1646  */
    {
		if (my_params->get_use_gnulib ())
			(yyval.p) = uintmax_type;
		else
			yyerror ("type uintmax_t only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
#line 1848 "parser.cc" /* yacc.c:1646  */
    break;

  case 73:
#line 392 "parser.yy" /* yacc.c:1646  */
    { (yyval.p) = float_type; }
#line 1854 "parser.cc" /* yacc.c:1646  */
    break;

  case 74:
#line 394 "parser.yy" /* yacc.c:1646  */
    {
		if (my_params->get_use_gnulib ())
			(yyval.p) = double_type;
		else
			yyerror ("type double only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
#line 1866 "parser.cc" /* yacc.c:1646  */
    break;

  case 75:
#line 401 "parser.yy" /* yacc.c:1646  */
    { (yyval.p) = string_type; }
#line 1872 "parser.cc" /* yacc.c:1646  */
    break;

  case 76:
#line 402 "parser.yy" /* yacc.c:1646  */
    { (yyval.p) = char_type; }
#line 1878 "parser.cc" /* yacc.c:1646  */
    break;

  case 77:
#line 403 "parser.yy" /* yacc.c:1646  */
    { (yyval.p) = flag_type; }
#line 1884 "parser.cc" /* yacc.c:1646  */
    break;

  case 80:
#line 408 "parser.yy" /* yacc.c:1646  */
    { (yyval.str) = (yyvsp[-1].str); }
#line 1890 "parser.cc" /* yacc.c:1646  */
    break;

  case 82:
#line 415 "parser.yy" /* yacc.c:1646  */
    {
		param->set_low_range ((yyvsp[-2].str));
		mylog.write ((string)"  parameter range: " + (yyvsp[-2].str) + "..");
		free ((yyvsp[-2].str));
	}
#line 1900 "parser.cc" /* yacc.c:1646  */
    break;

  case 84:
#line 425 "parser.yy" /* yacc.c:1646  */
    {
		param->set_high_range ((yyvsp[0].str));
		mylog.write ((string)"  parameter range: .." + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1910 "parser.cc" /* yacc.c:1646  */
    break;

  case 85:
#line 431 "parser.yy" /* yacc.c:1646  */
    {
		param->set_high_range ((yyvsp[0].str));
		mylog.write ((string)"  parameter range: .." + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1920 "parser.cc" /* yacc.c:1646  */
    break;

  case 89:
#line 443 "parser.yy" /* yacc.c:1646  */
    { (yyval.str) = (yyvsp[-1].str); }
#line 1926 "parser.cc" /* yacc.c:1646  */
    break;

  case 90:
#line 447 "parser.yy" /* yacc.c:1646  */
    {
		param->add_description ((yyvsp[0].str), false);
		mylog.write ((string) "  description: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1936 "parser.cc" /* yacc.c:1646  */
    break;

  case 91:
#line 453 "parser.yy" /* yacc.c:1646  */
    {
		param->add_description ((yyvsp[0].str), true);
		mylog.write ((string) "  description starting in 1st column: " + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1946 "parser.cc" /* yacc.c:1646  */
    break;

  case 95:
#line 466 "parser.yy" /* yacc.c:1646  */
    {
		my_params->append_usage_line ((yyvsp[0].str));
		mylog.write ((string)"  usage string:" + (yyvsp[0].str));
		free ((yyvsp[0].str));
	}
#line 1956 "parser.cc" /* yacc.c:1646  */
    break;

  case 98:
#line 478 "parser.yy" /* yacc.c:1646  */
    {
		param->add_code_line ((yyvsp[-1].str));
		mylog.write ((string)"  code string:" + (yyvsp[-1].str));
		free ((yyvsp[-1].str));
	}
#line 1966 "parser.cc" /* yacc.c:1646  */
    break;

  case 99:
#line 484 "parser.yy" /* yacc.c:1646  */
    {
		param->add_code_line ("");
		mylog.write ((string)"  code string:");
	}
#line 1975 "parser.cc" /* yacc.c:1646  */
    break;

  case 100:
#line 491 "parser.yy" /* yacc.c:1646  */
    {
	}
#line 1982 "parser.cc" /* yacc.c:1646  */
    break;

  case 101:
#line 494 "parser.yy" /* yacc.c:1646  */
    {
		(yyval.str) = strdup ((string ("(")).c_str ());
	}
#line 1990 "parser.cc" /* yacc.c:1646  */
    break;

  case 102:
#line 498 "parser.yy" /* yacc.c:1646  */
    {
		(yyval.str) = strdup ((string (")")).c_str ());
	}
#line 1998 "parser.cc" /* yacc.c:1646  */
    break;

  case 103:
#line 502 "parser.yy" /* yacc.c:1646  */
    {
		(yyval.str) = strdup ((string ((yyvsp[-1].str)) + (yyvsp[0].str)).c_str ());
		free ((yyvsp[-1].str));
		free ((yyvsp[0].str));
	}
#line 2008 "parser.cc" /* yacc.c:1646  */
    break;


#line 2012 "parser.cc" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 508 "parser.yy" /* yacc.c:1906  */


int yyerror (const string s)
{
  cerr << s << " in " << yyfilename << ": " << yylineno << endl;
  return 1;
}

void dump_short_reps (const string s)
{
  mylog.write (LOG_OPTION_NO_CR, "  short representation: "); 
  mylog.write (LOG_OPTION_NO_TS, s);
}

void add_param_to_list ()
{
  mylog.write ("done reading parameter...adding to list");
  my_params->add (*param);
  delete param;
}

void dump_param_type (param_type p)
{
  mylog.write (LOG_OPTION_NO_CR, "  parameter type: ");
  switch (p)
    {
    case int_type:
      mylog.write (LOG_OPTION_NO_TS, "int");
      break;
    case long_type:
      mylog.write (LOG_OPTION_NO_TS, "long int");
      break;
    case ulong_type:
      mylog.write (LOG_OPTION_NO_TS, "unsigned long int");
      break;
    case intmax_type:
      mylog.write (LOG_OPTION_NO_TS, "intmax_t");
      break;
    case uintmax_type:
      mylog.write (LOG_OPTION_NO_TS, "uintmax_t");
      break;
    case float_type:
      mylog.write (LOG_OPTION_NO_TS, "float");
      break;
    case double_type:
      mylog.write (LOG_OPTION_NO_TS, "double");
      break;
    case string_type:
      mylog.write (LOG_OPTION_NO_TS, "string");
      break;
    case char_type:
      mylog.write (LOG_OPTION_NO_TS, "char");
      break;
    case flag_type:
      mylog.write (LOG_OPTION_NO_TS, "flag");
      break;
    default:
      break;
    }
}
