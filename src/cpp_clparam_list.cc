/** @file cpp_clparam_list.cc
\brief Methods for the C++ implementation of the command line parser */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <algorithm>
#include "cpp_clparam_list.h"
#include "sysinfo.h"
#include "systime.h"
#include "userinfo.h"
#include "genparse_util.h"

using genparse_util::C_comment;
using genparse_util::full_path;

/** Helper function for output_usage (). Output one print command. */
void CPP_Clparam_list::output_print_command (
  /** open stream where to print */
  ofstream& o, 
  /** Text which must be printed. Must contain starting double quotes '"', 
      ending double quotes are appended in this function. */
  const string &text) const
{
  o << "      std::cout << \"\\" << endl;
  o << text << "\";"               << endl;
}

/** set flag if Gnu Portability Library (Gnulib) shall be used */
void CPP_Clparam_list::set_use_gnulib (bool b) throw (EH)
{
  if (b)
    throw EH ("Use of GNU Portability library not supported for C++ output");
}

void CPP_Clparam_list::check_parameters (void) const
{
  if (get_internationalization ())
    cerr << "Warning: Internationalization not implemented for C++ output" << endl;
}

/** Output a class comment banner */
void CPP_Clparam_list::output_comments_class (ofstream& o, string classname, string comment) const
{
  o << "/*----------------------------------------------------------------------------" << endl;
  o << "**"                                                                             << endl;
  o << "** class " << classname                                                         << endl;
  o << "**"                                                                             << endl;
  o << "** " << comment                                                                 << endl;
  o << "**"                                                                             << endl;
  o << "**--------------------------------------------------------------------------*/" << endl;  
}

/** Output a C++ header file containing the interface of the parser class */
void CPP_Clparam_list::output_interface (const string filename_without_extension) const throw (EH)
{
  // make file .h extension
  const string filename = filename_without_extension + ".h";
  const string path = full_path (get_directory (), filename);
  list<Clparam> params = get_params ();
  list<Clparam>::const_iterator iter;
  list<string>::const_iterator str_iter;
  
  if (get_no_struct ())
    throw EH ("#no_struct not yet implemented for C++ output.");

  // tell the user what we're doing
  if (!get_quiet ())
    cout << "creating " << path << "...";
  
  // open the header file
  ofstream o;
  o.open (path.c_str ());
  if (!o)
    throw EH ("can't open file: " + path);
  
  // print a nice friendly comment banner at the beginning of the file
  output_comments_header (o, filename, "Header file for command line parser class");
  
  // do the ifdef
  o << "#ifndef " << genparse_util::str2upper (get_parsefunc ()) << "_H" << endl;
  o << "#define " << genparse_util::str2upper (get_parsefunc ()) << "_H" << endl;
  o << endl;

  // print the includes 
  o << "#include <iostream>" << endl;
  o << "#include <string>" << endl;
  output_includes (o);

  // dump the class
  output_comments_class (o, get_parsefunc (), "command line parser class");
  o << endl;
  o << "class " << get_parsefunc () << endl;
  o << "{" << endl;

  // private section
  o << "private:" << endl;
  o << "  " << C_comment ("parameters") << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      o << "  " << type2str (iter->get_type ()) << " _"
        << get_parameter_name (*iter) << ";" << endl;
      if (iter->requires_additional_flag_arg ())
        o << "  " << type2str (flag_type) << " _"
          << get_parameter_name (*iter) << "_flag;" << endl;
      if (iter->get_store_longindex ())
        o << "  " << type2str (int_type) << " _"
          << get_parameter_name (*iter) << "_li;" << endl;
    }
  o << endl;
  o << "  " << C_comment ("other stuff to keep track of") << endl;
  o << "  " << string_type_str () << " _program_name;" << endl;
  o << "  int _optind;" << endl;
  o << endl;

  // public section
  o << "public:" << endl;
  o << "  " << C_comment ("constructor and destructor") << endl;
  o << "  " << get_parsefunc () << " (int, char **) throw (" << string_type_str () << ");" << endl;
  o << "  ~" << get_parsefunc () << " (){}" << endl;
  o << endl;
  o << "  " << C_comment ("usage function") << endl;
  o << "  void usage (int status);" << endl;
  o << endl;
  o << "  " << C_comment ("return next (non-option) parameter") << endl;
  o << "  int next_param () { return _optind; }" << endl;
  o << endl;

  if (have_callbacks ())
    {
      list<string> param_callback_list;

      o << "  " << C_comment ("callback functions") << endl;
      /* global callbacks */
      if (!get_global_callback ().empty ())
        o << "  " << boolean_type_str () << ' ' << get_global_callback () << " ();" << endl;

      /* parameter related callbacks */
      for (iter = params.begin (); iter != params.end (); iter++)
        if (!(iter->get_callback ()).empty ())
          /* Print every callback function only once, no matter how many parameters use it. */
          if (find (param_callback_list.begin (), param_callback_list.end (),
            iter->get_callback ()) == param_callback_list.end ())
            {
              o << "  " << boolean_type_str () << ' ' << iter->get_callback () << " ();" << endl;
              param_callback_list.push_back (iter->get_callback ());
            }
      o << endl;
    }
  
  // functions to return parameter values ("getter functions")
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      o << "  " << type2str (iter->get_type ()) << ' ' << get_parameter_name (*iter)
        << " () const { return _" << get_parameter_name (*iter) << "; }" << endl;
      if (iter->requires_additional_flag_arg ())
        o << "  " << type2str (flag_type) << ' ' << get_parameter_name (*iter) << "_flag"
          << " () const { return _" << get_parameter_name (*iter) << "_flag; }" << endl;
      if (iter->get_store_longindex ())
        o << "  " << type2str (int_type) << ' ' << get_parameter_name (*iter) << "_li"
          << " () const { return _" << get_parameter_name (*iter) << "_li; }" << endl;
    }

  // closing bracket for class definition
  o << "};" << endl;
  o << endl;
  o << "#endif" << endl;

  if (!o)
    throw EH ("can't write to file: " + path);

  // close the file
  o.close ();
  if (!o)
    throw EH ("can't close file: " + path);

  // tell the user what we're doing
  if (!get_quiet ())
    cout << "done" << endl;
}

/** Output a C++ source file containing the implementation of the parser class */
void CPP_Clparam_list::output_implementation (const string filename_without_extension) 
  const throw (EH)
{
  // make file extension
  const string filename = filename_without_extension + "." + get_cppext ();
  const string path = full_path (get_directory (), filename);
  list<Clparam> params = get_params ();
  list<Clparam>::iterator iter;
  list<string>::const_iterator str_iter;
  ostringstream usage_stream;
  
  // tell the user what we're doing
  if (!get_quiet ())
    cout << "creating " << path << "...";
  
  // open the header file
  ofstream o;
  o.open (path.c_str ());
  if (!o)
    throw EH ("can't open file: " + path);
  
  // print a nice friendly comment banner at the beginning of the file
  output_comments_header (o, filename, "Definition of command line parser class");
  
  // includes
  o << "#include <getopt.h>" << endl;
  o << "#include <stdlib.h>" << endl;
  o << "#include \"" << filename_without_extension << ".h\"" << endl;
  o << endl;
 
  // comments for constructor
  output_function_comment (o, get_parsefunc () + "::" + get_parsefunc (), "Constructor method.");
  o << endl;

  // constructor
  o << get_parsefunc () << "::" << get_parsefunc () 
    << " (int argc, char *argv[]) throw (" << string_type_str () << " )" << endl;
  o << "{" << endl;
  
  // local variables
  o << "  extern char *optarg;" << endl;
  o << "  extern int optind;" << endl;
  o << "  int c;" << endl;
  o << endl;

  // options structure
  o << "  static struct option long_options[] =" << endl; 
  o << "  {" << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if (iter->get_long_rep ().length () > 0)
        {
          o << "    {\"" << iter->get_long_rep () << "\", ";
          switch (iter->get_long_has_arg ())
            {
            case no_argument:
              o << "no_argument";
              break;
            case optional_argument:
              o << "optional_argument";
              break;
            case required_argument:
              o << "required_argument";
              break;
            default:
              break;
            }
          o << ", NULL, ";
          if (iter->get_long_only ())
	    o << iter->get_longopt_value ();
          else
	    o << '\'' << iter->get_short_rep (0) << '\'';
          o << "}," << endl;
        }
    }
  o << "    {NULL, 0, NULL, 0}" << endl;
  o << "  };" << endl;
  o << endl;

  o << "  _program_name += argv[0];" << endl;
  o << endl;

  // assign default values
  o << "  " << C_comment ("default values") << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      switch (iter->get_type ())
        {
        case flag_type:  // all flags default to off
          o << "  _" << get_parameter_name (*iter) << " = false;" << endl;
          break;
        case string_type: // copy default if there, NULL otherwise
          if (!iter->get_default_value ().empty ())
            o << "  _" << get_parameter_name (*iter)
              << " = \"" << iter->get_default_value () << "\";" << endl;
          break;
        default: // all else - copy default if there
          if (!iter->get_default_value ().empty ())
            o << "  _" << get_parameter_name (*iter)
              << " = " << iter->get_default_value () << ";" << endl;
        }
      if (iter->requires_additional_flag_arg ())
        o << "  _" << get_parameter_name (*iter) << "_flag = false;" << endl;
      if (iter->get_store_longindex ())
        o << "  _" << get_parameter_name (*iter) << "_li = 0;" << endl;
    }
  o << endl;

  // reset global optind variable, otherwise it would not be possible to instantiate the parser
  // class multiple times in the same program
  o << "  optind = 0;" << endl;

  // Make the getopt_long () call
  o << "  while ((c = getopt_long (argc, argv, \"";
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if (!iter->get_long_only ())
        {
          o << iter->get_short_reps ();
          if (iter->get_short_has_arg () == required_argument)
            o << ':';
          else if (iter->get_short_has_arg () == optional_argument)
            o << "::";
        }
    }
  o << "\", long_options, &optind)) != - 1)" << endl;

  // do the switch statement
  o << "    {" << endl;
  o << "      switch (c)" << endl;
  o << "        {" << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      unsigned int indent_width;
      #define INDENT string (indent_width, ' ')

      if (iter->get_long_only ())
        o << "        case " << iter->get_longopt_value () << ": " << endl;
      else
        {
          for (unsigned int i = 0; i < iter->get_num_short_reps ();i++)
            o << "        case '" << iter->get_short_rep (i) << "': " << endl;
        }

      indent_width = 10;
      if (iter->requires_additional_flag_arg ())
        {
          o << INDENT << '_' << get_parameter_name (*iter) << "_flag = true;" << endl;
          o << INDENT << "if (optarg != NULL)" << endl;
          indent_width += 2;
        }
      o << INDENT << '_' << get_parameter_name (*iter);
      switch (iter->get_type ())
        {
        case string_type:
          o << " = optarg;" << endl;
          break;
        case int_type:
          o << " = atoi (optarg);" << endl;
          break;
        case float_type:
          o << " = atof (optarg);" << endl;
          break;
        case char_type:
          o << " = *optarg;" << endl;
          break;
        case flag_type:
          o << " = true;" << endl;
          if (iter->get_long_rep () == "help")
	    o << INDENT << "this->usage (EXIT_SUCCESS);" << endl;
	  break;
        default:
          break;
        }
      if (iter->get_store_longindex ())
        o << INDENT << '_' << get_parameter_name (*iter) << "_li = optind;" << endl;

      // do low range checking
      if (!iter->get_low_range ().empty ())
        {
          o << INDENT << "if (_" << get_parameter_name (*iter)
            << " < " << iter->get_low_range () << ")" << endl;
          indent_width += 2;
          o << INDENT << '{' << endl;
          indent_width += 2;
          o << INDENT << string_type_str () << " s;" << endl;
	  o << INDENT << "s += \"parameter range error: " 
	    << get_parameter_name (*iter) << " must be >= " << iter->get_low_range () 
	    << "\";" << endl;
	  o << INDENT << "throw (s);" << endl;
          indent_width -= 2;
          o << INDENT << '}' << endl;
          indent_width -= 2;
        }
      // do high range checking
      if (!iter->get_high_range ().empty ())
        {
          o << INDENT << "if (_" << get_parameter_name (*iter)
            << " > " << iter->get_high_range () << ")" << endl;
          indent_width += 2;
          o << INDENT << '{' << endl;
          indent_width += 2;
	  o << INDENT << string_type_str () << " s;" << endl;
	  o << INDENT << "s += \"parameter range error: " 
            << get_parameter_name (*iter) << " must be <= " << iter->get_high_range () 
	    << "\";" << endl;
	  o << INDENT << "throw (s);" << endl;
          indent_width -= 2;
          o << INDENT << '}' << endl;
          indent_width -= 2;
        }

      // write user defined code for this parameter
      list<string> code_lines = iter->get_code_lines ();
      list<string>::const_iterator code_iter;
      for (code_iter = code_lines.begin (); code_iter != code_lines.end (); code_iter++)
        o << INDENT << *code_iter                                                   << endl;

      // do callbacks
      if (!iter->get_callback ().empty ())
        {
          o << INDENT << "if (!" << iter->get_callback () << " ())" << endl;
          indent_width += 2;
          o << INDENT << "this->usage (" << get_exit_value () << ");" << endl;
          indent_width -= 2;
        }
      
      // do break statement
      o << INDENT << "break;"                                                       << endl 
                                                                                    << endl;            
    }

  // print default action for unknown parameters
  o << "        default:" << endl;
  o << "          this->usage (" << get_exit_value () << ");" << endl << endl;
  o << "        }" << endl;
  o << "    } /* while */" << endl << endl;

  // save a pointer to the next argument
  o << "  _optind = optind;" << endl;

  // do global callbacks
  if (!get_global_callback ().empty ())
    {
      o << "  if (!" << get_global_callback () << " ())" << endl;
      o << "    usage (" << get_exit_value () << ");"                  << endl << endl;
    }
  
  o << "}" << endl;
  o << endl;

  // comments for usage ()
  output_function_comment (o, get_parsefunc () + "::usage", 
    "Print out usage information, then exit.");
  o << endl;

  // usage function
  o << "void " << get_parsefunc () << "::usage (int status)" << endl;
  o << "{"                                                 << endl;
  o << "  if (status != EXIT_SUCCESS)"                     << endl;
  o << "    std::cerr << \"Try `\" << _program_name << \" --help' for more information.\\n\";" << endl;
  o << "  else"                                            << endl;
  o << "    {"                                             << endl;
  list<string> usage_list = get_usage ();
  for (str_iter = usage_list.begin (); str_iter != usage_list.end (); str_iter++)
    {
      string::size_type macro_pos, arg_start_pos, arg_end_pos, closing_bracket_pos;
      string macro_argument;
      bool line_manipulated;

      string line = *str_iter;
      do
        {
          if ((macro_pos = line.find (string ("__PROGRAM_NAME__"))) != string::npos)
            {
              line.replace (macro_pos, 16, string ("\" << _program_name << \""));
            }
          else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
            "__STRING__", line)) != string::npos)
            {
              macro_argument = string (line, arg_start_pos, arg_end_pos - arg_start_pos + 1);
              line.replace (macro_pos, closing_bracket_pos - macro_pos + 1, 
                string ("\" << ") + macro_argument + string (" << \""));
            }
          else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
            "__INT__", line)) != string::npos)
            {
              macro_argument = string (line, arg_start_pos, arg_end_pos - arg_start_pos + 1);
              line.replace (macro_pos, closing_bracket_pos - macro_pos + 1, 
                string ("\" << ") + macro_argument + string (" << \""));
            }
        }
      while (macro_pos != string::npos);
      if (line.find ("__DO_NOT_DOCUMENT__") == string::npos)
        {
          line_manipulated = false;

          do
            {
              if ((macro_pos = line.find (string ("__NEW_PRINT__"))) != string::npos)
                {
                  if (macro_pos > 0)
                    {
                      if (!usage_stream.str ().empty ())
                        usage_stream << "\\" << endl;
                      usage_stream << string (line, 0, macro_pos);
                      usage_stream << "\\n";
                    }

                  if (usage_stream.str ().length () > 0)
                    output_print_command (o, usage_stream.str ());

                  usage_stream.str ("");

                  /* Remove __NEW_PRINT__ and any characters before */
                  line.erase (0, macro_pos + 13);

                  line_manipulated = true;
                }
              else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
                "__CODE__", line)) != string::npos)
                {
                  if (macro_pos > 0)
                    {
                      if (!usage_stream.str ().empty ())
                        usage_stream << "\\" << endl;
                      usage_stream << string (line, 0, macro_pos);
                      usage_stream << "\\n";
                    }

                  if (usage_stream.str ().length () > 0)
                    output_print_command (o, usage_stream.str ());

                  o << "      " 
                    << line.substr (arg_start_pos, closing_bracket_pos - arg_start_pos) << endl;

                  usage_stream.str ("");

                  /* Remove __CODE__() and any characters before */
                  line.erase (0, closing_bracket_pos + 1);

                  line_manipulated = true;
                }
              else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
                "__COMMENT__", line)) != string::npos)
                {
                  if (macro_pos > 0)
                    {
                      if (!usage_stream.str ().empty ())
                        usage_stream << "\\" << endl;
                      usage_stream << string (line, 0, macro_pos);
                      usage_stream << "\\n";
                    }

                  if (usage_stream.str ().length () > 0)
                    output_print_command (o, usage_stream.str ());

                  usage_stream.str ("");

                  o << "      // " 
                    << line.substr (arg_start_pos, closing_bracket_pos - arg_start_pos) << endl;

                  /* Remove __COMMENT__() and any characters before */
                  line.erase (0, closing_bracket_pos + 1);

                  line_manipulated = true;
                }
            }
          while (macro_pos != string::npos);
          if (!line_manipulated || !line.empty ())
            {
              if (!usage_stream.str ().empty ())
                usage_stream << "\\" << endl;
              usage_stream << line << "\\n";
            }
        }
    }
  if (usage_stream.str ().length () > 0)
    output_print_command (o, usage_stream.str ());
  o << "    }" << endl;

  o << "  exit (status);" << endl;
  o << "}" << endl;

  if (!o)
    throw EH ("can't write to file: " + path);

  // close the C++ file
  o.close ();
  if (!o)
    throw EH ("can't close file: " + path);

  // tell the user what we're doing
  if (!get_quiet ()) 
    cout << "done" << endl;  
}

/** Output a C++ source file containing the default implementation of the callback functions */
void CPP_Clparam_list::output_callbacks (const string filename_without_extension) const throw (EH)
{
  list<Clparam> params = get_params ();
  list<string>::const_iterator str_iter;
  list<Clparam>::const_iterator iter;

  // first figure out if we have to do anything at all - no callbacks = no
  // callback file
  if (have_callbacks ())
    {
      // make file extension
      const string filename = filename_without_extension + "_cb." + get_cppext ();
      const string path = full_path (get_directory (), filename);
      list<string> param_callback_list;
      
      // tell the user what we're doing
      if (!get_quiet ())
	cout << "creating " << path << "...";
      
      // open the callbacks file
      ofstream o;
      o.open (path.c_str ());
      if (!o)
        throw EH ("can't open file: " + path);
      
      // print a nice friendly comment banner at the beginning of the file
      output_comments_header (o, filename, "Callbacks for command line parser class");
      
      // includes
      o << "#include \"" << filename_without_extension << ".h\"" << endl;
      
      // loop through global callbacks
      if (!get_global_callback ().empty ())
	{
          o << endl;

	  // comments for 
	  output_function_comment (o, get_parsefunc () + "::" + get_global_callback (),
				   "Global callback.");
	  o << endl;
	  o << boolean_type_str () << ' ' << get_parsefunc () << "::" << get_global_callback () 
            << " ()" << endl;
          o << "{" << endl;
          o << "  return true;" << endl;
          o << "}" << endl;
	}

      // do the parameter callbacks
      for (iter = params.begin (); iter != params.end (); iter++)
        if (!iter->get_callback ().empty ())
          /* Print every callback function only once, no matter how many parameters use it. */
          if (find (param_callback_list.begin (), param_callback_list.end (),
            iter->get_callback ()) == param_callback_list.end ())
            {
              o << endl;

	      output_function_comment (o, get_parsefunc () + "::" + iter->get_callback (),
				       "Parameter callback.");
	      o << endl;
              o << boolean_type_str () << ' ' << get_parsefunc () << "::" <<
		iter->get_callback () << " ()" << endl;
              o << "{" << endl;
              o << "  return true;" << endl;
              o << "}" << endl;

              param_callback_list.push_back (iter->get_callback ());
            }

      if (!o)
        throw EH ("can't write to file: " + path);

      // close the callback file
      o.close ();
      if (!o)
        throw EH ("can't close file: " + path);

      // tell the user what we're doing
      if (!get_quiet ())
	cout << "done" << endl;
    } // if
}
