/** @file sysinfo.h
\brief Class definition for UNIX information on the system */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYSINFO_H
#define SYSINFO_H

#include <string>
#include "eh_system.h"

//---------------------------------------------------------------------------
//
// class Sysinfo
//
//---------------------------------------------------------------------------

class Sysinfo
{
private:
  string _name;       // name of the operating system, e.g., "Linux"
  string _release;    // kernel verion, e.g., "2.2.13"
  string _build;      // date of kernel compilation, etc.
  string _hardware;   // type of processor, e.g., i686
  string _domainname; // Domain name - this doesn't work on linux...

public:
  // Constructor and destructor
  Sysinfo () throw (EH_system);
  ~Sysinfo () {}

  // functions to retrieve the values
  string name ()       { return _name; }
  string release ()    { return _release; }
  string build ()      { return _build; }
  string hardware ()   { return _hardware; }
  string domainname () { return _domainname; }

  // format the output into somthing I like
  string format () { return _name + " " + _release + 
		      " (" + _build + ") " + _hardware; }
};

#endif



