/** @file c_clparam_list.h
\brief C implementation of the command line parser */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef C_CLPARAM_LIST_H
#define C_CLPARAM_LIST_H

#include <string>
#include "clparam_list.h"

/**
\brief C implementation of class Clparam_list

Generates C output files for parsing a command line.

3 Files can be generated: 
  - a C header file
  - a C source file containing the parser class
  - a C source file containing callback functions
*/
class C_Clparam_list: public Clparam_list
{
private:
  /** Add internationalization macro _() */
  string add_i18n_macro (string s) const { return "_(" + s + ')'; }

  void output_print_command    (ofstream& o, const string &text, const string &args) const;
  void output_usage (           ofstream& o) const;
  void output_get_long_options (ofstream& o) const;

  bool implementation_requires_error_h    (void) const;
  bool implementation_requires_xstrtol_h  (void) const;
  bool implementation_requires_xstrtod_h  (void) const;
  bool implementation_requires_strtod_h   (void) const;
  bool interface_requires_xstrtol_h       (void) const;

  const string string_type_str (void) const { return "char *"; }

  // output C code
  void output_interface (     string filename_without_extension) const throw (EH);
  void output_implementation (string filename_without_extension) const throw (EH);
  void output_callbacks (     string filename_without_extension) const throw (EH);

public:
  C_Clparam_list () {}
  ~C_Clparam_list () {}
};

#endif  // C_CLPARAM_LIST_H
