/** @file c_clparam_list.cc
\brief Methods for the C implementation of the command line parser */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <algorithm>
#include "c_clparam_list.h"
#include "sysinfo.h"
#include "systime.h"
#include "userinfo.h"
#include "genparse_util.h"

using genparse_util::full_path;
using genparse_util::str2C;

/** Helper function for output_usage (). Output one print command. Uses the "print" command
if there are arguments (args not empty), "puts" otherwise. */
void C_Clparam_list::output_print_command (
  /** open stream where to print */
  ofstream& o, 
  /** body of the message */
  const string &text, 
  /** list of arguments with colons between them, e.g. ", x, y" */
  const string &args) const
{
  string usage_string;

  usage_string = "\"\\\n";
  o << "      ";
  /* puts () always prints trailing newline "\n", so it can only be used if the string which
     has to be printed ends with a newline. */
  if (args.empty () && (text.length () >= 2) && (text.substr (text.length () - 2, 2) == "\\n"))
    {
      o << "puts";
      /* Do not print the trailing "\n" if the command is puts. */
      usage_string += text.substr (0, text.length () - 2);
    }
  else
    {
      o << "printf";
      usage_string += text;
    }
  usage_string += '\"';
  o << " (";
  if (get_internationalization ())
    o << add_i18n_macro (usage_string);
  else
    o << usage_string;
  o << args << ");" << endl;
}

/** Output the usage function */
void C_Clparam_list::output_usage (ofstream& o) const
{
  list<string>::const_iterator iter;
  ostringstream usage_stream, arg_stream;

  // comments
  output_function_comment (o, "usage", "Print out usage information, then exit");

  // declaration
  o                                                         << endl;
  o << "void usage (int status, char *program_name)"        << endl;
  o << "{"                                                  << endl;
  o << "  if (status != EXIT_SUCCESS)"                      << endl;
  o << "    fprintf (stderr, ";
  string s ("\"Try `%s --help' for more information.\\n\"");
  if (get_internationalization ())
    o << add_i18n_macro (s);
  else
    o << s;
  o << ','                                                  << endl;
  o << "            program_name);"                         << endl;
  o << "  else"                                             << endl;
  o << "    {"                                              << endl;

  list<string> usage_list = get_usage ();
  for (iter = usage_list.begin (); iter != usage_list.end (); iter++)
    {
      string::size_type macro_pos, arg_start_pos, arg_end_pos, closing_bracket_pos;
      bool line_manipulated;

      string line = *iter;
      do
        {
          if ((macro_pos = line.find (string ("__PROGRAM_NAME__"))) != string::npos)
            {
              line.replace (macro_pos, 16, string ("%s"));
              arg_stream << ", program_name";
            }
          else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
            "__STRING__", line)) != string::npos)
            {
              arg_stream << ", " << string (line, arg_start_pos, arg_end_pos - arg_start_pos + 1);
              line.replace (macro_pos, closing_bracket_pos - macro_pos + 1, string ("%s"));
            }
          else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
            "__INT__", line)) != string::npos)
            {
              arg_stream << ", " << string (line, arg_start_pos, arg_end_pos - arg_start_pos + 1);
              line.replace (macro_pos, closing_bracket_pos - macro_pos + 1, string ("%d"));
            }
        }
      while (macro_pos != string::npos);
      if (line.find ("__DO_NOT_DOCUMENT__") == string::npos)
        {
          line_manipulated = false;

          do
            {
              if ((macro_pos = line.find (string ("__NEW_PRINT__"))) != string::npos)
                {
                  if (macro_pos > 0)
                    {
                      if (!usage_stream.str ().empty ())
                        usage_stream << "\\" << endl;
                      usage_stream << string (line, 0, macro_pos);
                      usage_stream << "\\n";
                    }

                  if (!usage_stream.str ().empty ())
                    output_print_command (o, usage_stream.str (), arg_stream.str ());

                  usage_stream.str ("");
                  arg_stream.str ("");

                  /* Remove __NEW_PRINT__ and any characters before */
                  line.erase (0, macro_pos + 13);

                  line_manipulated = true;
                }
              else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
                "__CODE__", line)) != string::npos)
                {
                  if (macro_pos > 0)
                    {
                      if (!usage_stream.str ().empty ())
                        usage_stream << "\\" << endl;
                      usage_stream << string (line, 0, macro_pos);
                      usage_stream << "\\n";
                    }

                  if (!usage_stream.str ().empty ())
                    output_print_command (o, usage_stream.str (), arg_stream.str ());
                  o << "      " 
                    << line.substr (arg_start_pos, closing_bracket_pos - arg_start_pos) << endl;

                  usage_stream.str ("");
                  arg_stream.str ("");

                  /* Remove __CODE__() and any characters before */
                  line.erase (0, closing_bracket_pos + 1);

                  line_manipulated = true;
                }
              else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
                "__COMMENT__", line)) != string::npos)
                {
                  if (macro_pos > 0)
                    {
                      if (!usage_stream.str ().empty ())
                        usage_stream << "\\" << endl;
                      usage_stream << string (line, 0, macro_pos);
                      usage_stream << "\\n";
                    }

                  if (!usage_stream.str ().empty ())
                    output_print_command (o, usage_stream.str (), arg_stream.str ());

                  usage_stream.str ("");
                  arg_stream.str ("");

                  o << "      /* " 
                    << line.substr (arg_start_pos, closing_bracket_pos - arg_start_pos)
                    << " */" << endl;

                  /* Remove __COMMENT__() and any characters before */
                  line.erase (0, closing_bracket_pos + 1);

                  line_manipulated = true;
                }
            }
          while (macro_pos != string::npos);
          if (!line_manipulated || !line.empty ())
            {
              if (!usage_stream.str ().empty ())
                usage_stream << "\\" << endl;
              usage_stream << line << "\\n";
            }
        }
    }
  if (!usage_stream.str ().empty ())
    output_print_command (o, usage_stream.str (), arg_stream.str ());

  // back matter
  o << "    }"            << endl;
  o << "  exit (status);" << endl;
  o << "}"                << endl;
}


/** Output the get_long_options function */
void C_Clparam_list::output_get_long_options (ofstream& o) const
{
  // comments
  output_function_comment (o, "get_long_options", "Return long_options struct");
  o                                                   << endl;
  o << "struct option const *get_long_options (void)" << endl;
  o << "{"                                            << endl;
  o << "  return long_options;"                       << endl;
  o << "}"                                            << endl;
}

/** Returns true if the parser interface (include file) requires xstrtol.h */
bool C_Clparam_list::interface_requires_xstrtol_h (void) const
{
  list<Clparam> params = get_params ();
  list<Clparam>::const_iterator iter;

  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if ((iter->get_type () == intmax_type) || (iter->get_type () == uintmax_type))
        return true;
    }

  return false;
}

/** Returns true if the parser implementation (.c file) requires error.h */
bool C_Clparam_list::implementation_requires_error_h (void) const
{
  list<Clparam> params = get_params ();
  list<Clparam>::const_iterator iter;

  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if ((iter->get_type () ==   long_type) || (iter->get_type () ==   ulong_type) 
       || (iter->get_type () == intmax_type) || (iter->get_type () == uintmax_type)
       || (iter->get_type () == double_type))
        return true;
    }

  return false;
}

/** Returns true if the parser implementation (.c file) requires xstrtol.h */
bool C_Clparam_list::implementation_requires_xstrtol_h (void) const
{
  list<Clparam> params = get_params ();
  list<Clparam>::const_iterator iter;

  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if ((iter->get_type () ==   long_type) || (iter->get_type () ==   ulong_type) 
       || (iter->get_type () == intmax_type) || (iter->get_type () == uintmax_type))
        return true;
    }

  return false;
}

/** Returns true if the parser implementation (.c file) requires xstrtod.h */
bool C_Clparam_list::implementation_requires_xstrtod_h (void) const
{
  list<Clparam> params = get_params ();
  list<Clparam>::const_iterator iter;

  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if (iter->get_type () == double_type)
        return true;
    }

  return false;
}

/** Returns true if the parser implementation (.c file) requires strtod.h */
bool C_Clparam_list::implementation_requires_strtod_h (void) const
{
  return implementation_requires_xstrtod_h ();
}

/** Output the C header file containing the interface of the parser class */
void C_Clparam_list::output_interface (const string filename_without_extension) const throw (EH)
{
  // make file .h extension
  const string filename = filename_without_extension + ".h";
  const string path = full_path (get_directory (), filename);
  list<Clparam> params = get_params ();
  list<Clparam>::const_iterator iter;

  // tell the user what we're doing
  if (!get_quiet ())
    cout << "creating " << path << "...";
  
  // open the header file
  ofstream o;
  o.open (path.c_str ());
  if (!o)
    throw EH ("can't open file: " + path);

  // print a nice friendly comment banner at the beginning of the file
  output_comments_header (o, filename, "Header file for command line parser");
  
  // print the includes 
  o << "#include <stdio.h>"                                         << endl;
  if (interface_requires_xstrtol_h())
    o << "#include \"xstrtol.h\""                                   << endl;
  // add include files specified in the genparse (.gp) file
  output_includes (o);

  // add a bool type for C
  o << "#ifndef bool"                                               << endl;
  o << "typedef enum bool_t"                                        << endl;
  o << "{"                                                          << endl;
  o << "  false = 0, true"                                          << endl;
  o << "} bool;"                                                    << endl;
  o << "#endif"                                                     << endl 
                                                                    << endl;

  if (!get_no_struct ())
    {
      o << "/* customized structure for command line parameters */" << endl;
      o << "struct arg_t"                                           << endl;
      o << "{"                                                      << endl;
      for (iter = params.begin (); iter != params.end (); iter++)
        {
          o << "  " << type2str (iter->get_type ()) << ' '
            << get_parameter_name (*iter) << ';'                    << endl;
          if (iter->requires_additional_flag_arg ())
            o << "  " << type2str (flag_type) << ' '
              << get_parameter_name (*iter) << "_flag;"             << endl;
          if (iter->get_store_longindex ())
            o << "  " << type2str (int_type) << ' '
              << get_parameter_name (*iter) << "_li;"               << endl;
        }
      o << "  int optind;"                                          << endl;
      o << "};"                                                     << endl 
                                                                    << endl;
    }

  // function prototypes
  o << "/* function prototypes */"                                  << endl;
  o << "void " << get_parsefunc () << " (";
  if (!get_no_struct ())
    o << "struct arg_t *my_args, ";
  o << "int argc, char *argv[]);"                                   << endl;
  o << "void usage (int status, char *program_name);"               << endl;
  if (get_export_long_options ())
    o << "struct option const *get_long_options (void);"            << endl;

  // more prototypes, for the callbacks, global and local
  if (have_callbacks ())
    {
      list<string> param_callback_list;

      o                                                             << endl;
      o << "/* callback functions */"                               << endl;
      /* global callback function */
      if (!get_global_callback ().empty ())
        o << "int " << get_global_callback () 
          << " (struct arg_t *);"                                   << endl 
                                                                    << endl;

      /* parameter related callbacks */
      for (iter = params.begin (); iter != params.end (); iter++)
        {
          if (!iter->get_callback ().empty ())
            /* Print every callback function only once, no matter how many parameters use it. */
            if (find (param_callback_list.begin (), param_callback_list.end (), 
              iter->get_callback ()) == param_callback_list.end ())
              {
                o << "int " << iter->get_callback () << " (" << type2str (iter->get_type ()) 
                  << ");" << endl;
                param_callback_list.push_back (iter->get_callback ());
              }
        }
    }

  if (!o)
    throw EH ("can't write to file: " + path);

  // close the header file
  o.close ();
  if (!o)
    throw EH ("can't close file: " + path);

  // tell the user what we're doing
  if (!get_quiet ())
    cout << "done" << endl;
}

/** Output the C source file containing the implementation of the parser struct */
void C_Clparam_list::output_implementation (const string filename_without_extension) 
  const throw (EH)
{
  list<Clparam> params = get_params ();
  list<Clparam>::const_iterator iter;

  // make file .c extension
  const string filename = filename_without_extension + ".c";
  const string path = full_path (get_directory (), filename);

  // tell the user what we're doing
  if (!get_quiet ()) 
    cout << "creating " << path << "...";

  // open the c file
  ofstream o;
  o.open (path.c_str ());
  if (!o)
    throw EH ("can't open file: " + path);

  // print a nice friendly comment banner at the beginning of the file
  output_comments_header (o, filename, "C file for command line parser");
  
  // print the includes 
  o << "#include <string.h>"                                          << endl;
  o << "#include <stdlib.h>"                                          << endl;
  o << "#include <getopt.h>"                                          << endl;
  if (implementation_requires_error_h ())
    o << "#include \"error.h\""                                       << endl;
  if (implementation_requires_xstrtol_h ())
    o << "#include \"xstrtol.h\""                                     << endl;
  if (implementation_requires_xstrtod_h ())
    o << "#include \"xstrtod.h\""                                     << endl;
  if (implementation_requires_strtod_h ())
    {
      if (get_internationalization ())
        o << "#include \"c-strtod.h\""                                << endl;
      else
        o << "#include \"xstrtod.h\""                                 << endl;
    }
  o << "#include \"" << filename_without_extension + ".h" << "\""     << endl 
                                                                      << endl;
  
  // write the options structure
  o << "static struct option const long_options[] ="                  << endl;
  o << "{"                                                            << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if (iter->get_long_rep ().length () > 0)
        {
          o << "  {\"" << iter->get_long_rep () << "\", ";
          switch (iter->get_long_has_arg ())
            {
            case no_argument:
              o << "no_argument";
              break;
            case optional_argument:
              o << "optional_argument";
              break;
            case required_argument:
              o << "required_argument";
              break;
            default:
              break;
            }
          o << ", NULL, ";
          if (iter->get_long_only ())
            o << iter->get_longopt_value ();
          else
            o << '\'' << iter->get_short_rep (0) << '\'';
          o << "},"                                                   << endl;
        }
    }
  o << "  {NULL, 0, NULL, 0}"                                         << endl;
  o << "};"                                                           << endl 
                                                                      << endl;
  
  // comments
  output_function_comment (o, get_parsefunc (), 
    "Parse the argv array of command line parameters");
  
  // declaration and local vars
  o                                                                   << endl;
  o << "void " << get_parsefunc () << " (";
  if (!get_no_struct ())
    o << "struct arg_t *my_args, ";
  o << "int argc, char *argv[])"                                      << endl;
  o << "{"                                                            << endl;
  o << "  extern char *optarg;"                                       << endl;
  o << "  extern int optind;"                                         << endl;
  o << "  int c;"                                                     << endl;
  o << "  int errflg = 0;"                                            << endl 
                                                                      << endl;

  if (!get_no_struct ())
    {
      // write initialisations
      for (iter = params.begin (); iter != params.end (); iter++)
        {
          switch (iter->get_type ())
            {
            case flag_type:  // all flags default to off
               o << "  my_args->" << get_parameter_name (*iter) 
                << " = false;"                                        << endl;
              break;
            case string_type: // copy default if there, NULL otherwise
              if (!iter->get_default_value ().empty ())
                o << "  my_args->" << get_parameter_name (*iter)
                  << " = \"" << iter->get_default_value () << "\";"   << endl;
              else
                o << "  my_args->" << get_parameter_name (*iter) 
                  << " = NULL;"                                       << endl;
              break;
            default: // all else - copy default if there
              if (!iter->get_default_value ().empty ())
                o << "  my_args->" << get_parameter_name (*iter)
                  << " = " << iter->get_default_value () << ";"       << endl;
            }
          if (iter->requires_additional_flag_arg ())
            o << "  my_args->" << get_parameter_name (*iter) 
              << "_flag = false;"                                     << endl;
          if (iter->get_store_longindex ())
            o << "  my_args->" << get_parameter_name (*iter) 
              << "_li = 0;"                                           << endl;
        }
      o                                                               << endl;
    }

  // reset global optind variable, otherwise it would not be possible to instantiate the parser
  // class multiple times in the same program
  o << "  optind = 0;"                                                << endl;

  // Make the getopt_long () call
  o << "  while ((c = getopt_long (argc, argv, \"";
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if (!iter->get_long_only ())
        {
          o << iter->get_short_reps ();
          if (iter->get_short_has_arg () == required_argument)
            o << ':';
          else if (iter->get_short_has_arg () == optional_argument)
            o << "::";
        }
    }
  o << "\", long_options, &optind)) != - 1)"                          << endl;

  // do the switch statement
  o << "    {"                                                        << endl;
  o << "      switch (c)"                                             << endl;
  o << "        {"                                                    << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      unsigned int indent_width;
      #define INDENT string (indent_width, ' ')

      if (iter->get_long_only ())
        o << "        case " << iter->get_longopt_value () << ":"     << endl;
      else
        {
          for (unsigned int i = 0; i < iter->get_num_short_reps ();i++)
            o << "        case '" << iter->get_short_rep (i) << "':"  << endl;
        }

      indent_width = 10;
      if (!get_no_struct ())
        {
          if (iter->requires_additional_flag_arg ())
            {
              o << INDENT << "my_args->" << get_parameter_name (*iter) 
                << "_flag = true;"                                    << endl;
              o << INDENT << "if (optarg != NULL)"                    << endl;
              indent_width += 2;
            }

          if (get_use_gnulib () 
              && ((iter->get_type () == long_type)   || (iter->get_type () == ulong_type) 
               || (iter->get_type () == intmax_type) || (iter->get_type () == uintmax_type) 
               || (iter->get_type () == double_type)))
            {
              string err_msg;

              o << INDENT << "if (";
              switch (iter->get_type ())
                {
                case long_type:
                  o << "xstrtol";
                  break;
                case ulong_type:
                  o << "xstrtoul";
                  break;
                case intmax_type:
                  o << "xstrtoimax";
                  break;
                case uintmax_type:
                  o << "xstrtoumax";
                  break;
                case double_type:
                  o << "! xstrtod";
                  break;
                default:
                  break;
                }
              o << " (optarg, NULL";
              if (iter->get_type () != double_type)
                o << ", 10";
              o << ", &my_args->" << get_parameter_name (*iter) << ", ";
              if (iter->get_type () == double_type)
                if (get_internationalization ())
                  o << "c_strtod)";
                else
                  o << "strtod)";
              else
                o << "NULL) != LONGINT_OK";
              if (!iter->get_err_msg ().empty ())
                {
                  if (!iter->get_low_range ().empty ())
                    {
                      o                                                             << endl;
                      o << INDENT << "    || my_args->" << get_parameter_name (*iter) 
                        << " < " << iter->get_low_range ();
                    }
                  if (!iter->get_high_range ().empty ())
                    {
                      o                                                             << endl;
                      o << INDENT << "    || my_args->" << get_parameter_name (*iter) 
                        << " > " << iter->get_high_range ();
                    }
                }
              o << ')'                                                              << endl;
              indent_width += 2;
              o << INDENT << "error (" << get_exit_value () << ", 0, ";
              if (!iter->get_err_msg ().empty ())
                err_msg = iter->get_err_msg ();
              else
                err_msg = string ("\"could not convert %s to " + type2str (iter->get_type ()) + '\"');
              if (get_internationalization ())
                o << add_i18n_macro (err_msg);
              else
                o << err_msg;
              o << ", ";
              if (!iter->get_err_msg_conv ().empty ())
                o << iter->get_err_msg_conv () << " (";
              o << "optarg";
              if (!iter->get_err_msg_conv ().empty ())
                o << ')';
              o << ");"                                                             << endl;
              indent_width -= 2;
            }
          else
            {
              o << INDENT << "my_args->" << get_parameter_name (*iter);
              switch (iter->get_type ())
                {
                case string_type:
                  o << " = optarg;"                                                 << endl;
                  break;
                case int_type:
                  o << " = atoi (optarg);"                                          << endl;
                  break;
                case float_type:
                 o << " = atof (optarg);"                                           << endl;
                  break;
                case char_type:
                  o << " = *optarg;"                                                << endl;
                  break;
                case flag_type:
                  o << " = true;"                                                   << endl;
                  if (iter->get_long_rep () == "help")
                    o << INDENT << "usage (EXIT_SUCCESS, argv[0]);"                 << endl;
                  break;
                default:
                  break;
                }
            }

          if (iter->requires_additional_flag_arg ())
            indent_width -= 2;

          // store longindex (last argument to getopt_long) if requested
          if (iter->get_store_longindex ())
            {
              o << INDENT << "my_args->" << get_parameter_name (*iter) 
                << "_li = optind;"                                                  << endl;
            }
        }

      // do low range checking
      if ((iter->get_err_msg ().empty ()) && (!iter->get_low_range ().empty ()))
        {
          o << INDENT << "if (my_args->" << get_parameter_name (*iter)
            << " < " << iter->get_low_range () << ")"                               << endl;
          indent_width += 2;
          o << INDENT << "{"                                                        << endl;
          indent_width += 2;
          o << INDENT << "fprintf (stderr, ";
          string s ("\"parameter range error: " + 
            get_parameter_name (*iter) + " must be >= " +
            iter->get_low_range () + "\\n\"");
          if (get_internationalization ())
            o << add_i18n_macro (s);
          else
            o << s;
          o << ");"                                                                 << endl;
          o << INDENT << "errflg++;"                                                << endl;
          indent_width -= 2;
          o << INDENT << "}"                                                        << endl;
          indent_width -= 2;
        }
      // do high range checking
      if ((iter->get_err_msg ().empty ()) && (!iter->get_high_range ().empty ()))
        {
          o << INDENT << "if (my_args->" << get_parameter_name (*iter)
            << " > " << iter->get_high_range () << ")"                              << endl;
          indent_width += 2;
          o << INDENT << "{"                                                        << endl;
          indent_width += 2;
          o << INDENT << "fprintf (stderr, ";
          string s ("\"parameter range error: " +
            get_parameter_name (*iter) + " must be <= " +
            iter->get_high_range () + "\\n\"");
          if (get_internationalization ())
            o << add_i18n_macro (s);
          else
            o << s;
          o << ");"                                                                 << endl;
          o << INDENT << "errflg++;"                                                << endl;
          indent_width -= 2;
          o << INDENT << "}"                                                        << endl;
          indent_width -= 2;
        }

      // write user defined code for this parameter
      list<string> code_lines = iter->get_code_lines ();
      list<string>::const_iterator code_iter;
      for (code_iter = code_lines.begin (); code_iter != code_lines.end (); code_iter++)
        o << INDENT << *code_iter                                                   << endl;

      // do callbacks
      if (!iter->get_callback ().empty ())
        {
          o << INDENT << "if (!" << iter->get_callback ()
            << " (my_args->" << str2C (get_parameter_name (*iter))
            << "))"                                                                 << endl;
          indent_width += 2;
          o << INDENT << "usage (" << get_exit_value () << ", argv[0]);"            << endl;
          indent_width -= 2;
        }

      // do break statement
      o << INDENT << "break;"                                                       << endl 
                                                                                    << endl;            
    }

  // print default action for unknown parameters
  o << "        default:"                                                           << endl;
  o << "          usage (" << get_exit_value () << ", argv[0]);"                    << endl 
                                                                                    << endl;

  // print the end of the function, with global callback calls
  o << "        }"                                                                  << endl;
  o << "    } /* while */"                                                          << endl 
                                                                                    << endl;
  o << "  if (errflg)"                                                              << endl;
  o << "    usage (" << get_exit_value () << ", argv[0]);"                          << endl;

  if (!get_global_callback ().empty ())
    {
      o                                                                             << endl;
      o << "  if (!" << get_global_callback () << " (my_args))"                     << endl;
      o << "    usage (" << get_exit_value () << ", argv[0]);"                      << endl;
    }

  if (!get_no_struct ())
    {
      o                                                                             << endl;
      o << "  if (optind >= argc)"                                                  << endl;
      o << "    my_args->optind = 0;"                                               << endl;
      o << "  else"                                                                 << endl;
      o << "    my_args->optind = optind;"                                          << endl;
    }

  o << "}"                                                                          << endl
                                                                                    << endl;

  // dump the usage () function
  output_usage (o);
  
  // dump the get_long_options () function
  if (get_export_long_options ())
    {
      o                                                                             << endl;
      output_get_long_options (o);
    }
  
  if (!o)
    throw EH ("can't write to file: " + path);

  // close the C file
  o.close ();  
  if (!o)
    throw EH ("can't close file: " + path);

  // tell the user what we're doing
  if (!get_quiet ())
    cout << "done" << endl;  
}

/** Output the C source file containing the default implementation of the callback functions */
void C_Clparam_list::output_callbacks (const string filename_without_extension) const throw (EH)
{
  list<Clparam> params = get_params ();
  list<Clparam>::iterator iter;

  // first figure out if we have to do anything at all - no callbacks = no
  // callback file
  if (have_callbacks ())
    {
      // make the callback filename
      const string filename = filename_without_extension + "_cb.c";
      const string path = full_path (get_directory (), filename);
      list<string> param_callback_list;

      // tell the user what we're doing
      if (!get_quiet ())
	cout << "creating " << path << "...";
      
      // open the callback file
      ofstream o;
      o.open (path.c_str ());
      if (!o)
        throw EH ("can't open file: " + path);

      // do the main comments
      output_comments_header (o, filename, "Callback routines for command line parser");

      // use the header filename for the includes
      o << "#include <stdio.h>"                                         << endl;
      o << "#include \"" << filename_without_extension + ".h" << "\""   << endl 
                                                                        << endl;

      // do the global callbacks first
      if (!get_global_callback ().empty ())
        {
          output_function_comment (o, get_global_callback (), "User defined global callback.");
          o                                                             << endl;
          o << "int " << get_global_callback () << " (struct arg_t *a)" << endl;
          o << "{"                                                      << endl;
          o << "  return 1;"                                            << endl;
          o << "}"                                                      << endl 
                                                                        << endl;
        }

      // do the parameter callbacks
      for (iter = params.begin (); iter != params.end (); iter++)
	if (!iter->get_callback ().empty ())
          if (find (param_callback_list.begin (), param_callback_list.end (), 
            iter->get_callback ()) == param_callback_list.end ())
            {
	      output_function_comment (o, iter->get_callback (), 
                "User defined parameter callback.");
	      o                                                         << endl;
	      o << "int " << iter->get_callback () << " ("
                << type2str (iter->get_type ()) << " var)"              << endl;
	      o << "{"                                                  << endl;
	      o << "  return 1;"                                        << endl;
	      o << "}"                                                  << endl 
                                                                        << endl;

              param_callback_list.push_back (iter->get_callback ());
            }

      if (!o)
        throw EH ("can't write to file: " + path);

      // close the callback file
      o.close ();
      if (!o)
        throw EH ("can't close file: " + path);

      // tell the user what we're doing
      if (!get_quiet ())
	cout << "done" << endl;
    }
}
