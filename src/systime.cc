/** @file systime.cc
\brief Methods for the system time class */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/time.h>
#include <unistd.h>
#include "systime.h"

#define TIMESTR_LEN 64

//---------------------------------------------------------------------------
//
// Systime::Systime ()
//
// Constructor method.  Call the function that does the work.
//
//---------------------------------------------------------------------------

Systime::Systime ()
{
  refresh ();
}

//---------------------------------------------------------------------------
//
// Systime::operator=()
//
// Assignment operator overload
//
//---------------------------------------------------------------------------

Systime& Systime::operator=(const Systime& rhs)
{
  // guard against self-assignment
  if (this != &rhs)
    {
      _tv = rhs._tv;
      _tp = localtime ((time_t *) &_tv.tv_sec);
    }

  return *this;
}

//---------------------------------------------------------------------------
//
// Systime::operator-()
//
// Assignment operator overload
//
//---------------------------------------------------------------------------

Systime Systime::operator-(const Systime st)
{
  Systime temp;

  temp._tv.tv_sec = _tv.tv_sec - st._tv.tv_sec;
  temp._tv.tv_usec = _tv.tv_usec - st._tv.tv_usec;
  temp._tp = localtime ((time_t *) &_tv.tv_sec);

  return temp;
}

//---------------------------------------------------------------------------
//
// Systime::refresh ()
//
// Update the time.  Read the clock again.
//
//---------------------------------------------------------------------------

void Systime::refresh () throw (EH_system)
{  
  // Get the time from the system clock
  if (gettimeofday (&_tv, (struct timezone *) 0) == -1)
    {
      EH_system e ("can't get system time");
      throw e;
    }
  
  // Convert them, assign to our structures
  _tp = localtime ((time_t *) &_tv.tv_sec);
}

//---------------------------------------------------------------------------
//
// Systime::hms ()
//
// Return HH:MM:SS
//
//---------------------------------------------------------------------------

string Systime::hms ()
{
  char timestr[TIMESTR_LEN];

  strftime (timestr, TIMESTR_LEN, "%T", _tp);
  string s = timestr;

  return s; 
}

//---------------------------------------------------------------------------
//
// Systime::month ()
//
// Return the month in a string
//
//---------------------------------------------------------------------------

string Systime::month ()
{
  char timestr[TIMESTR_LEN];

  strftime (timestr, TIMESTR_LEN, "%B", _tp);
  string s = timestr;

  return s; 
}

//---------------------------------------------------------------------------
//
// Systime::day ()
//
// Return the day (e.g., "monday") in a string
//
//---------------------------------------------------------------------------

string Systime::day ()
{
  char timestr[TIMESTR_LEN];

  strftime (timestr, TIMESTR_LEN, "%A", _tp);
  string s = timestr;

  return s; 
}

//---------------------------------------------------------------------------
//
// Systime::unix_ctime ()
//
// Return time in UNIX ctime () format
//
//---------------------------------------------------------------------------

string Systime::unix_ctime ()
{
  time_t tmp;
  string s;

  // Convert the time
  tmp = mktime (_tp);
  s = ctime (&tmp);

  return s; 
}
