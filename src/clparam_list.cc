/** @file clparam_list.cc
\brief Methods for the command line parameter list class */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <fstream>
#include <sstream>
#include <ctype.h>
#include <algorithm>
#include "clparam_list.h"
#include "sysinfo.h"
#include "systime.h"
#include "userinfo.h"
#include "genparse_util.h"

/** Constructor */
Clparam_list::Clparam_list ()
{
  _cppext               = "cc";
  _exit_value           = "EXIT_FAILURE";
  _quiet                = false;
  _longmembers          = false;
  _internationalization = false;
  _static_headers       = false;
  _export_long_options  = false;
  _use_gnulib           = false;
  _many_prints          = false;
  _no_struct            = false;
  _break_lines          = - 1;
}

/** Copy constructor */
Clparam_list& Clparam_list::operator=(const Clparam_list &rhs)
{
  // guard against self-assignment
  if (this != &rhs)
    {
      _params               = rhs._params;
      _global_callback      = rhs._global_callback;
      _include_list         = rhs._include_list;
      _mandatory_list       = rhs._mandatory_list;
      _parsefunc            = rhs._parsefunc;
      _directory            = rhs._directory;
      _cppext               = rhs._cppext;
      _exit_value           = rhs._exit_value;
      _quiet                = rhs._quiet;
      _longmembers          = rhs._longmembers;
      _internationalization = rhs._internationalization;
      _static_headers       = rhs._static_headers;
      _break_lines          = rhs._break_lines;
      _export_long_options  = rhs._export_long_options;
      _use_gnulib           = rhs._use_gnulib;
      _many_prints          = rhs._many_prints;
      _usage_list           = rhs._usage_list;
    }
  
  return *this;
}

/** Return true if at least 1 parameter has a callback function. */
bool Clparam_list::parameters_have_callbacks (void) const
{
  return (find_if (_params.begin (), _params.end (), mem_fun_ref (&Clparam::has_callback)) 
    != _params.end ());
}

/** Return true if there are any callback functions (global or related to a parameter). */
bool Clparam_list::have_callbacks (void) const
{
  return (parameters_have_callbacks () || !_global_callback.empty ());
}

/** Add a parameter to the list, checking for duplicates. */
void Clparam_list::add (const Clparam &p) throw (EH)
{
  if (this->contains (p))
    {
      throw EH ("duplicate parameter");
    }

  _params.push_back (p);
}

/** Does the list contain this parameter? Yes if either the short or long representation matches. */
bool Clparam_list::contains (const Clparam &p) const
{
  list<Clparam>::const_iterator iter;
  unsigned int i;
  
  for (iter = _params.begin (); iter != _params.end (); iter++)
    {
      if (!iter->get_long_only () && (iter->get_short_rep (0) != 0))
        for (i = 0; i < p.get_num_short_reps (); i++)
          if (iter->contains (p.get_short_rep (i)))
            return true;

      if ((iter->get_long_rep ().length () > 0) && (iter->get_long_rep () == p.get_long_rep ()))
        return true;
    }
  return false;
}

/** Does the list contain this parameter? Yes if short representation matches. */
bool Clparam_list::contains (const char c) const
{
  list<Clparam>::const_iterator iter;
  
  for (iter = _params.begin (); iter != _params.end (); iter++)
    if (iter->contains (c))
      return true;
  return false;
}

/** Does the list contain this parameter? Yes if short representation matches. */
bool Clparam_list::contains (const string s) const
{
  list<Clparam>::const_iterator iter;
  
  for (iter = _params.begin (); iter != _params.end (); iter++)
    if (iter->get_long_rep () == s)
      return true;
  return false;
}

/** Dump contents to a log file */
void Clparam_list::logdump (Logfile& l) const
{
  // preface
  l.write ("dumping parameter list...");

  // do the global stuff here
  list<string>::const_iterator iter;
  for (iter = _include_list.begin (); iter != _include_list.end (); iter++)
    l.write ("  include file: " + *iter);
  for (iter = _mandatory_list.begin (); iter != _mandatory_list.end (); iter++)
    l.write ("  mandatory: " + *iter);
  if (!get_global_callback ().empty ())
    l.write ("  global_callback: " + get_global_callback ());

  // now do the parameters
  list<Clparam>::const_iterator iter2;
  for (iter2 = _params.begin (); iter2 != _params.end (); iter2++)
    iter2->logdump (l);

  // afterword
  l.write ("finished dumping parameter list");
}

/** Given a parameter type, return that type in a string */
const string Clparam_list::type2str (param_type t) const
{
  switch (t)
    {
    case flag_type:
      return boolean_type_str ();
      break;
    case int_type:
      return "int";
      break;
    case long_type:
      return "long int";
      break;
    case ulong_type:
      return "unsigned long int";
      break;
    case intmax_type:
      return "intmax_t";
      break;
    case uintmax_type:
      return "uintmax_t";
      break;
    case float_type:
      return "float";
      break;
    case double_type:
      return "double";
      break;
    case char_type:
      return "char";
      break;
    case string_type:
      return string_type_str ();
    default:
      throw EH ("unknown parameter type");
    }
}

/** Returns true if all parameters have only parameters in the long form. */
bool Clparam_list::long_only (void) const
{
  list<Clparam>::const_iterator iter;
  for (iter = _params.begin (); iter != _params.end (); iter++)
    if (!iter->get_long_only ())
      return false;

  return true;
}

/** Returns a short form of the options in the form [ -abc ] */
string Clparam_list::get_options_string (void) const
{
  string rslt;

  rslt = "[ -";
  for (list<Clparam>::const_iterator iter = _params.begin (); iter != _params.end (); iter++)
    if ((!iter->get_long_only ()) && (!iter->get_do_not_document ()))
      rslt += iter->get_short_reps ();
  rslt += " ]";

  return rslt;
}

/** Returns a command line glossary. Every element in the result array is 1 line. */
list<string> Clparam_list::get_glossary (void) const
{
  list<string> lines;
  list<Cldesc> param_desc;
  string line;
  list<Cldesc>::const_iterator param_iter;
  unsigned int i;
  unsigned int indentation = 10;

  if (get_many_prints ())
    lines.push_back("__NEW_PRINT__");

  list<Clparam>::const_iterator iter;
  for (iter = _params.begin (); iter != _params.end (); iter++)
    {
      if (!iter->get_do_not_document ())
        {
          if (!iter->get_comment ().empty ())
            lines.push_back("__COMMENT__(" + iter->get_comment () + ')');

          line = "  ";
          if (!iter->get_long_only ()) 
            {
              for (i = 0; i < iter->get_num_short_reps (); i++)
                line += string (" [ -") + iter->get_short_rep (i) + " ]";
            }

          if (!iter->get_long_rep ().empty ())
            {
              line += " [ --" + iter->get_long_rep ();
              if (!iter->get_opt_name ().empty ())
                {
                  if (iter->get_opt_name_in_square_braces ())
                    line += '[';
                  line += '=' + iter->get_opt_name ();
                  if (iter->get_opt_name_in_square_braces ())
                    line += ']';
                }
              line += " ]";
            }


          // Print the type
          line += " (type=";
          switch (iter->get_type ())
            {
            case flag_type:
              line += "FLAG";
              break;
            case int_type:
              line += "INTEGER";
              break;
            case long_type:
              line += "LONG";
              break;
            case ulong_type:
              line += "UNSIGNED LONG";
              break;
            case intmax_type:
              line += "INTMAX";
              break;
            case uintmax_type:
              line += "UINTMAX";
              break;
            case float_type:
              line += "REAL";
              break;
            case double_type:
              line += "DOUBLE";
              break;
            case char_type:
              line += "CHARACTER";
              break;
            case string_type:
              line += "STRING";
              break;
            default:
              throw EH ("unknown parameter type");
            }
          if (!iter->get_high_range ().empty () || !iter->get_low_range ().empty ()
            || !iter->get_default_value ().empty ())
            line += ",";

          // Print the range
          if (!iter->get_low_range ().empty ())
            {
              line += " range=" + iter->get_low_range () + "...";
              if (!iter->get_high_range ().empty ())
                line += iter->get_high_range ();
              line += ",";	    
            }
          else
            {
              if (!iter->get_high_range ().empty ())
                line += "range=..." + iter->get_high_range () + ",";
            }

          // Print default values, but not for flags
          if (!iter->get_default_value ().empty () && iter->get_type () != flag_type)
            line += " default=" + iter->get_default_value ();

          line += string (")");
          lines.push_back (line);

          param_desc = iter->get_description (indentation, 0, get_break_lines ());
          for (param_iter = param_desc.begin (); param_iter != param_desc.end (); param_iter++)
            if (param_iter->get_start_1st_col () == true)
              lines.push_back (param_iter->get_description ());
            else
              lines.push_back (string (indentation, ' ') + param_iter->get_description ());

          if (get_many_prints ())
            lines.back() += "__NEW_PRINT__";
      }
    }

  return lines;
}

/** Returns a GNU style command line glossary. Every element in the result array is 1 line. */
list<string> Clparam_list::get_glossary_gnu (unsigned int indentation) const
{
  list<string> lines;
  list<Cldesc> param_desc;
  string line;
  unsigned int i;
  list<Cldesc>::const_iterator param_iter;

  if (get_many_prints ())
    lines.push_back("__NEW_PRINT__");

  list<Clparam>::const_iterator iter;
  for (iter = _params.begin (); iter != _params.end (); iter++)
    {
      if (!iter->get_do_not_document ())
        {
          if (!iter->get_comment ().empty ())
            lines.push_back("__COMMENT__(" + iter->get_comment () + ')');

          line = " ";
          if (!iter->get_long_only ())
            /* this parameter only has a long representation */
            for (i = 0; i < iter->get_num_short_reps ();i++)
              line += string (" -") + iter->get_short_rep (i);
          else if (!long_only ())
            /* at least one parameter in the list has a short representation */
            line += string (4, ' ');

          if (!iter->get_long_only () && !iter->get_long_rep ().empty ())
            line += ',';

          if (!iter->get_long_rep ().empty ())
            line += " --" + iter->get_long_rep ();

          if (!iter->get_opt_name ().empty ())
            {
              if (iter->get_opt_name_in_square_braces ())
                line += '[';
              line += '=' + iter->get_opt_name ();
              if (iter->get_opt_name_in_square_braces ())
                line += ']';
            }

          /* Append parameter description */
          param_desc = iter->get_description (indentation, 2, get_break_lines ());
          param_iter = param_desc.begin ();
          /* print first description string in same line */
          if (param_iter != param_desc.end ())
            {
              string::size_type length = line.length ();
              if (length < indentation)
                line += string (indentation - length, ' ');
              else if (_break_lines >= 0)
                {
                  lines.push_back (line);
                  line = string (indentation, ' ');
                }

              lines.push_back (line + param_iter->get_description ());
              param_iter++;
            }
          else
            {
              lines.push_back (line);
            }
          while (param_iter != param_desc.end ())
            {
              if (param_iter->get_start_1st_col () == true)
                lines.push_back (param_iter->get_description ());
              else
                lines.push_back (string (indentation, ' ') + param_iter->get_description ());
              param_iter++;
            }

          if (get_many_prints ())
            lines.back() += "__NEW_PRINT__";
        }
    }

  return lines;
}

/** Returns a string with all mandatory arguments separated by spaces */
string Clparam_list::get_mandatories_string (void) const
{
  string rslt;
  unsigned int i;
  list<string>::const_iterator str_iter;

  str_iter = _mandatory_list.begin ();
  i = 0;
  while (i < _mandatory_list.size ())
    {
      rslt += *str_iter;
      if (i + 1 < _mandatory_list.size ())
        rslt += ' ';
      str_iter++;
      i++;
    }
  return rslt;
}

/** Helper function for parsing one line in the section for the usage () 
function of the genparse file for a macro. 
\return Index of the first occurance of macro with a valid argument or string::npos
        if none could be found. */
string::size_type Clparam_list::parse_macro (
  /** Another return value: Index of the first character of the macro argument. 
      Only valid if the return value of this function is != string::npos. */
  string::size_type &arg_start_pos,
  /** Another return value: Index of the last character of the macro argument. 
      Only valid if the return value of this function is != string::npos. */
  string::size_type &arg_end_pos, 
  /** Another return value: Index of the closing bracket of the macro argument. 
      Only valid if the return value of this function is != string::npos. */
  string::size_type &closing_bracket_pos, 
  /** the macro to parse for */
  const string &macro,
  /** The line which must be parsed for macros. */
  const string &line) const
{
  string::size_type macro_pos;
  string::size_type arg_opening_bracket_pos;

  macro_pos = line.find (macro);
  if (macro_pos != string::npos)
    {
      arg_start_pos = macro_pos + macro.length ();
      while (isspace (line[arg_start_pos])) arg_start_pos++;
      if (line[arg_start_pos] != '(')
        return string::npos;
      arg_start_pos++;
      closing_bracket_pos     = line.find  (')', arg_start_pos);
      arg_opening_bracket_pos = line.rfind ('(', closing_bracket_pos);
      while ((arg_opening_bracket_pos != string::npos) 
          && (arg_opening_bracket_pos > arg_start_pos))
        {
          closing_bracket_pos     = line.find  (')', closing_bracket_pos + 1);
          arg_opening_bracket_pos = line.rfind ('(', arg_opening_bracket_pos - 1);
        }
      if (closing_bracket_pos == string::npos)
        return string::npos;
      arg_end_pos = closing_bracket_pos - 1;
      while (isspace (line[arg_end_pos])) arg_end_pos--;
    }
  return macro_pos;
}

/** return string explaining usage */
list<string> Clparam_list::get_usage (void) const
{
  list<string> rval;
  list<string> usage_lines;
  list<string>::const_iterator iter;
  ostringstream ostr;
  unsigned int line_length = 0;

  for (iter = _usage_list.begin (); iter != _usage_list.end (); iter++)
    {
      string::size_type pos;
      string line = *iter;

      do
        {
          pos = line.find (string ("__OPTIONS_SHORT__"));
          if (pos != string::npos)
            line = line.replace (pos, 17, get_options_string ());
        }
      while (pos != string::npos);

      do
        {
          pos = line.find (string ("__MANDATORIES__"));
          if (pos != string::npos)
            line = line.replace (pos, 15, get_mandatories_string ());
        }
      while (pos != string::npos);

      pos = line.find (string ("__GLOSSARY__"));
      if (pos != string::npos)
        {
          usage_lines = get_glossary ();
        }
      else
        {
          pos = line.find (string ("__GLOSSARY_GNU__"));
          if (pos != string::npos)
            {
              line.erase (0, pos + 16);  /* Remove __GLOSSARY_GNU__ and any characters before */

              istringstream linestr (line);
              char opening_bracket, closing_bracket;
              unsigned int indentation;

              linestr >> opening_bracket >> indentation >> closing_bracket;
              if ((opening_bracket == '(') && (closing_bracket == ')'))
                usage_lines = get_glossary_gnu (indentation);
              else
                usage_lines = get_glossary_gnu ();
            }
        }

      if (pos != string::npos)
        {
          if (line_length > 0)
            {
              /* print stream buffer and clear it */
              rval.push_back (ostr.str ());
              ostr.str ("");
              line_length = 0;
            }

          /* print glossary */
          list<string>::const_iterator usage_iter;
          for (usage_iter = usage_lines.begin (); usage_iter != usage_lines.end (); usage_iter++)
            rval.push_back (*usage_iter);
        }
      else if (get_break_lines () < 0)
        {
          rval.push_back (line);
        }
      else
        {
          do
            {
              string extra_line;

              pos = line.find (string ("__NL__"));
              if (pos != string::npos)
                {
                  extra_line = string (line, pos + 6, string::npos);
                  line = string (line, 0, pos);
                }

              istringstream istr (line);
              string word;

              while (istr >> word)
                {
                  if (line_length > 0)
                    {
                      if (line_length + word.length () + 1 > (unsigned int)get_break_lines ())
                        {
                          rval.push_back (ostr.str ());
                          ostr.str ("");
                          line_length = 0;
                        }
                      else
                        {
                          ostr << ' ';
                          line_length += 1;
                        }
                    }

                  ostr << word;
                  line_length += word.length ();
                }

              if (pos != string::npos)
                {
                  rval.push_back (ostr.str ());
                  ostr.str ("");
                  line_length = 0;
                  line = extra_line;
                }
            }
          while (pos != string::npos);
        }
    }

  if (!ostr.str () .empty ())
    rval.push_back (ostr.str ());

  return rval;
}

/** Output the comments at the beginning of a file. */
void Clparam_list::output_comments_header (ofstream& o, const string &filename, 
  const string &purpose) const
{
  // opening comments with filename
  o << "/******************************************************************************" << endl;
  o << "**"                                                                              << endl;
  o << "** " << filename                                                                 << endl;
  o << "**"                                                                              << endl;

  if (get_static_headers () == false)
    {
      // timestamp the file
      Systime t;
      o << "** " << t.unix_ctime ();

      // add the system information
      Sysinfo i;
      o << "** " << i.format ()                                                          << endl;

      // add the user information
      Userinfo u;
      o << "** " << u.format ()                                                          << endl;
      o << "**"                                                                          << endl;
    }
  
  // add the purpose - as if there is one :-)
  o << "** " << purpose                                                                  << endl;
  o << "**"                                                                              << endl;

  // do a little advertising for the program...
  o << "** Automatically created by " << PACKAGE << " v" << VERSION                      << endl;
  o << "**"                                                                              << endl;
  o << "** See http://genparse.sourceforge.net for details and updates"                  << endl;
  o << "**"                                                                              << endl;

  // that's it - finish up
  o << "******************************************************************************/" << endl;
  o                                                                                      << endl;  
}

/** Output a function comment banner. */
void Clparam_list::output_function_comment (ofstream& o, const string &function_name, 
  const string &comment) const
{
  o << "/*----------------------------------------------------------------------------" << endl;
  o << "**"                                                                             << endl;
  o << "** " << function_name << " ()"                                                  << endl;
  o << "**"                                                                             << endl;
  o << "** " << comment                                                                 << endl;
  o << "**"                                                                             << endl;
  o << "**--------------------------------------------------------------------------*/" << endl;  
}

/** Output the general include files that were specified in the .gp file. */
void Clparam_list::output_includes (ofstream& o) const
{
  // loop through the list
  list<string>::const_iterator iter;
  for (iter = _include_list.begin (); iter != _include_list.end (); iter++)
    o << "#include " << *iter << endl;
  o << endl;
}

/** Get parameter list */
const list<Clparam> &Clparam_list::get_params (void) const
{
  return (const list<Clparam> &)_params;
}

string const Clparam_list::get_parameter_name (const Clparam &param) const
{
  string rval;

  if (param.get_long_only () || (get_longmembers () && !param.get_long_rep ().empty ()))
    rval = param.get_long_rep ();
  else
    rval = string (1, param.get_short_rep (0));

  return (genparse_util::str2C (rval));
}

/** Call this function once after parsing. */
void Clparam_list::post_parser_adjust (void)
{
  list<Clparam>::iterator iter;

  for (iter = _params.begin (); iter != _params.end (); iter++)
    {
      if (iter->get_type () == flag_type)
        {
          iter->set_short_has_arg (no_argument);
          iter->set_long_has_arg ( no_argument);
        }
      else
        {
          if (iter->get_short_has_arg () == argument_undefined)
            iter->set_short_has_arg (required_argument);

          if (iter->get_long_has_arg () == argument_undefined)
            iter->set_long_has_arg (required_argument);
        }

      iter->normalize_code_lines_indentation ();
    }
}

/** Template for generating the parsing engine.

Language specific subfunctions must be implemented in derived classes. 
*/
void Clparam_list::output (const string filename_without_extension) const
{
  check_parameters ();

  output_interface (     filename_without_extension);
  output_implementation (filename_without_extension);
  output_callbacks (     filename_without_extension);
}
