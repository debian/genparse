/** @file clparam_list_factory.cc
\brief Methods for the command line parameter list factory class */
/*
 * Copyright (C) 2006 - 2016
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "clparam_list_factory.h"
#include "c_clparam_list.h"
#include "cpp_clparam_list.h"
#include "java_clparam_list.h"

/** Creates the Clparam_list sub class using new which is able to generate language code

@param language must be "c" or "C" in order to generate C code, "c++" or "cpp" or "cc" or "cxx"
in order to generate C++ code and "java" or "Java" in order to generate Java code.
*/
Clparam_list *Clparam_list_factory::Create_Clparam_list (const string &language) const throw (EH)
{
  if (language == "c" || language == "C")
    return new C_Clparam_list;
  else if (language == "c++" || language == "cpp" || language == "cc" || language == "cxx")
    return new CPP_Clparam_list;
  else if (language == "java" || language == "Java")
    return new Java_Clparam_list;
  else
    throw EH ("invalid parameter " + language);
}
