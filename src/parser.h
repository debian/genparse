/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    SLASH = 258,
    RANGE_SPEC1 = 259,
    RANGE_SPEC2 = 260,
    FUNC_SPEC = 261,
    NONE = 262,
    OPEN_BRACE = 263,
    CLOSE_BRACE = 264,
    OPEN_SQUARE_BRACE = 265,
    CLOSE_SQUARE_BRACE = 266,
    EQUAL = 267,
    INCLUDE = 268,
    LT = 269,
    GT = 270,
    MANDATORY = 271,
    EXIT_VALUE = 272,
    BREAK_LINES = 273,
    DOUBLEQUOTE = 274,
    ASTERISK = 275,
    EXCLAMATIONMARK = 276,
    STORE_LONGINDEX = 277,
    EXPORT_LONG_OPTIONS = 278,
    GP_INCLUDE = 279,
    ERR_MSG = 280,
    COMMENT = 281,
    DO_NOT_DOCUMENT = 282,
    OPEN_ROUND_BRACE = 283,
    CLOSE_ROUND_BRACE = 284,
    COLON = 285,
    ADD_FLAG = 286,
    NO_STRUCT = 287,
    CODE_END = 288,
    QUOTED = 289,
    FIRST_QUOTED = 290,
    C_VAR = 291,
    ALNUM = 292,
    FILENAME = 293,
    CHAR_VAL = 294,
    FIRST_STR = 295,
    USAGE_LINE = 296,
    CODE_LINE = 297,
    OPT_NAME = 298,
    COMMENT_STR = 299,
    FUNCTION = 300,
    FLAG = 301,
    INT = 302,
    LONG = 303,
    ULONG = 304,
    INTMAX = 305,
    UINTMAX = 306,
    FLOAT = 307,
    DOUBLE = 308,
    CHAR = 309,
    STRING = 310
  };
#endif
/* Tokens.  */
#define SLASH 258
#define RANGE_SPEC1 259
#define RANGE_SPEC2 260
#define FUNC_SPEC 261
#define NONE 262
#define OPEN_BRACE 263
#define CLOSE_BRACE 264
#define OPEN_SQUARE_BRACE 265
#define CLOSE_SQUARE_BRACE 266
#define EQUAL 267
#define INCLUDE 268
#define LT 269
#define GT 270
#define MANDATORY 271
#define EXIT_VALUE 272
#define BREAK_LINES 273
#define DOUBLEQUOTE 274
#define ASTERISK 275
#define EXCLAMATIONMARK 276
#define STORE_LONGINDEX 277
#define EXPORT_LONG_OPTIONS 278
#define GP_INCLUDE 279
#define ERR_MSG 280
#define COMMENT 281
#define DO_NOT_DOCUMENT 282
#define OPEN_ROUND_BRACE 283
#define CLOSE_ROUND_BRACE 284
#define COLON 285
#define ADD_FLAG 286
#define NO_STRUCT 287
#define CODE_END 288
#define QUOTED 289
#define FIRST_QUOTED 290
#define C_VAR 291
#define ALNUM 292
#define FILENAME 293
#define CHAR_VAL 294
#define FIRST_STR 295
#define USAGE_LINE 296
#define CODE_LINE 297
#define OPT_NAME 298
#define COMMENT_STR 299
#define FUNCTION 300
#define FLAG 301
#define INT 302
#define LONG 303
#define ULONG 304
#define INTMAX 305
#define UINTMAX 306
#define FLOAT 307
#define DOUBLE 308
#define CHAR 309
#define STRING 310

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 57 "parser.yy" /* yacc.c:1909  */

  char    *str;  /* string */
  int     i;
  param_type p;

#line 170 "parser.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_H_INCLUDED  */
