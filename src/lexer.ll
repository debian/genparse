/*
 * $Id: lexer.ll,v 1.24 2010/03/14 14:51:17 mgeng Exp $
 *
 * Copyright (C) 2000, 2006, 2007, 2008
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

%option yylineno
%option noyywrap
%{
/*
 * Definitions section
 */

#include <stdio.h>
#include <string.h>
#include "clparam.h"
#include "parser.h"
#include "genparse.h"

/*
 * Help with debugging
 */
#ifdef DEBUG
#define return_token(x) { fprintf (stderr,"LEX: Token %s: '%s'\n",#x,yytext); return x; }
#else
#define return_token(x) { return (x); }
#endif

#define MAX_INCLUDE_DEPTH 10
YY_BUFFER_STATE include_stack[MAX_INCLUDE_DEPTH];
int include_stack_ptr = 0;
int old_start_condition;
int brace_level;
%}

/*
 * More definitions
 */

range1                  ".."
range2                  "..."
char                    [A-Za-z]
first_str               ^[A-Za-z0-9]+
digit                   [0-9]
alnum                   [\-a-zA-Z_0-9]+(\.[0-9]+)?
c_var                   [a-zA-Z_][a-zA-Z_0-9]*
file                    [a-zA-Z_][a-zA-Z_0-9/\-]*[\.a-zA-Z_0-9/\-]*
opt_name                [a-zA-Z_0-9|{}]+
whitespace              [ \t\n]
doublequote             [\"]
any                     .
empty_line              ^\n
any_line                .*
code_line               [^()\n]*
comment_str             [^()\n]*
func_spec               "()"
space                   [ \t]

%x usage include include_doublequote gp_include comment __code__

%s before_type after_type

%%

{whitespace}*           ; /* do nothing on whitespace */

"/*"                    { /* Ignore C comments */
                          int c1 = 0;
                          int c2 = yyinput ();
            
                          for (;;)
                            {
                              if (c2 == EOF) break;
                              if (c1 == '*' && c2 == '/') break;
                              c1 = c2;
                              c2 = yyinput ();
                            }
                        }

"NONE"                  {
                          BEGIN (before_type);
                          return_token (NONE);
                        }
{first_str}             {
                          BEGIN (before_type);
                          yylval.str = strdup (yytext);
                          return_token (FIRST_STR);
                        }
"{"                     return_token (OPEN_BRACE);
"}"                     return_token (CLOSE_BRACE);
"["                     return_token (OPEN_SQUARE_BRACE);
"]"                     return_token (CLOSE_SQUARE_BRACE);
"("                     return_token (OPEN_ROUND_BRACE);
")"                     return_token (CLOSE_ROUND_BRACE);
"/"                     return_token (SLASH);
"int"                   {
                          BEGIN (after_type);
                          return_token (INT);
                        }
"long"                  {
                          BEGIN (after_type);
                          return_token (LONG);
                        }
"ulong"                 {
                          BEGIN (after_type);
                          return_token (ULONG);
                        }
"intmax"                {
                          BEGIN (after_type);
                          return_token (INTMAX);
                        }
"uintmax"               {
                          BEGIN (after_type);
                          return_token (UINTMAX);
                        }
"float"                 {
                          BEGIN (after_type);
                          return_token (FLOAT);
                        }
"double"                {
                          BEGIN (after_type);
                          return_token (DOUBLE);
                        }
"char"                  {
                          BEGIN (after_type);
                          return_token (CHAR);
                        }
"string"                {
                          BEGIN (after_type);
                          return_token (STRING);
                        }
"flag"                  {
                          BEGIN (after_type);
                          return_token (FLAG);
                        }
"#include"              {
                          old_start_condition = YY_START;
                          BEGIN (include);
                          return_token (INCLUDE);
                        }
"<"                     return_token (LT);
">"                     return_token (GT);
{doublequote}           return_token (DOUBLEQUOTE);
<include>"<"            return_token (LT);
<include>">"            {
                          BEGIN (old_start_condition);
                          return_token (GT);
                        }
<include>{doublequote}  {
                          BEGIN (include_doublequote);
                          return_token (DOUBLEQUOTE);
                        }
<include_doublequote>{doublequote}  {
                          BEGIN (old_start_condition);
                          return_token (DOUBLEQUOTE);
                        }
"="                     return_token (EQUAL);
"*"                     return_token (ASTERISK);
"!"                     return_token (EXCLAMATIONMARK);
","                     return_token (COLON);
"#mandatory"            return_token (MANDATORY);
"#exit_value"           return_token (EXIT_VALUE);
"#break_lines"          return_token (BREAK_LINES);
"#export_long_options"  return_token (EXPORT_LONG_OPTIONS);
"#no_struct"            return_token (NO_STRUCT);
"__ERR_MSG__"           return_token (ERR_MSG);
"__DO_NOT_DOCUMENT__"   return_token (DO_NOT_DOCUMENT);
"__ADD_FLAG__"          return_token (ADD_FLAG);
"__COMMENT__"           {
                          old_start_condition = YY_START;
                          BEGIN (comment);
                          return_token (COMMENT);
                        }
<comment>"("            return_token (OPEN_ROUND_BRACE);
<comment>")"            {
                          BEGIN (old_start_condition);
                          return_token (CLOSE_ROUND_BRACE);
                        }
<comment>{comment_str}  {
                          yylval.str = strdup (yytext);
                          return_token (COMMENT_STR);
                        }
<before_type,after_type>"__STORE_LONGINDEX__" return_token (STORE_LONGINDEX);
"#usage_begin"          BEGIN (usage);
<usage>"#usage_end"     BEGIN (0);

<after_type>"__CODE__"  {
                          old_start_condition = YY_START;
                          brace_level = 0;
                          BEGIN (__code__);
                        }

<__code__>"("           {
                          brace_level++;
                          if (brace_level > 1)
                            return (OPEN_ROUND_BRACE);
                        }
<__code__>")"           {
                          brace_level--;
                          if (brace_level < 0)
                            throw EH ("unexpected ) at " + string(yytext));
                          if (brace_level > 0)
                            {
                              return (CLOSE_ROUND_BRACE);
                            }
                          else
                            {
                              BEGIN (old_start_condition);
                              return (CODE_END);
                            }
                        }

<__code__>{code_line}   {
                          if (brace_level <= 0)
                            throw EH ("missing opening ( at " + string(yytext));
                          yylval.str = strdup (yytext);
                          return_token (CODE_LINE);
                        }

<__code__>\n            {
                          return_token (CODE_END);
                        }

{c_var}                 {
                          yylval.str = strdup (yytext);
                          return_token (C_VAR);
                        }

{func_spec}             return_token (FUNC_SPEC);

{c_var}/{space}*{func_spec} {
                          yylval.str = strdup (yytext);
                          return_token (FUNCTION);
                        }

<include,include_doublequote>{file} {
                          yylval.str = strdup (yytext);
                          return_token (FILENAME);
                        }

<before_type>{opt_name}	{
                          yylval.str = strdup (yytext);
                          return_token (OPT_NAME);
                        }

{alnum}                 {
                          yylval.str = strdup (yytext);
                          return_token (ALNUM);
                        }

<after_type>{range1}    return_token (RANGE_SPEC1);
<after_type>{range2}    return_token (RANGE_SPEC2);
^"\""[^\"]*"\""         {
                          yylval.str = strdup (yytext+1); 
                          yylval.str[strlen (yylval.str)-1]='\0';
                          return_token (FIRST_QUOTED);
                        }
"\""[^\"]*"\""          {
                          yylval.str = strdup (yytext+1); 
                          yylval.str[strlen (yylval.str)-1]='\0';
                          return_token (QUOTED);
                        }
"\'"[^\'\n]*"\'"        { 
                          yylval.str = strdup (yytext); 
                          return_token (CHAR_VAL);
                        }

{any}                   { /* error */
                          fprintf (stderr,"parser error in line %d ('%s') \n",yylineno,yytext);
                          exit (1);
                        }

<usage>{any_line}       {
                          yylval.str = strdup (yytext);
                          return_token (USAGE_LINE);
                        }

<usage>{empty_line}     {
                          yylval.str = strdup ("");
                          return_token (USAGE_LINE);
                        }

<*>.|\n                 /* Don't print unmatched characters in start conditions */


"#gp_include"           {
                          old_start_condition = YY_START;
                          BEGIN (gp_include);
                          return_token (GP_INCLUDE);
                        }
<gp_include>{whitespace}*;  /* do nothing on whitespace */
<gp_include>{file}      {   /* include file name */
                          if (include_stack_ptr >= MAX_INCLUDE_DEPTH)
                            throw EH ("gp_includes nested too deeply");

                          include_stack[include_stack_ptr++] = YY_CURRENT_BUFFER;

                          yyin = fopen (yytext, "r");
                          if (!yyin)
                            throw EH ("failed to open gp_include file " + string(yytext));

                          yy_switch_to_buffer (yy_create_buffer (yyin, YY_BUF_SIZE));

                          BEGIN (old_start_condition);

                          yylval.str = strdup (yytext);
                          return_token (FILENAME);
                        }

<<EOF>> {
          if (--include_stack_ptr < 0)
            {
              yyterminate ();
            }
          else
            {
              yy_delete_buffer (YY_CURRENT_BUFFER);
              yy_switch_to_buffer (include_stack[include_stack_ptr]);
            }
        }
%%
