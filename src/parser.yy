/*
 * $Id: parser.yy,v 1.33 2016/11/12 16:52:21 mgeng Exp $
 *
 * Copyright (C) 2000, 2006, 2007, 2008, 2009, 2010, 2011
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

%{

#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include "genparse.h"
#include "clparam_list.h"
#include "logfile.h"
#include "unique_int.h"

#define YYDEBUG 1

// These  prototypes are necessary for C++ compilation
int yyerror (const string);
int yylex ();
void dump_short_reps (const string);
void add_param_to_list ();
void dump_param_type (param_type);

// These are all defined in genparse.cc
extern Clparam_list *my_params;
extern Unique_Int longopt_value;
extern Logfile mylog;
extern int yylineno;
extern char *yyfilename;

// This is an object we use over and over to read in parameters
Clparam *param;

%}

%expect 34 /* Report a warning only if this number of shift / reduce conflicts doesn't match */

%union 
{
  char    *str;  /* string */
  int     i;
  param_type p;
}

%token SLASH RANGE_SPEC1 RANGE_SPEC2 FUNC_SPEC NONE OPEN_BRACE CLOSE_BRACE
%token OPEN_SQUARE_BRACE CLOSE_SQUARE_BRACE EQUAL INCLUDE LT GT
%token MANDATORY EXIT_VALUE BREAK_LINES DOUBLEQUOTE ASTERISK EXCLAMATIONMARK
%token STORE_LONGINDEX EXPORT_LONG_OPTIONS GP_INCLUDE ERR_MSG COMMENT DO_NOT_DOCUMENT
%token OPEN_ROUND_BRACE CLOSE_ROUND_BRACE COLON ADD_FLAG NO_STRUCT CODE_END

%token <str> QUOTED FIRST_QUOTED C_VAR ALNUM FILENAME CHAR_VAL FIRST_STR USAGE_LINE CODE_LINE
%token <str> OPT_NAME COMMENT_STR FUNCTION
%token <i>   FLAG INT LONG ULONG INTMAX UINTMAX FLOAT DOUBLE CHAR STRING

%type <str>  desc short_params callback default single_code_line
%type <p>    type
%%

/*
 * Rules section
 */

all: globals entries usages
	| globals usages
	;

globals: /* empty */
	| globals global
	| global
	;

global: include
	| mandatory
	| exit_value
	| break_lines
	| export_long_options
	| no_struct
	| global_callback
	;

include: INCLUDE LT FILENAME GT
	{
		my_params->add_include ((string)"<" + $3 + ">");
		mylog.write ((string) "adding include file: <" + $3 + ">");
		free ($3);
	}
	| INCLUDE DOUBLEQUOTE FILENAME DOUBLEQUOTE
	{
		my_params->add_include ((string)"\"" + $3 + "\"");
		mylog.write ((string) "adding include file: \"" + $3 + "\"");
		free ($3);
	}
        ;

mandatory: MANDATORY C_VAR
	{
		my_params->add_mandatory ($2);
		mylog.write ((string) "adding mandatory: " + $2);
		free ($2);
	}
        ;

exit_value: EXIT_VALUE C_VAR
	{
		my_params->set_exit_value ($2);
		mylog.write ((string) "adding exit value: " + $2);
		free ($2);
	}
        | EXIT_VALUE ALNUM
	{
		my_params->set_exit_value ($2);
		mylog.write ((string) "adding exit value: " + $2);
		free ($2);
	}
        ;

break_lines: BREAK_LINES ALNUM
	{
		my_params->set_break_lines ($2);
		mylog.write ((string) "breaking lines to a width of: " + $2);
		free ($2);
	}
        ;

export_long_options: EXPORT_LONG_OPTIONS
	{
		my_params->set_export_long_options (true);
		mylog.write ((string) "exporting long options struct");
	}
        ;

no_struct: NO_STRUCT
	{
		my_params->set_no_struct (true);
		mylog.write ((string) "do not auto generate a struct / class for parsing results");
	}
        ;

global_callback: callback
	{
		my_params->set_global_callback ($1);
		mylog.write ((string) "setting global callback: " + $1);
	}
	;

entries: entries entry
	| entry
	;

entry:
	{
		param = new Clparam; mylog.write ("reading parameter...");
	}
	new_entry
	{
		add_param_to_list ();
	}
	| gp_include
	;

new_entry: param type options
	{
		param->set_type ($2);
		dump_param_type ($2);
	}
	;

options: /* empty */
	| options option
	| option
	;

option: default    
	{
		param->set_default_value ($1);
		mylog.write ((string) "  default value: " + $1);
		free ($1);
	}
	| range
	| callback
	{
		param->set_callback ($1);
		mylog.write ((string) "  callback: " + $1);
	}
	| descs
	| STORE_LONGINDEX
	{
		param->set_store_longindex (true);
		mylog.write ((string) "  longindex for this parameter must be stored");
	}
	| err_msg
	| ADD_FLAG
	{
		param->set_add_flag (true);
		mylog.write ((string) "  this parameter also gets a flag");
	}
	| DO_NOT_DOCUMENT
	{
		param->set_do_not_document (true);
		mylog.write ((string) "  do not document this parameter");
	}
	| comment
	| code_lines
	;

descs: descs desc
	| desc
	;

err_msg: ERR_MSG OPEN_ROUND_BRACE err_msg_arg CLOSE_ROUND_BRACE
	;

err_msg_arg: ALNUM
	{
		param->set_err_msg ($1);
		mylog.write ((string) "  conversion error message: " + $1);
		free ($1);
	}
	|
	QUOTED
	{
		param->set_err_msg (string("\"") + $1 + '\"');
		mylog.write ((string) "  conversion error message: \"" + $1 + '\"');
		free ($1);
	}
	|
	ALNUM COLON C_VAR
	{
		param->set_err_msg ($1);
		mylog.write ((string) "  conversion error message: " + $1);
		param->set_err_msg_conv ($3);
		mylog.write ((string) "  conversion function for erraneous argument: " + $3);
		free ($1);
		free ($3);
	}
	|
	QUOTED COLON C_VAR
	{
		param->set_err_msg (string("\"") + $1 + '\"');
		mylog.write ((string) "  conversion error message: \"" + $1 + '\"');
		param->set_err_msg_conv ($3);
		mylog.write ((string) "  conversion function for erraneous argument: " + $3);
		free ($1);
		free ($3);
	}
	;

comment: COMMENT OPEN_ROUND_BRACE COMMENT_STR CLOSE_ROUND_BRACE
	{
		param->set_comment ($3);
		mylog.write ((string) "  comment: " + $3);
		free ($3);
	}

gp_include: GP_INCLUDE FILENAME
	{
		mylog.write ((string) "including genparse file" + $2);
		free ($2);
	}
	;

param: short_params
	{
		param->set_short_reps ($1);
		dump_short_reps ($1);
		free ($1);

	}
	| NONE SLASH long_param
	{
		param->set_longopt_value (longopt_value.get ());
		param->set_long_only (true);
	}
	| short_params SLASH long_param
	{
		param->set_short_reps ($1);
		dump_short_reps ($1);
		free ($1);
	}
	;

short_params: FIRST_STR
	| FIRST_STR ASTERISK
	{
		param->set_short_has_arg (optional_argument);
	}
	| FIRST_STR EXCLAMATIONMARK
	{
		param->set_short_has_arg (no_argument);
	}
	;

long_param: multi_long_option
	| multi_long_option mandatory_opt_name
	| multi_long_option optional_opt_name
	;

mandatory_opt_name: EQUAL C_VAR
	{
		param->set_opt_name ($2);
		free ($2);
	}
	| EQUAL OPT_NAME
	{
		param->set_opt_name ($2);
		free ($2);
	}
	;

optional_opt_name: OPEN_SQUARE_BRACE mandatory_opt_name CLOSE_SQUARE_BRACE
	{
		param->set_opt_name_in_square_braces (true);
	}
	;

multi_long_option: long_option {}
	| long_option ASTERISK
	{
		param->set_long_has_arg (optional_argument);
	}
	| long_option EXCLAMATIONMARK
	{
		param->set_long_has_arg (no_argument);
	}
	;

long_option: ALNUM
	{
		param->set_long_rep ($1);
		mylog.write ((string) "  long representation: " + $1);
		free ($1);
	}
	| C_VAR
	{
		param->set_long_rep ($1);
		mylog.write ((string) "  long representation: " + $1);
		free ($1);
	}
	;

type: INT        { $$ = int_type; }
	| LONG
	{
		if (my_params->get_use_gnulib ())
			$$ = long_type;
		else
			yyerror ("type long only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
	| ULONG
	{
		if (my_params->get_use_gnulib ())
			$$ = ulong_type;
		else
			yyerror ("type ulong only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
	| INTMAX
	{
		if (my_params->get_use_gnulib ())
			$$ = intmax_type;
		else
			yyerror ("type intmax_t only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
	| UINTMAX
	{
		if (my_params->get_use_gnulib ())
			$$ = uintmax_type;
		else
			yyerror ("type uintmax_t only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
	| FLOAT  { $$ = float_type; }
	| DOUBLE
	{
		if (my_params->get_use_gnulib ())
			$$ = double_type;
		else
			yyerror ("type double only supported if GNU Portability Library is used "
				 "(option --gnulib enables it)");
	}
	| STRING { $$ = string_type; }
	| CHAR   { $$ = char_type; }
	| FLAG   { $$ = flag_type; }
	;

default: ALNUM
	| CHAR_VAL
	| OPEN_BRACE QUOTED CLOSE_BRACE { $$ = $2; }
	;

range: OPEN_SQUARE_BRACE contiguous_range CLOSE_SQUARE_BRACE
	;

contiguous_range: ALNUM range_spec more_range 
	{
		param->set_low_range ($1);
		mylog.write ((string)"  parameter range: " + $1 + "..");
		free ($1);
	}
	| range_spec more_range
	;

/* both are needed because of lexing ambiguity */
more_range: ALNUM
	{
		param->set_high_range ($1);
		mylog.write ((string)"  parameter range: .." + $1);
		free ($1);
	}
	| C_VAR
	{
		param->set_high_range ($1);
		mylog.write ((string)"  parameter range: .." + $1);
		free ($1);
	}
	|
	;

range_spec: RANGE_SPEC1
	| RANGE_SPEC2
	;

callback: FUNCTION FUNC_SPEC { $$ = $1; }
	;

desc: QUOTED
	{
		param->add_description ($1, false);
		mylog.write ((string) "  description: " + $1);
		free ($1);
	}
	| FIRST_QUOTED
	{
		param->add_description ($1, true);
		mylog.write ((string) "  description starting in 1st column: " + $1);
		free ($1);
	}
	;

usages: /* empty */
	| usages usage
	| usage
	;

usage: USAGE_LINE
	{
		my_params->append_usage_line ($1);
		mylog.write ((string)"  usage string:" + $1);
		free ($1);
	}
	;

code_lines: code_lines code_line
	| code_line
	;

code_line: single_code_line CODE_END
	{
		param->add_code_line ($1);
		mylog.write ((string)"  code string:" + $1);
		free ($1);
	}
	| CODE_END
	{
		param->add_code_line ("");
		mylog.write ((string)"  code string:");
	}
	;

single_code_line: CODE_LINE
	{
	}
	| OPEN_ROUND_BRACE
	{
		$$ = strdup ((string ("(")).c_str ());
	}
	| CLOSE_ROUND_BRACE
	{
		$$ = strdup ((string (")")).c_str ());
	}
	| single_code_line single_code_line
	{
		$$ = strdup ((string ($1) + $2).c_str ());
		free ($1);
		free ($2);
	}
	;
%%

int yyerror (const string s)
{
  cerr << s << " in " << yyfilename << ": " << yylineno << endl;
  return 1;
}

void dump_short_reps (const string s)
{
  mylog.write (LOG_OPTION_NO_CR, "  short representation: "); 
  mylog.write (LOG_OPTION_NO_TS, s);
}

void add_param_to_list ()
{
  mylog.write ("done reading parameter...adding to list");
  my_params->add (*param);
  delete param;
}

void dump_param_type (param_type p)
{
  mylog.write (LOG_OPTION_NO_CR, "  parameter type: ");
  switch (p)
    {
    case int_type:
      mylog.write (LOG_OPTION_NO_TS, "int");
      break;
    case long_type:
      mylog.write (LOG_OPTION_NO_TS, "long int");
      break;
    case ulong_type:
      mylog.write (LOG_OPTION_NO_TS, "unsigned long int");
      break;
    case intmax_type:
      mylog.write (LOG_OPTION_NO_TS, "intmax_t");
      break;
    case uintmax_type:
      mylog.write (LOG_OPTION_NO_TS, "uintmax_t");
      break;
    case float_type:
      mylog.write (LOG_OPTION_NO_TS, "float");
      break;
    case double_type:
      mylog.write (LOG_OPTION_NO_TS, "double");
      break;
    case string_type:
      mylog.write (LOG_OPTION_NO_TS, "string");
      break;
    case char_type:
      mylog.write (LOG_OPTION_NO_TS, "char");
      break;
    case flag_type:
      mylog.write (LOG_OPTION_NO_TS, "flag");
      break;
    default:
      break;
    }
}
