/** @file clparam.h
\brief Class definition for a single command line parameter */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLPARAM_H
#define CLPARAM_H

#include <list>
#include <string>
#include "cldesc.h"
#include "logfile.h"

// valid types of parameters
enum param_type
{
  null_type = 0, int_type, long_type, ulong_type, intmax_type, uintmax_type, 
    float_type, double_type, char_type, string_type, flag_type
};

enum has_arg_type
{
  no_argument = 0, required_argument, optional_argument, argument_undefined
};

/** \brief Contains information for 1 command line parameter. */
class Clparam
{
private:
  string        _short_reps;      // single letter representation
  string        _long_rep;        // string representation
  string        _opt_name;        // e.g. SIZE in --block-size=SIZE, only used on help screen
  bool          _opt_name_in_square_braces; // e.g. in --block*[=SIZE]
  param_type    _type;
  string        _default_value;
  string        _low_range;
  string        _high_range;
  string        _callback;
  string        _err_msg;
  string        _err_msg_conv;
  string        _comment;
  list<Cldesc>  _descriptions;
  list<string>  _code_lines;
  int           _longopt_value;
  bool          _long_only;
  bool          _store_longindex;
  bool          _do_not_document;
  bool          _add_flag;
  has_arg_type  _short_has_arg;
  has_arg_type  _long_has_arg;

public:
  // Constructor, destructor and copy constructor
  Clparam ();
  ~Clparam () {}
  Clparam& operator=(const Clparam &);

  // functions to set the values
  void set_short_reps (const string &s)       { _short_reps = s; }
  void set_long_rep (const string &s)         { _long_rep = s; }
  void set_opt_name (const string &s)         { _opt_name = s; }
  void set_opt_name_in_square_braces (bool b) { _opt_name_in_square_braces = b; }
  void set_type (param_type t)                { _type = t; }
  void set_default_value (const string &s)    { _default_value = s; }
  void set_low_range (const string &s)        { _low_range = s; }
  void set_high_range (const string &s)       { _high_range = s; }
  void set_callback (const string &s)         { _callback = s; }
  void set_err_msg (const string &s)          { _err_msg = s; }
  void set_err_msg_conv (const string &s)     { _err_msg_conv = s; }
  void set_comment (const string &s)          { _comment = s; }
  void add_description (const string &description, bool start_1st_col = false);
  void set_longopt_value (int i)              { _longopt_value = i; }
  void set_long_only (bool b)                 { _long_only = b; }
  void set_store_longindex (bool b)           { _store_longindex = b; }
  void set_do_not_document (bool b)           { _do_not_document = b; }
  void set_add_flag (bool b)                  { _add_flag = b; }
  void set_short_has_arg (has_arg_type x)     { _short_has_arg = x; }
  void set_long_has_arg (has_arg_type x)      { _long_has_arg = x; }
  void add_code_line (const string & s)       { _code_lines.push_back (s); }

  // functions to retrieve the values
  bool requires_additional_flag_arg (void)           const;
  char get_short_rep (unsigned int i)                const;
  const string get_short_reps (void)                 const { return _short_reps; }
  unsigned int get_num_short_reps (void)             const { return _short_reps.length (); }
  bool contains (char c)                             const;
  const string &get_long_rep (void)                  const { return _long_rep; }
  const string &get_opt_name (void)                  const { return _opt_name; }
  bool          get_opt_name_in_square_braces (void) const { return _opt_name_in_square_braces; }
  param_type    get_type (void)                      const { return _type; }
  const string &get_default_value (void)             const { return _default_value; }
  const string &get_low_range (void)                 const { return _low_range; }
  const string &get_high_range (void)                const { return _high_range; }
  const string &get_callback (void)                  const { return _callback; }
  const string &get_err_msg (void)                   const { return _err_msg; }
  const string &get_err_msg_conv (void)              const { return _err_msg_conv; }
  const string &get_comment (void)                   const { return _comment; }
  int           get_longopt_value (void)             const { return _longopt_value; }
  bool          get_long_only (void)                 const { return _long_only; }
  bool          get_store_longindex (void)           const { return _store_longindex; }
  bool          get_do_not_document (void)           const { return _do_not_document; }
  bool          get_add_flag (void)                  const { return _add_flag; }
  has_arg_type  get_short_has_arg (void)             const { return _short_has_arg; }
  has_arg_type  get_long_has_arg (void)              const { return _long_has_arg; }
  list<Cldesc>  get_description (unsigned int indentation, 
    unsigned int additional_indentation_following_lines, int break_lines) const;
  const list<string> &get_code_lines (void)           const { return _code_lines; }
  void normalize_code_lines_indentation (void);
  
  bool has_callback (void) const { return !_callback.empty (); }

  void logdump (Logfile&) const;
};

#endif  // CLPARAM_H
