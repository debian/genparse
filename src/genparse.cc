/** @file genparse.cc
\brief main () function of the command-line parser generator */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cstdio>
#include <string>
#include "genparse.h"
#include "logfile.h"
#include "clparam_list.h"
#include "clparam_list_factory.h"
#include "parse_cl.h"
#include "unique_int.h"

// Characters are 0-255, so this the first safe value
const int FIRST_LONGOPT_VALUE = 256;

char *yyfilename;
extern FILE *yyin;
extern int yyparse ();
extern int yydebug;

// This is the cat's pajamas - the main object
Clparam_list *my_params = NULL;

// Declare the logfile object globally so that we can access if from 
// everywhere
Logfile mylog;

// The object that will give us longopt numbers
Unique_Int longopt_value (FIRST_LONGOPT_VALUE);

/*---------------------------------------------------------------------------
 * 
 * add_extra_params ()
 *
 * Add -h, -v
 *-------------------------------------------------------------------------*/

void add_extra_params (void)
{
  Clparam *p;

  // if any of the extra parameters are not already set, add them now
  // start with help
  mylog.write ("Adding -h / --help parameter");
  if (!my_params->contains ("help"))
    {
      p = new Clparam ();
      if (!my_params->contains ('h'))
        p->set_short_reps (string ("h"));
      else
        p->set_long_only (true);
      p->set_long_rep ("help");
      p->set_type (flag_type);
      p->add_description ("Display this help and exit.");
      my_params->add (*p);
      delete p;
    }

  // version
  mylog.write ("Adding -v / --version parameter");
  if (!my_params->contains ("version"))
    {
      p = new Clparam ();
      if (!my_params->contains ('v'))
        p->set_short_reps (string ("v"));
      else
        p->set_long_only (true);
      p->set_long_rep ("version");
      p->set_type (flag_type);
      p->add_description ("Output version information and exit.");
      my_params->add (*p);
      delete p;
    }
}

/*---------------------------------------------------------------------------
 * 
 * main ()
 *
 *-------------------------------------------------------------------------*/

int main (int argc, char *argv[])
{
  // Parse my command line parameters
  // Here's the lowdown:
  // d = debug (logging) flag
  // f = filename of log file
  // o = filename of output file
  Cmdline cl (argc, argv);
  
  // if the user just wants help or the version, do that now then quit
  if (cl.v ())
    {
      cout << PACKAGE << " v" << VERSION << endl;
      exit (0);
    }
  if (cl.h ())
    cl.usage (0);

  Clparam_list_factory factory;
  my_params = factory.Create_Clparam_list (cl.l ());
  
  // set quiet mode if specified
  if (cl.q ())
    my_params->set_quiet ();

  // for debugging purposes, it is useful to traverse the list and make sure
  // it really has everything that we gave it
  my_params->logdump (mylog);
  
  // set the parsing function / class
  my_params->set_parsefunc (cl.p ());

  // set the directory for output files
  my_params->set_directory (cl.D ());

  // set the c++ extension
  my_params->set_cppext (cl.c ());

  // set longmembers flag (use of long member names in parser class)
  my_params->set_longmembers (cl.m ());

  // set internationalization flag (put internationalization macro _() arount help text)
  my_params->set_internationalization (cl.i ());

  // set --static-headers flag (don't add creation date, user name, kernel version etc.)
  my_params->set_static_headers (cl.s ());

  // set flag if Gnu Portability Library (Gnulib) shall be used
  my_params->set_use_gnulib (cl.g ());

  /** set flag specifying if every command line parameter shall be output in a separate print 
      command */
  my_params->set_many_prints (cl.P ());

  // make sure that we've got a file to parse
  if (cl.next_param () >= argc)
    throw EH ("Must include a genparse file on the command line.");
  
  // Start the log file if debugging mode is turned on
  if (cl.d ())
    {
      mylog.open (cl.f ());
      yydebug= 1;
    }

  // Open the .gp file for parsing in C style so that the yacc code works
  FILE *fp = fopen (argv[cl.next_param ()], "r");
  if (fp == NULL) 
    throw EH ("can't open input genparse file: " + (string) argv[cl.next_param ()]);
  yyin = fp;
  mylog.write ("opened input file: " + (string) argv[cl.next_param ()]);

  // set the filename and run the parser on the .gp file
  yyfilename = argv[cl.next_param ()];
  if (yyparse ())
    exit (1);

  // add --help and --version params if not already defined
  add_extra_params ();
  
  // required after parsing
  my_params->post_parser_adjust ();

  if (!my_params->has_usage ())
    {
      /* No usage function defined in the Genparse file => set default */
      my_params->append_usage_line ("usage: __PROGRAM_NAME__ __OPTIONS_SHORT__ __MANDATORIES__");
      my_params->append_usage_line ("__GLOSSARY__");
    }

  // if we got here, it parsed successfully, now we clean up a little
  fclose (fp);
  mylog.write ("done reading input file");
  mylog.write_separator ();

  // Now we write the output files
  my_params->output (cl.o ());

  // Close the log file before we leave
  mylog.close ();

  // quit, and do it nicely!
  exit (0);
}
