/** @file eh.cc
\brief Methods for the error handler class */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <iostream>
#include <sstream>
#include "eh.h"

// default dispostion is to halt upon errors
eh_disp_t EH::_disposition = EH_DISP_HALT;

// default callback function is set to nothing
void (*EH::_callback)(string) = NULL;

//---------------------------------------------------------------------------
//
// EH::EH ()
//
// Constructor method.  Do something intelligent with the error string.
//
//---------------------------------------------------------------------------

EH::EH (string s)
{
  action (s, "", 0);
}

//---------------------------------------------------------------------------
//
// EH::EH (string, string, int)
//
// Constructor method.  Do something intelligent with the error string.
//
//---------------------------------------------------------------------------

EH::EH (string s, string f, int l)
{
  action (s, f, l);
}
//---------------------------------------------------------------------------
//
// EH::action ()
//
// Decides what to do based on the current disposition
//
//---------------------------------------------------------------------------

void EH::action (string s, string f, int l)
{
  ostringstream oss;

  if (!f.empty ())
    oss << f << ":" << l << "  " << s;
  else
    oss << s;

  switch (_disposition)
    {
    case EH_DISP_HALT:
      cerr << oss.str () << endl;
      abort ();
      break;

    case EH_DISP_CONTINUE:
      cerr << oss.str () << endl;
      break;

    case EH_DISP_IGNORE:
      break;

    case EH_DISP_CALLBACK:
      if (_callback == NULL)
	{
	  cerr << "EH: must set callback function" << endl;
	  abort ();
	}
      _callback (s);
      break;
    }

  return;
}

