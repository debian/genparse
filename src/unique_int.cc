/** @file unique_int.cc
\brief Methods for unique integer generator class */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "unique_int.h"

//---------------------------------------------------------------------------
//
// Unique_Int::get ()
//
// Get the next unique int. 
//
//---------------------------------------------------------------------------

int Unique_Int::get ()
{
  int temp;

  temp = _i;
  _i++;
  return temp;
}











