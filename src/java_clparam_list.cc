/** @file java_clparam_list.cc
\brief Methods for the Java implementation of the command line parser */
/*
 * Copyright (C) 2006 - 2016
 * Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <algorithm>
#include "java_clparam_list.h"
#include "sysinfo.h"
#include "systime.h"
#include "userinfo.h"
#include "genparse_util.h"

using genparse_util::C_comment;
using genparse_util::full_path;

/** Helper function for output_usage (). Output one print command. */
void Java_Clparam_list::output_print_command (
  /** open stream where to print */
  ofstream& o, 
  /** Text which must be printed. Must contain starting double quotes '"', 
      ending double quotes are appended in this function. */
  const string &text) const
{
  o << "        System.out.println (" << endl;
  o << text << "\");"               << endl;
}

/** set flag if Gnu Portability Library (Gnulib) shall be used */
void Java_Clparam_list::set_use_gnulib (bool b) throw (EH)
{
  if (b)
    throw EH ("Use of GNU Portability library not supported for Java output");
}

void Java_Clparam_list::check_parameters (void) const
{
  if (get_internationalization ())
    cerr << "Warning: Internationalization not implemented for Java output" << endl;
}

/** Output a class comment banner */
void Java_Clparam_list::output_comments_object (ofstream& o, string classname, string comment, 
  string objectname) const
{
  o << "/*----------------------------------------------------------------------------" << endl;
  o << "**"                                                                             << endl;
  o << "** " << objectname << ' ' << classname                                          << endl;
  o << "**"                                                                             << endl;
  o << "** " << comment                                                                 << endl;
  o << "**"                                                                             << endl;
  o << "**--------------------------------------------------------------------------*/" << endl;  
}

/** Output a file containing the java exception class thrown by the parser class. */
void Java_Clparam_list::output_exception_class (void) const throw (EH)
{
  const string filename = get_exception_class_name () + ".java";
  const string path = full_path (get_directory (), filename);
  list<Clparam> params = get_params ();
  
  // tell the user what we're doing
  if (!get_quiet ())
    cout << "creating " << path << "...";
  
  // open the file
  ofstream o;
  o.open (path.c_str ());
  if (!o)
    throw EH ("can't open file: " + path);
  
  // print a nice friendly comment banner at the beginning of the file
  output_comments_header (o, filename, "Interface of the command line parser class");

  // output the definition of the exception class
  output_comments_object (o, get_exception_class_name (), 
    "Exception class thrown by the command line parser class", "class");
  o << endl;
  o << "public class " << get_parsefunc () << "Ex extends RuntimeException" << endl;
  o << '{' << endl;
  o << "  public " << get_exception_class_name () << " (String text) { super (text); }" << endl;
  o << '}' << endl;

  if (!o)
    throw EH ("can't write to file: " + path);

  // close the file
  o.close ();
  if (!o)
    throw EH ("can't close file: " + path);

  // tell the user what we're doing
  if (!get_quiet ())
    cout << "done" << endl;
}

/** Output a file containing the java interface of the parser class + another file containing
    the exception class. */
void Java_Clparam_list::output_interface (const string filename_without_extension) const throw (EH)
{
  // make file <filename_without_extension>Interface.java
  const string filename = filename_without_extension + "Interface.java";
  const string path = full_path (get_directory (), filename);
  list<Clparam> params = get_params ();
  list<string>::const_iterator str_iter;
  list<Clparam>::const_iterator iter;
  
  if (get_no_struct ())
    throw EH ("#no_struct not yet implemented for C++ output.");

  // tell the user what we're doing
  if (!get_quiet ())
    cout << "creating " << path << "...";
  
  // open the file
  ofstream o;
  o.open (path.c_str ());
  if (!o)
    throw EH ("can't open file: " + path);
  
  // print a nice friendly comment banner at the beginning of the file
  output_comments_header (o, filename, "Interface of the command line parser class");

  // dump the class
  output_comments_object (o, get_interface_name (), 
    "Interface of the command line parser class", "interface");
  o << endl;
  o << "public interface " << get_interface_name () << endl;
  o << "{" << endl;

  o << "  " << C_comment ("usage function") << endl;
  o << "  void usage (int status, String program_name);" << endl;
  o << endl;

  o << "  " << C_comment ("return next (non-option) parameter") << endl;
  o << "  int next_param ();" << endl;
  o << endl;

  if (have_callbacks ())
    {
      list<string> param_callback_list;

      o << "  " << C_comment ("callback functions") << endl;
      /* global callbacks */
      if (!get_global_callback ().empty ())
        o << "  boolean " << get_global_callback () << " ();" << endl;
      /* parameter related callbacks */
      for (iter = params.begin (); iter != params.end (); iter++)
        if (!(iter->get_callback ()).empty ())
          /* Print every callback function only once, no matter how many parameters use it. */
          if (find (param_callback_list.begin (), param_callback_list.end (),
            iter->get_callback ()) == param_callback_list.end ())
            {
              o << "  boolean " << iter->get_callback () << " ();" << endl;
              param_callback_list.push_back (iter->get_callback ());
            }
      o << endl;
    }
  
  o << "  " << C_comment ("getter functions for command line parameters") << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      o << "  " << type2str (iter->get_type ()) << ' ' << get_parameter_name (*iter)
        << " ();" << endl;
      if (iter->requires_additional_flag_arg ())
        o << "  " << type2str (flag_type) << ' ' << get_parameter_name (*iter) 
          << "_flag ();" << endl;
      if (iter->get_store_longindex ())
        o << "  " << type2str (int_type) << ' ' << get_parameter_name (*iter) 
          << "_li ();" << endl;
    }

  // closing bracket for class definition
  o << "};" << endl;

  if (!o)
    throw EH ("can't write to file: " + path);

  // close the file
  o.close ();
  if (!o)
    throw EH ("can't close file: " + path);

  // tell the user what we're doing
  if (!get_quiet ())
    cout << "done" << endl;

  output_exception_class ();
}

/** Output a java source file containing the implementation of the parser class */
void Java_Clparam_list::output_implementation (const string filename_without_extension) 
  const throw (EH)
{
  // make file extension
  const string filename = filename_without_extension + ".java";
  const string path = full_path (get_directory (), filename);
  list<Clparam> params = get_params ();
  list<string>::const_iterator str_iter;
  list<Clparam>::const_iterator iter;
  unsigned int i;
  ostringstream usage_stream;
  
  // tell the user what we're doing
  if (!get_quiet ())
    cout << "creating " << path << "...";
  
  // open the file
  ofstream o;
  o.open (path.c_str ());
  if (!o)
    throw EH ("can't open file: " + path);
  
  // print a nice friendly comment banner at the beginning of the file
  output_comments_header (o, filename, "Definition of command line parser class");
  
  // imports
  o << "import gnu.getopt.LongOpt;" << endl;
  o << "import gnu.getopt.Getopt;"  << endl;
  o << endl;
 
  // comments for parser class
  output_function_comment (o, "class " + get_parsefunc (), "Command line parser class.");
  o << endl;

  // dump the class
  o << "public class " << get_parsefunc () << " implements " << get_interface_name () << endl;
  o << "{" << endl;
  
  o << "  " << C_comment ("parameters") << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      o << "  private " << type2str (iter->get_type ()) << " _" 
        << get_parameter_name (*iter);
      if (iter->get_type () == string_type)
        o << " = \"\"";
      o << ";" << endl;
      if (iter->requires_additional_flag_arg ())
        o << "  private " << type2str (flag_type) << " _"
          << get_parameter_name (*iter) << "_flag;" << endl;
      if (iter->get_store_longindex ())
        o << "  private " << type2str (int_type) << " _"
          << get_parameter_name (*iter) << "_li;" << endl;
    }
  o << endl;

  o << "  " << C_comment ("Name of the calling program") << endl;
  o << "  private String _executable;" << endl;
  o << endl;

  o << "  " << C_comment ("next (non-option) parameter") << endl;
  o << "  private int _optind;" << endl;
  o << endl;

  // Constructor
  o << "  " << C_comment ("Must be constructed with parameters.") << endl;
  o << "  public " << get_parsefunc () << " (String[] argv) throws " 
    << get_exception_class_name () << endl;
  o << "  {" << endl;

  o << "    " << C_comment ("character returned by optind ()") << endl;
  o << "    int c;" << endl;
  o << endl;

  // options structure
  o << "    LongOpt[] longopts = new LongOpt[" << params.size () << "];" << endl;
  int param_no = 0;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if (iter->get_long_rep ().length () > 0)
        {
          o << "    longopts[" << param_no << "] = new LongOpt (\"" << iter->get_long_rep () 
            << "\", LongOpt.";
          switch (iter->get_long_has_arg ())
            {
            case no_argument:
              o << "NO_ARGUMENT";
              break;
            case optional_argument:
              o << "OPTIONAL_ARGUMENT";
              break;
            case required_argument:
              o << "REQUIRED_ARGUMENT";
              break;
            default:
              break;
            }
          o << ", null, ";
          if (iter->get_long_only ())
	    o << iter->get_longopt_value ();
          else
	    o << '\'' << iter->get_short_rep (0) << '\'';
          o << ");" << endl;
          param_no++;
        }
    }
  o << endl;

  o << "    _executable = " << get_parsefunc () << ".class.getName ();" << endl;
  o << endl;

  // assign default values
  o << "    " << C_comment ("default values") << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      switch (iter->get_type ())
        {
        case flag_type:  // all flags default to off
          o << "    _" << get_parameter_name (*iter) << " = false;" << endl;
          break;
        case string_type: // copy default if there, NULL otherwise
          if (!iter->get_default_value ().empty ())
            o << "    _" << get_parameter_name (*iter)
              << " = \"" << iter->get_default_value () << "\";" << endl;
          break;
        default: // all else - copy default if there
          if (!iter->get_default_value ().empty ())
            {
            o << "    _" << get_parameter_name (*iter)
              << " = " << iter->get_default_value ();
            if (iter->get_type () == float_type) o << "f";
            o << ";" << endl;
          }
        }
      if (iter->requires_additional_flag_arg ())
        o << "    _" << get_parameter_name (*iter) << "_flag = false;" << endl;
      if (iter->get_store_longindex ())
        o << "    _" << get_parameter_name (*iter) << "_li = 0;" << endl;
    }
  o << endl;

  // Make the Getopt () call
  o << "    Getopt g = new Getopt (_executable, argv, \"";
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      if (!iter->get_long_only ())
        {
          o << iter->get_short_reps ();
          if (iter->get_short_has_arg () == required_argument)
            o << ':';
          else if (iter->get_short_has_arg () == optional_argument)
            o << "::";
        }
    }
  o << "\", longopts);" << endl;

  o << "    while ((c = g.getopt ()) != -1)" << endl;
  o << "    {" << endl;

  // do the switch statement
  o << "      switch (c)" << endl;
  o << "      {" << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      unsigned int indent_width;
      #define INDENT string (indent_width, ' ')

      if (iter->get_long_only ())
        o << "        case " << iter->get_longopt_value () << ": " << endl;
      else
        {
          for (i = 0; i < iter->get_num_short_reps ();i++)
            o << "        case '" << iter->get_short_rep (i) << "': " << endl;
        }

      indent_width = 10;
      if (iter->requires_additional_flag_arg ())
        {
          o << INDENT << '_' << get_parameter_name (*iter) << "_flag = true;" << endl;
          o << INDENT << "if (g.getOptarg () != null)" << endl;
          indent_width += 2;
        }
      o << INDENT << '_' << get_parameter_name (*iter);
      switch (iter->get_type ())
        {
        case string_type:
          o << " = g.getOptarg ();" << endl;
          break;
        case int_type:
          o << " = Integer.parseInt (g.getOptarg ());" << endl;
          break;
        case float_type:
          o << " = Float.parseFloat (g.getOptarg ());" << endl;
          break;
        case char_type:
          o << " = g.getOptarg ().charAt (0);" << endl;
          break;
        case flag_type:
          o << " = true;" << endl;
          if (iter->get_long_rep () == "help")
            o << INDENT << "usage (0, _executable);" << endl;
          break;
        default:
          break;
        }
      if (iter->get_store_longindex ())
        o << INDENT << '_' << get_parameter_name (*iter) << "_li = g.getLongind ();" << endl;

      // do low range checking
      if (!iter->get_low_range ().empty ())
        {
          o << INDENT << "if (_" << get_parameter_name (*iter)
            << " < " << iter->get_low_range ();
          indent_width += 2;
          if (iter->get_type () == float_type) o << "f";
          o << ")" << endl;
	  o << INDENT << "throw new " << get_exception_class_name () 
            << " (\"parameter range error: " << get_parameter_name (*iter)
            << " must be >= " << iter->get_low_range ();
          if (iter->get_type () == float_type) o << "f";
	  o << "\");" << endl;
          indent_width -= 2;
        }
      // do high range checking
      if (!iter->get_high_range ().empty ())
        {
          o << INDENT << "if (_" << get_parameter_name (*iter)
            << " > " << iter->get_high_range ();
          indent_width += 2;
          if (iter->get_type () == float_type) o << "f";
          o << ")" << endl;
	  o << INDENT << "throw new " << get_exception_class_name () 
            << " (\"parameter range error: " << get_parameter_name (*iter)
            << " must be <= " << iter->get_high_range ();
          if (iter->get_type () == float_type) o << "f";
	  o << "\");" << endl;
          indent_width -= 2;
        }

      // write user defined code for this parameter
      list<string> code_lines = iter->get_code_lines ();
      list<string>::const_iterator code_iter;
      for (code_iter = code_lines.begin (); code_iter != code_lines.end (); code_iter++)
        o << INDENT << *code_iter                                                   << endl;

      // do callbacks
      if (!iter->get_callback ().empty ())
        {
          o << INDENT << "if (!" << iter->get_callback () << " ())" << endl;
          indent_width += 2;
          o << INDENT << "usage (" << get_exit_value () << ", _executable);" << endl;
          indent_width -= 2;
        }
      
      // do break statement
      o << INDENT << "break;"                                                       << endl 
                                                                                    << endl;            
    }

  // print default action for unknown parameters
  o << "        default:" << endl;
  o << "          usage (" << get_exit_value () << ", _executable);" << endl << endl;
  o << "        }" << endl;
  o << "    } /* while */" << endl << endl;

  // save a pointer to the next argument
  o << "    _optind = g.getOptind ();" << endl;

  // do global callbacks
  if (!get_global_callback ().empty ())
    {
      o << "    if (!" << get_global_callback () << " ())"           << endl;
      o << "      usage (" << get_exit_value () << ", _executable);" << endl << endl;
    }
  
  o << "  }" << endl;
  o << endl;

  // usage function
  o << "  public void " << "usage (int status, String program_name)" << endl;
  o << "  {"                                                         << endl;
  o << "    if (status != 0)"                                        << endl;
  o << "      {"                                                     << endl;
  o << "        System.err.println (\"Try `\" + program_name + \" --help' for more information.\");" 
    <<                                                                  endl; 
  o << "      }"                                                     << endl;
  o << "    else"                                                    << endl;
  o << "      {"                                                     << endl;
  list<string> usage_list = get_usage ();
  for (str_iter = usage_list.begin (); str_iter != usage_list.end (); str_iter++)
    {
      string::size_type macro_pos, arg_start_pos, arg_end_pos, closing_bracket_pos;
      string macro_argument;
      bool line_manipulated;

      string line = *str_iter;
      do
        if ((macro_pos = line.find (string ("__PROGRAM_NAME__"))) != string::npos)
          line.replace (macro_pos, 16, string ("\" + program_name + \""));
      while (macro_pos != string::npos);
      if (line.find ("__DO_NOT_DOCUMENT__") == string::npos)
        {
          line_manipulated = false;

          do
            {
              if ((macro_pos = line.find (string ("__NEW_PRINT__"))) != string::npos)
                {
                  if (macro_pos > 0)
                    {
                      if (!usage_stream.str ().empty ())
                        usage_stream << "\\n\" +" << endl;
                      usage_stream << '\"' << string (line, 0, macro_pos);
                    }

                  if (usage_stream.str ().length () > 0)
                    output_print_command (o, usage_stream.str ());

                  usage_stream.str ("");

                  /* Remove __NEW_PRINT__ and any characters before */
                  line.erase (0, macro_pos + 13);

                  line_manipulated = true;
                }
              else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
                "__CODE__", line)) != string::npos)
                {
                  if (macro_pos > 0)
                    {
                      if (!usage_stream.str ().empty ())
                        usage_stream << "\\n\" +" << endl;
                      usage_stream << '\"' << string (line, 0, macro_pos);
                    }

                  if (usage_stream.str ().length () > 0)
                    output_print_command (o, usage_stream.str ());

                  o << "        " 
                    << line.substr (arg_start_pos, closing_bracket_pos - arg_start_pos) << endl;

                  usage_stream.str ("");

                  /* Remove __CODE__() and any characters before */
                  line.erase (0, closing_bracket_pos + 1);

                  line_manipulated = true;
                }
              else if ((macro_pos = parse_macro (arg_start_pos, arg_end_pos, closing_bracket_pos, 
                "__COMMENT__", line)) != string::npos)
                {
                  if (macro_pos > 0)
                    {
                      if (!usage_stream.str ().empty ())
                        usage_stream << "\\n\" +" << endl;
                      usage_stream << '\"' << string (line, 0, macro_pos);
                    }

                  if (usage_stream.str ().length () > 0)
                    output_print_command (o, usage_stream.str ());

                  usage_stream.str ("");

                  o << "        /* " 
                    << line.substr (arg_start_pos, closing_bracket_pos - arg_start_pos) 
                    << " */" << endl;

                  /* Remove __COMMENT__() and any characters before */
                  line.erase (0, closing_bracket_pos + 1);

                  line_manipulated = true;
                }
            }
          while (macro_pos != string::npos);
          if (!line_manipulated || !line.empty ())
            {
              if (!usage_stream.str ().empty ())
                usage_stream << "\\n\" +" << endl;
              usage_stream << '\"' << line;
            }
        }
    }
  if (usage_stream.str ().length () > 0)
    output_print_command (o, usage_stream.str ());
  o << "      }" << endl;

  o << "    System.exit (status);" << endl;
  o << "  }" << endl;
  o << endl;

  o << "  " << C_comment ("return next (non-option) parameter") << endl;
  o << "  public int next_param () { return _optind; }" << endl;
  o << endl;

  if (have_callbacks ())
    {
      list<string> param_callback_list;

      o << "  " << C_comment ("Callback functions") << endl;
      o << "  " << C_comment ("Derive your own class and overwrite any of the callback ") << endl;
      o << "  " << C_comment ("functions if you need customized callbacks.") << endl;
      /* global callbacks */
      if (!get_global_callback ().empty ())
        o << "  public " << boolean_type_str () << ' ' << get_global_callback () 
          << " () { return true; }" << endl;

      /* parameter related callbacks */
      for (iter = params.begin (); iter != params.end (); iter++)
        if (!(iter->get_callback ()).empty ())
          if (find (param_callback_list.begin (), param_callback_list.end (),
            iter->get_callback ()) == param_callback_list.end ())
            {
	      o << "  public " << boolean_type_str () << ' ' << iter->get_callback () 
                << " () { return true; }" << endl;
              param_callback_list.push_back (iter->get_callback ());
            }
      o << endl;
  }

  o << "  " << C_comment ("getter functions for command line parameters") << endl;
  for (iter = params.begin (); iter != params.end (); iter++)
    {
      o << "  public " << type2str (iter->get_type ()) << ' ' 
        << get_parameter_name (*iter) << " () { return _" 
        << get_parameter_name (*iter) << "; }" << endl;
      if (iter->requires_additional_flag_arg ())
        o << "  public " << type2str (flag_type) << ' ' 
          << get_parameter_name (*iter) << "_flag () { return _" 
          << get_parameter_name (*iter) << "_flag; }" << endl;
      if (iter->get_store_longindex ())
        o << "  public " << type2str (int_type) << ' ' 
          << get_parameter_name (*iter) << "_li () { return _" 
          << get_parameter_name (*iter) << "_li; }" << endl;
    }

  // closing bracket for class definition
  o << "}" << endl;

  if (!o)
    throw EH ("can't write to file: " + path);

  // close the file
  o.close ();
  if (!o)
    throw EH ("can't close file: " + path);

  // tell the user what we're doing
  if (!get_quiet ()) 
    cout << "done" << endl;  
}

/** Dummy. In Java no separate file is generated for callbacks. Derive your own class from the
    command line class instead. */
void Java_Clparam_list::output_callbacks (const string filename_without_extension) const throw (EH)
{
}
