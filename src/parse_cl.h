/******************************************************************************
**
** parse_cl.h
**
** Sun Nov 16 13:14:13 2008
** Linux 2.6.23 (#9 Sun Jan 27 22:29:05 CET 2008) i686
** linux@mgpc (Michael Geng)
**
** Header file for command line parser class
**
** Automatically created by genparse v0.8.1
**
** See http://genparse.sourceforge.net for details and updates
**
******************************************************************************/

#ifndef CMDLINE_H
#define CMDLINE_H

#include <iostream>
#include <string>

/*----------------------------------------------------------------------------
**
** class Cmdline
**
** command line parser class
**
**--------------------------------------------------------------------------*/

class Cmdline
{
private:
  /* parameters */
  std::string _c;
  bool _d;
  std::string _D;
  std::string _f;
  bool _g;
  bool _i;
  std::string _l;
  bool _m;
  std::string _o;
  std::string _p;
  bool _P;
  bool _q;
  bool _s;
  bool _h;
  bool _v;

  /* other stuff to keep track of */
  std::string _program_name;
  int _optind;

public:
  /* constructor and destructor */
  Cmdline (int, char **) throw (std::string);
  ~Cmdline (){}

  /* usage function */
  void usage (int status);

  /* return next (non-option) parameter */
  int next_param () { return _optind; }

  std::string c () { return _c; }
  bool d () { return _d; }
  std::string D () { return _D; }
  std::string f () { return _f; }
  bool g () { return _g; }
  bool i () { return _i; }
  std::string l () { return _l; }
  bool m () { return _m; }
  std::string o () { return _o; }
  std::string p () { return _p; }
  bool P () { return _P; }
  bool q () { return _q; }
  bool s () { return _s; }
  bool h () { return _h; }
  bool v () { return _v; }
};

#endif
