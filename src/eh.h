/** @file eh.h
\brief Class definition generic error handler */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EH_H
#define EH_H

#include <string>

using namespace std;

// types of dispositions
enum eh_disp_t
{
  EH_DISP_HALT,      // print msg then terminate
  EH_DISP_CONTINUE,  // print msg then continue where we left off
  EH_DISP_IGNORE,    // do nothing, just return
  EH_DISP_CALLBACK   // pass the msg to a callback function
};

#define EH_LOCATION __FILE__,__LINE__

//---------------------------------------------------------------------------
//
// class EH
//
//---------------------------------------------------------------------------

class EH
{
protected:
  static eh_disp_t _disposition;
  static void (*_callback) (string);

  // determines what to do based on disposition
  void action (string, string, int);

public:
  // Constructor, destructor
  EH (string);
  EH (string, string, int); // for passing __FILE__ and __LINE__
  EH () {} // for inherited classes, we don't need to do anything
  ~EH (){}
  
  // disposition functions
  static eh_disp_t disposition ()            { return _disposition; }
  static void      disposition (eh_disp_t d) { _disposition = d; }
  
  // callback function
  static void      callback (void (*f)(string)) { _callback = f; }
};

#endif
