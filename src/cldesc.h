/** @file cldesc.h
\brief Class definition for one line of decriptive text of one command line parameter */
/*
 * Copyright (C) 2007 - 2016
 * Michael Geng <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLDESC_H
#define CLDESC_H

#include <string>

using namespace std;

/** \brief Contains descriptive text for one line of text of one command line parameter. */
class Cldesc
{
private:
  string _description;      // descriptive text
  bool   _start_1st_col;    // true if _description shall start in the 1st column

public:
  // Constructor, destructor and copy constructor
  Cldesc ();
  Cldesc (const string &description, bool start_1st_col = false);
  ~Cldesc () {}
  Cldesc& operator=(const Cldesc &);

  // functions to set the values
  void set_description (const string &s) { _description = s; }
  void set_start_1st_col (bool b)        { _start_1st_col = b; }

  // functions to retrieve the values
  const string &get_description   (void) const { return _description; }
  bool          get_start_1st_col (void) const { return _start_1st_col; }
};

#endif  // CLDESC_H
