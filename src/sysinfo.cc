/** @file sysinfo.cc
\brief Methods for the system information class */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/utsname.h>
#include "sysinfo.h"

//---------------------------------------------------------------------------
//
// Sysinfo::Sysinfo ()
//
// Constructor method.  Does most of the work
//
//---------------------------------------------------------------------------

Sysinfo::Sysinfo () throw (EH_system)
{
  struct utsname machine;

  // Get the machine-specific info
  if (uname (&machine) == -1)
    {
      EH_system e ("can't get system info");
      throw e;
    }

  // store all of the items locally
  _name = machine.sysname;
  _release = machine.release;
  _build = machine.version;
  _hardware = machine.machine;
  _domainname = machine.nodename;

}











