/** @file logfile.h
\brief Class definition for logfiles */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGFILE_H
#define LOGFILE_H

#include <fstream>
#include <string>
#include "systime.h"
#include "eh.h"

enum log_option_t 
{
  LOG_OPTION_NORMAL, // send timestamp and carriage return
  LOG_OPTION_NO_CR,  // don't send the carriage return
  LOG_OPTION_NO_TS   // don't send the timestamp
};

//---------------------------------------------------------------------------
//
// class Logfile
//
// Instead of subclassing ofstream, we roll our own.  This way we have
// the flexibility to add a timestamp to the beginning of each line, which
// is very useful for debugging real-time code.
//
//---------------------------------------------------------------------------

class Logfile
{
private:
  string      _filename;
  ofstream    _log;
  bool        _active;
  bool        _is_open;

  // write a timestamp - called by public write () functions
  void write_ts () { Systime t; _log << t.hms () << "." << t.microseconds () << ": "; }

public:
  // Constructor and destructor
  Logfile ();
  ~Logfile (){}

  // open the log file
  void open (string) throw (EH);

  // close the log file
  void close ();

  // check if the logfile is active
  bool is_active () { return _active; }

  // suspend logging w/o closing the file
  void suspend () { _active = false; }

  // resume suspended logging
  void resume () { _active = true; }

  // functions to retrieve the values
  string filename ()   { return _filename; }

  // Call one of these to write to the log file
  void write_separator ();
  void write (string);
  void write (log_option_t, string);
  void write (char);
  void write (log_option_t, char);
};

#endif
