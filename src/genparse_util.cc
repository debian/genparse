/** @file genparse_util.cc
\brief Utilities used within the genparse project */
/*
 * Copyright (C) 2000, 2006 - 2016
 * Michael S. Borella <mike@borella.net> and
 * Michael Geng       <linux@MichaelGeng.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <algorithm>
#include "genparse_util.h"

namespace genparse_util
{
  /** Convert a string to upper case */
  string str2upper (string s)
  {
    /* toupper is ambiguous, so use a cast in order to choose the right one */
    transform (s.begin (), s.end (), s.begin (), (int (*)(int))toupper);
    return s;
  }

  /** Given a string, return it in C comment form in a new string */
  string C_comment (string s)
  {
    return "/* " + s + " */";
  }

  /** combine directory and filename to a complete path. */
  string full_path (const string directory, const string filename)
  {
    if (directory.length () > 0)
    {
      return (directory + '/' + filename);
    }
    else
    {
      return (filename);
    }
  }

  /** Converts '-' to '_' */
  char minus_to_underscore (const char c)
  {
    if (c == '-')
      return '_';
    else
      return c;
  }

  /** Convert a string to C style. Converts every '-' to '_' and prepend and underscore ('_') to
      every single digit name */
  string str2C (string s)
  {
    if (s.length () == 1)
      {
        if (isdigit (s[0]))
          return ('_' + s);
      }
    else
      transform (s.begin (), s.end (), s.begin (), (int (*)(int))minus_to_underscore);
    return s;
  }
}
